package ie.rapdb;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.Color;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUI_EmailDarren extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txtDieCode;
	private JTextField txtMOD;
	private JTextField txtName;
	private JComboBox cboDieType, cboRepeat;
	private JCheckBox checkBox;
	private GUI_ProgressBar frame;
	private String repeat;

	// /**
	// * Launch the application.
	// */
	// public static void main(String[] args) {
	// try {
	// GUI_EmailDarren dialog = new GUI_EmailDarren();
	// dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	// dialog.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	/**
	 * Create the dialog.
	 */
	public GUI_EmailDarren() {
		setBounds(100, 100, 750, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JPanel panelBackground = new JPanel();
		panelBackground.setBackground(new Color(112, 128, 144));
		panelBackground.setForeground(new Color(112, 128, 144));
		panelBackground.setBounds(10, 11, 714, 218);
		contentPanel.add(panelBackground);
		panelBackground.setLayout(null);
		{
			JPanel panelLabels = new JPanel();
			panelLabels.setBounds(10, 70, 694, 26);
			panelBackground.add(panelLabels);
			panelLabels.setLayout(new GridLayout(1, 6, 0, 0));

			JLabel lblDieType = new JLabel("Die Type");
			lblDieType.setForeground(new Color(112, 128, 144));
			lblDieType.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblDieType.setVerticalAlignment(SwingConstants.BOTTOM);
			lblDieType.setHorizontalAlignment(SwingConstants.CENTER);
			panelLabels.add(lblDieType);

			JLabel lblDieCode = new JLabel("Die Code");
			lblDieCode.setForeground(new Color(112, 128, 144));
			lblDieCode.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblDieCode.setVerticalAlignment(SwingConstants.BOTTOM);
			lblDieCode.setHorizontalAlignment(SwingConstants.CENTER);
			panelLabels.add(lblDieCode);

			JLabel lblModNumber = new JLabel("MOD Number");
			lblModNumber.setForeground(new Color(112, 128, 144));
			lblModNumber.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblModNumber.setVerticalAlignment(SwingConstants.BOTTOM);
			lblModNumber.setHorizontalAlignment(SwingConstants.CENTER);
			panelLabels.add(lblModNumber);

			JLabel lblRepeat = new JLabel("Repeat");
			lblRepeat.setForeground(new Color(112, 128, 144));
			lblRepeat.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblRepeat.setVerticalAlignment(SwingConstants.BOTTOM);
			lblRepeat.setHorizontalAlignment(SwingConstants.CENTER);
			panelLabels.add(lblRepeat);

			JLabel lblChecksComplete = new JLabel("Checks Complete");
			lblChecksComplete.setForeground(new Color(112, 128, 144));
			lblChecksComplete.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblChecksComplete.setVerticalAlignment(SwingConstants.BOTTOM);
			lblChecksComplete.setHorizontalAlignment(SwingConstants.CENTER);
			panelLabels.add(lblChecksComplete);

			JLabel lblName = new JLabel("Name");
			lblName.setForeground(new Color(112, 128, 144));
			lblName.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblName.setVerticalAlignment(SwingConstants.BOTTOM);
			lblName.setHorizontalAlignment(SwingConstants.CENTER);
			panelLabels.add(lblName);
		}

		JPanel panelInput = new JPanel();
		panelInput.setBounds(10, 97, 694, 26);
		panelBackground.add(panelInput);
		panelInput.setLayout(new GridLayout(0, 6, 0, 0));

		cboDieType = new JComboBox();
		cboDieType.setModel(new DefaultComboBoxModel(new String[] { "Internal",
				"External" }));
		cboDieType.setSelectedIndex(0);
		panelInput.add(cboDieType);

		txtDieCode = new JTextField();
		panelInput.add(txtDieCode);
		txtDieCode.setColumns(10);

		txtMOD = new JTextField();
		panelInput.add(txtMOD);
		txtMOD.setColumns(10);

		cboRepeat = new JComboBox(DatabaseConnection.getRepeatList().toArray());
		cboRepeat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				
					if (cboRepeat.getSelectedItem().equals("Other")) {
						
						repeat = JOptionPane.showInputDialog("Enter repeat");
						
						if(repeat != null)
						{
							cboRepeat.setEditable(true);
							cboRepeat.setSelectedItem(repeat);
							cboRepeat.setEditable(false);
						}
						else
						{
							cboRepeat.setEditable(true);
							cboRepeat.setSelectedItem("508");
							cboRepeat.setEditable(false);
						}
					} else {
						repeat = cboRepeat.getSelectedItem().toString();
						System.out.println("here1");
					}
				
			}
		});
		panelInput.add(cboRepeat);
		cboRepeat.addItem("Other");

		checkBox = new JCheckBox("");
		checkBox.setHorizontalAlignment(SwingConstants.CENTER);
		panelInput.add(checkBox);

		txtName = new JTextField();
		panelInput.add(txtName);
		txtName.setColumns(10);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						if (isValidData()) {

							dispose();

							runProgressBar();

							new Thread(new Runnable() {

								@Override
								public void run() {
									SendMail.sendNewDieEmail(cboDieType
											.getSelectedItem().toString(),
											txtDieCode.getText(), repeat,
											txtMOD.getText(), txtName.getText());

									frame.dispose();

								}
							}).start();
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						clearFields();
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	/**
	 * 
	 * Resets the fields
	 */
	private void clearFields() {
		cboDieType.setSelectedIndex(0);
		txtDieCode.setText("");
		txtMOD.setText("");
		checkBox.setSelected(false);
		txtName.setText("");
	}

	/**
	 * Validates user input before sending email
	 * 
	 * @return
	 */
	private boolean isValidData() {
		if (txtDieCode.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Enter die code", "Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (txtMOD.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Enter mod number", "Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (!checkBox.isSelected()) {
			JOptionPane.showMessageDialog(null,
					"Confirm that you have completed the checks", "Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		if (txtName.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Enter your name", "Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	/**
	 * Starts progress bar
	 */
	private void runProgressBar() {
		frame = new GUI_ProgressBar();
		frame.setSize(380, 120);
		frame.setUndecorated(true);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}// End class
