/**
 * 
 */
package ie.rapdb;

import java.io.InputStream;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.swing.JOptionPane;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 * @author Corwin
 * 
 */

public class Reports {

	private static String jobRef;
	private static String yr;
	private static String colour;
	private static String reason;
	private static String name;

	public static void createColourReport(String jr, String yrNo, String col,
			String rsn, String nm, Date startDate, Date endDate) {

		jobRef = jr;
		yr = yrNo;
		colour = col;
		reason = rsn;
		name = nm;

		if (jobRef.equals("All")) {
			jobRef = "%";
		}
		if (yr.equals("All")) {
			yr = "%";
		}
		if (colour.equals("All")) {
			colour = "%";
		}
		if (reason.equals("All")) {
			reason = "%";
		}
		if (name.equals("All")) {
			name = "%";
		}
		if (startDate == null) {
			Calendar cal = Calendar.getInstance();
			cal.set(2013, 02, 01);
			startDate = cal.getTime();
		}
		if (endDate == null) {
			endDate = new Date();
		}

		try {

			java.sql.Date sqlStartDate = new java.sql.Date(startDate.getTime());
			java.sql.Date sqlEndDate = new java.sql.Date(endDate.getTime());

			Connection conn = DatabaseConnection.connect();
			
			InputStream input = Reports.class
					.getResourceAsStream("/jrxml/Colour_A4.jasper");

			HashMap map = new HashMap();
			map.put("JobRef", jobRef);
			map.put("YR", yr);
			map.put("Colour", colour);
			map.put("Reason", reason);
			map.put("Name", name);
			map.put("StartDate", sqlStartDate);
			map.put("EndDate", sqlEndDate);
			map.put("SUBREPORT_DIR", "http://10.20.1.251:8081/reports/");
			
			
			//map.put("subreportParameter", "/jrxml/ColourReasons.jasper");
			
			
//			JasperReport jasperReport = JasperCompileManager
//					.compileReport(input);
			
			
			
//			JasperDesign jasperDesign = JRXmlLoader.load(input);
//			JasperReport jasperReport = JasperCompileManager
//					.compileReport(jasperDesign);

			JasperPrint jasperPrint = JasperFillManager.fillReport(
					input, map, conn);
			JasperViewer.viewReport(jasperPrint, false);

			DatabaseConnection.closeConnection();

		} catch (JRException e) {

			JOptionPane.showMessageDialog(null, e.getMessage());

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}

	public static void createReport(String file, String jr, String yrNo,
			String rsn, String nm, Date startDate, Date endDate) {

		jobRef = jr;
		yr = yrNo;
		reason = rsn;
		name = nm;

		if (jobRef.equals("All")) {
			jobRef = "%";
		}
		if (yr.equals("All")) {
			yr = "%";
		}
		if (reason.equals("All")) {
			reason = "%";
		}
		if (name.equals("All")) {
			name = "%";
		}
		if (startDate == null) {
			Calendar cal = Calendar.getInstance();
			cal.set(2013, 02, 01);
			startDate = cal.getTime();
		}
		if (endDate == null) {
			endDate = new Date();
		}

		try {

			java.sql.Date sqlStartDate = new java.sql.Date(startDate.getTime());
			java.sql.Date sqlEndDate = new java.sql.Date(endDate.getTime());

			Connection conn = DatabaseConnection.connect();
			
			//java.io.InputStream image = Reports.class.getResourceAsStream("/image/rapLogoRed.png");

			HashMap map = new HashMap();
			map.put("ProductCode", jobRef);
			map.put("YR", yr);
			map.put("Reason", reason);
			map.put("Name", name);
			map.put("StartDate", sqlStartDate);
			map.put("EndDate", sqlEndDate);
			//map.put("Image", image);

			java.io.InputStream input = Reports.class.getResourceAsStream(file);
			JasperDesign jasperDesign = JRXmlLoader.load(input);
			JasperReport jasperReport = JasperCompileManager
					.compileReport(jasperDesign);

			JasperPrint jasperPrint = JasperFillManager.fillReport(
					jasperReport, map, conn);
			JasperViewer.viewReport(jasperPrint, false);

			DatabaseConnection.closeConnection();

		} catch (JRException e) {
			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void createDieReport(String dieType, String file,
			Date startDate, Date endDate) {

		try {

			java.sql.Date sqlStartDate = new java.sql.Date(startDate.getTime());
			java.sql.Date sqlEndDate = new java.sql.Date(endDate.getTime());

			HashMap map = new HashMap();

			if (dieType.equals("internal")) {
				map.put("StartDate", sqlStartDate);
				map.put("EndDate", sqlEndDate);
			} else {
				map.put("StartDate", sqlStartDate);
				map.put("EndDate1", sqlEndDate);
			}

			Connection conn = DatabaseConnection.connect();

			java.io.InputStream input = Reports.class.getResourceAsStream(file);
			JasperDesign jasperDesign = JRXmlLoader.load(input);
			JasperReport jasperReport = JasperCompileManager
					.compileReport(jasperDesign);

			JasperPrint jasperPrint = JasperFillManager.fillReport(
					jasperReport, map, conn);
			JasperViewer.viewReport(jasperPrint, false);

			DatabaseConnection.closeConnection();

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void createReorderedDieReport(String file, Date startDate,
			Date endDate) {

		try {

			java.sql.Date sqlStartDate = new java.sql.Date(startDate.getTime());
			java.sql.Date sqlEndDate = new java.sql.Date(endDate.getTime());

			HashMap map = new HashMap();

			map.put("StartDate", sqlStartDate);
			map.put("EndDate", sqlEndDate);

			Connection conn = DatabaseConnection.connect();

			java.io.InputStream input = Reports.class.getResourceAsStream(file);
			JasperDesign jasperDesign = JRXmlLoader.load(input);
			JasperReport jasperReport = JasperCompileManager
					.compileReport(jasperDesign);

			JasperPrint jasperPrint = JasperFillManager.fillReport(
					jasperReport, map, conn);
			JasperViewer.viewReport(jasperPrint, false);

			DatabaseConnection.closeConnection();

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void createDieDataReport(String dieType, String manu,
			String dieCode, String mod, String qty, String cost) {

		try {

			HashMap map = new HashMap();
			map.put("DieType", dieType);
			map.put("Manufacturer", manu);
			map.put("DieCode", dieCode);
			map.put("MOD", mod);
			map.put("Qty", qty);
			map.put("Cost", cost);

			java.io.InputStream input = Reports.class
					.getResourceAsStream("/jrxml/DieData_A4.jrxml");
			JasperDesign jasperDesign = JRXmlLoader.load(input);
			JasperReport jasperReport = JasperCompileManager
					.compileReport(jasperDesign);

			JasperPrint jasperPrint = JasperFillManager.fillReport(
					jasperReport, map, new JREmptyDataSource());
			JasperViewer.viewReport(jasperPrint, false);

		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	public static void createMostOrderedPlates(Date startDate, Date endDate) {

		if (startDate == null) {
			Calendar cal = Calendar.getInstance();
			cal.set(2013, 02, 01);
			startDate = cal.getTime();
		}
		if (endDate == null) {
			endDate = new Date();
		}

		try {

			java.sql.Date sqlStartDate = new java.sql.Date(startDate.getTime());
			java.sql.Date sqlEndDate = new java.sql.Date(endDate.getTime());

			Connection conn = DatabaseConnection.connect();
			
			InputStream input = Reports.class
					.getResourceAsStream("/jrxml/MostOrderedPlates_A4.jasper");

			HashMap map = new HashMap();
			map.put("StartDate", sqlStartDate);
			map.put("EndDate", sqlEndDate);
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(
					input, map, conn);
			JasperViewer.viewReport(jasperPrint, false);

			DatabaseConnection.closeConnection();

		} catch (JRException e) {

			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void createHighestReasons(Date startDate, Date endDate) {

		if (startDate == null) {
			Calendar cal = Calendar.getInstance();
			cal.set(2013, 02, 01);
			startDate = cal.getTime();
		}
		if (endDate == null) {
			endDate = new Date();
		}

		try {

			java.sql.Date sqlStartDate = new java.sql.Date(startDate.getTime());
			java.sql.Date sqlEndDate = new java.sql.Date(endDate.getTime());

			Connection conn = DatabaseConnection.connect();
			
			InputStream input = Reports.class
					.getResourceAsStream("/jrxml/HighestReasons_A4.jasper");

			HashMap map = new HashMap();
			map.put("StartDate", sqlStartDate);
			map.put("EndDate", sqlEndDate);
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(
					input, map, conn);
			JasperViewer.viewReport(jasperPrint, false);

			DatabaseConnection.closeConnection();

		} catch (JRException e) {

			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void createSpilkerVsRoto() {

		try {

			Connection conn = DatabaseConnection.connect();
			
			InputStream input = Reports.class
					.getResourceAsStream("/jrxml/SpilkerVsRoto.jrxml");
			
			JasperDesign jasperDesign = JRXmlLoader.load(input);
			JasperReport jasperReport = JasperCompileManager
					.compileReport(jasperDesign);

			JasperPrint jasperPrint = JasperFillManager.fillReport(
					jasperReport, null, new JREmptyDataSource());
			JasperViewer.viewReport(jasperPrint, false);

			DatabaseConnection.closeConnection();

		} catch (JRException e) {

			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 
	 */
	public static void createColourStockCheckForm() {

		try {
		
			Connection conn = DatabaseConnection.connect();
			
			InputStream input = Reports.class
					.getResourceAsStream("/jrxml/StockCheck.jasper");

			JasperPrint jasperPrint = JasperFillManager.fillReport(
					input, null, conn);
			JasperViewer.viewReport(jasperPrint, false);

			DatabaseConnection.closeConnection();

		} catch (JRException e) {

			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void createInternalStockCheckForm() {

		try {
		
			Connection conn = DatabaseConnection.connect();
			
			InputStream input = Reports.class
					.getResourceAsStream("/jrxml/InternalStockNew_A4.jasper");

			JasperPrint jasperPrint = JasperFillManager.fillReport(
					input, null, conn);
			JasperViewer.viewReport(jasperPrint, false);

			DatabaseConnection.closeConnection();

		} catch (JRException e) {

			//e.printStackTrace();

		} catch (Exception e) {
			//e.printStackTrace();
		}
	}

	public static void createExternalStockCheckForm() {

		try {
		
			Connection conn = DatabaseConnection.connect();
			
			InputStream input = Reports.class
					.getResourceAsStream("/jrxml/ExternalStockNew_A4.jasper");

			JasperPrint jasperPrint = JasperFillManager.fillReport(
					input, null, conn);
			JasperViewer.viewReport(jasperPrint, false);

			DatabaseConnection.closeConnection();

		} catch (JRException e) {

			//e.printStackTrace();

		} catch (Exception e) {
			//e.printStackTrace();
		}
	}


}
