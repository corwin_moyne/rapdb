package ie.rapdb;
/**
 * 
 */

/**
 * @author Corwin
 *
 */
public class StockCheck {
	
	private String reference;
	private String plate;
	
	public StockCheck(String reference, String plate) {
		this.reference = reference;
		this.plate = plate;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getPlate() {
		return plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

	@Override
	public String toString() {
		return "StockCheck [reference=" + reference + ", plate=" + plate + "]";
	}
	
	

}
