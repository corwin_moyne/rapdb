/**
 * 
 */
package ie.rapdb;

/**
 * @author Corwin
 *
 */
public class CommonPlate {
	
	private int id;
	private String yr;
	private String plateRef;
	private String plateType;
	private int stock;
	
	public CommonPlate(){}
	
	/**
	 * @param id
	 * @param yr
	 */
	
	public CommonPlate(int id, String yr) {
		this.id = id;
		this.yr = yr;
		this.plateRef = null;
		this.plateType = null;
		this.stock = 0;
	}
	
	/**
	 * @param id
	 * @param yr
	 * @param plateRef
	 * @param plateType
	 * @param stock
	 */
	
	public CommonPlate(int id, String yr, String plateRef, 
			String plateType, int stock) {
		this.id = id;
		this.yr = yr;
		this.plateRef = plateRef;
		this.plateType = plateType;
		this.stock = stock;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the yr
	 */
	public String getYr() {
		return yr;
	}

	/**
	 * @param yr the yr to set
	 */
	public void setYr(String yr) {
		this.yr = yr;
	}

	/**
	 * @return the plateRef
	 */
	public String getPlateRef() {
		return plateRef;
	}

	/**
	 * @param plateRef the plateRef to set
	 */
	public void setPlateRef(String plateRef) {
		this.plateRef = plateRef;
	}

	/**
	 * @return the plateType
	 */
	public String getPlateType() {
		return plateType;
	}

	/**
	 * @param plateType the plateType to set
	 */
	public void setPlateType(String plateType) {
		this.plateType = plateType;
	}

	/**
	 * @return the stock
	 */
	public int getStock() {
		return stock;
	}

	/**
	 * @param stock the stock to set
	 */
	public void setStock(int stock) {
		this.stock = stock;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CommonPlate [id=" + id + ", yr=" + yr + ", plateRef="
				+ plateRef + ", plateType="
				+ plateType + ", stock=" + stock + "]";
	}
}
