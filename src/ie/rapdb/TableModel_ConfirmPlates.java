/**
 * 
 */
package ie.rapdb;

import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.table.AbstractTableModel;

/**
 * @author Corwin
 *
 */
public class TableModel_ConfirmPlates extends AbstractTableModel {
	
	private ArrayList<UnconfirmedPlates> unconfirmedPlates;

	private String[] columnNames = { "Plate Type", "Reference", "YR Number", "Colour", "Order Qty",
			"Reason", "Timeline", "Name", "Order" };
	
	public TableModel_ConfirmPlates(ArrayList<UnconfirmedPlates> unconfirmedPlates)
	{
		this.unconfirmedPlates = unconfirmedPlates;
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return unconfirmedPlates.size();
	}
	
	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	/* (non-Javadoc)
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int row, int col) {
		
		UnconfirmedPlates u = unconfirmedPlates.get(row);
		
		switch (col) {

		case 0:
			return u.getPlateType();
		case 1:
			return u.getReference();
		case 2:
			return u.getYr();
		case 3:
			return u.getColour();
		case 4:
			return u.getQty();
		case 5:
			return u.getReason();
		case 6:
			return u.getTimeline();
		case 7:
			return u.getName();
		case 8:
			return u.isConfirmed();
		}

		return null;
	}
	
	@Override
	public void setValueAt(Object value, int row, int col) {

		UnconfirmedPlates u = unconfirmedPlates.get(row);

		switch (col) {

		case 4:
			u.setQty(Integer.valueOf((String)value));
			break;
		case 5:
			if (value.equals("Other")) {
				unconfirmedPlates.get(row).getReason();
				break;
			} else {
				unconfirmedPlates.get(row).setReason((String) value);
				break;
			}
		case 6:
			u.setTimeline((String) value);
			break;
		case 8:
			u.setConfirmed((boolean) value);
			break;
		}

		DatabaseConnection.updateUnconfirmedPlates(u);
	}
	
	@Override
	public Class<?> getColumnClass(int col) {

		Class[] columns = new Class[] { String.class, String.class,
				String.class, String.class, Integer.class,
				String.class, String.class, String.class, Boolean.class };

		return columns[col];
	}
	
	@Override
	public boolean isCellEditable(int row, int col) {

		if(col == 4 || col == 5 || col == 6 || col == 8)
		{
			return true;
		}
		return false;
	}

}
