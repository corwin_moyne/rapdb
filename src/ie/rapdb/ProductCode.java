/**
 * 
 */
package ie.rapdb;

/**
 * @author Corwin
 *
 */
public class ProductCode {
    
    private String productCode;
    private String size;
    private String intDC;
    private String extDC;
    private String antimistYr;
    private String adhesiveYr;
    private String datalaseYr;
    private String otherYr;
    
    /**
     * @param size
     * 		The size of the product
     * @param intDC
     * 		The internal die code
     * @param extDC
     * 		The external die code
     */
    public ProductCode(String size, String intDC, String extDC) {

	this.size = size;
	this.intDC = intDC;
	this.extDC = extDC;
    }

    public ProductCode() {
	
	size = new String();
	intDC = new String();
	extDC = new String();
    }
    
    

    /**
     * @param productCode
     * @param size
     * @param intDC
     * @param extDC
     * @param antimistYr
     * @param adhesiveYr
     * @param datalaseYr
     * @param otherYr
     */
    public ProductCode(String productCode, String size, String intDC,
	    String extDC, String antimistYr, String adhesiveYr,
	    String datalaseYr, String otherYr) {
	
	this.productCode = productCode;
	this.size = size;
	this.intDC = intDC;
	this.extDC = extDC;
	this.antimistYr = antimistYr;
	this.adhesiveYr = adhesiveYr;
	this.datalaseYr = datalaseYr;
	this.otherYr = otherYr;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * @return the intDC
     */
    public String getIntDC() {
        return intDC;
    }

    /**
     * @param intDC the intDC to set
     */
    public void setIntDC(String intDC) {
        this.intDC = intDC;
    }

    /**
     * @return the extDC
     */
    public String getExtDC() {
        return extDC;
    }

    /**
     * @param extDC the extDC to set
     */
    public void setExtDC(String extDC) {
        this.extDC = extDC;
    }
    

    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    /**
     * @return the antimistYr
     */
    public String getAntimistYr() {
        return antimistYr;
    }

    /**
     * @param antimistYr the antimistYr to set
     */
    public void setAntimistYr(String antimistYr) {
        this.antimistYr = antimistYr;
    }

    /**
     * @return the adhesiveYr
     */
    public String getAdhesiveYr() {
        return adhesiveYr;
    }

    /**
     * @param adhesiveYr the adhesiveYr to set
     */
    public void setAdhesiveYr(String adhesiveYr) {
        this.adhesiveYr = adhesiveYr;
    }

    /**
     * @return the datalaseYr
     */
    public String getDatalaseYr() {
        return datalaseYr;
    }

    /**
     * @param datalaseYr the datalaseYr to set
     */
    public void setDatalaseYr(String datalaseYr) {
        this.datalaseYr = datalaseYr;
    }

    /**
     * @return the otherYr
     */
    public String getOtherYr() {
        return otherYr;
    }

    /**
     * @param otherYr the otherYr to set
     */
    public void setOtherYr(String otherYr) {
        this.otherYr = otherYr;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "ProductCode [productCode=" + productCode + ", size=" + size
		+ ", intDC=" + intDC + ", extDC=" + extDC + ", antimistYr="
		+ antimistYr + ", adhesiveYr=" + adhesiveYr + ", datalaseYr="
		+ datalaseYr + ", otherYr=" + otherYr + "]";
    }

    
    
    
    
    

}
