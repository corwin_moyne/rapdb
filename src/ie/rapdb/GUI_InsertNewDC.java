package ie.rapdb;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.border.TitledBorder;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;

public class GUI_InsertNewDC extends JDialog {

    /**
     * JPanel
     */
    private final JPanel contentPanel = new JPanel();
    private JPanel panel1, panel2;

    /**
     * JTextField
     */
    private JTextField txtDC;

    /**
     * JCheckbox
     */
    private JCheckBox chbInternal, chbExternal;

    /**
     * JComboBox
     */
    private JComboBox cboRepeat;
    private JLabel lblDc;

    /**
     * Create the dialog.
     */
    public GUI_InsertNewDC() {
	setBounds(100, 100, 450, 300);
	getContentPane().setLayout(new BorderLayout());
	contentPanel.setBackground(new Color(112, 128, 144));
	contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
	getContentPane().add(contentPanel, BorderLayout.CENTER);
	contentPanel.setLayout(new GridLayout(0, 2, 0, 0));

	panel1 = new JPanel();
	panel1.setBorder(new TitledBorder(null, "Please Select",
		TitledBorder.LEADING, TitledBorder.TOP, null, null));
	contentPanel.add(panel1);
	panel1.setLayout(new GridLayout(2, 1, 0, 0));

	chbInternal = new JCheckBox("Internal");
	chbInternal.setForeground(new Color(112, 128, 144));
	chbInternal.setFont(new Font("Tahoma", Font.PLAIN, 14));
	chbInternal.setHorizontalAlignment(SwingConstants.CENTER);
	panel1.add(chbInternal);

	chbExternal = new JCheckBox("External");
	chbExternal.setForeground(new Color(112, 128, 144));
	chbExternal.setFont(new Font("Tahoma", Font.PLAIN, 14));
	chbExternal.setHorizontalAlignment(SwingConstants.CENTER);
	panel1.add(chbExternal);

	panel2 = new JPanel();
	contentPanel.add(panel2);
	panel2.setLayout(null);

	JLabel lblSelectRepeat = new JLabel("Select Repeat");
	lblSelectRepeat.setForeground(new Color(112, 128, 144));
	lblSelectRepeat.setFont(new Font("Tahoma", Font.PLAIN, 14));
	lblSelectRepeat.setBounds(48, 46, 115, 31);
	lblSelectRepeat.setHorizontalAlignment(SwingConstants.CENTER);
	panel2.add(lblSelectRepeat);

	cboRepeat = new JComboBox(DatabaseConnection.getRepeatList().toArray());
	cboRepeat.setBounds(35, 78, 141, 20);
	panel2.add(cboRepeat);

	JLabel lblEnterDieCode = new JLabel("Enter Die Code");
	lblEnterDieCode.setForeground(new Color(112, 128, 144));
	lblEnterDieCode.setFont(new Font("Tahoma", Font.PLAIN, 14));
	lblEnterDieCode.setHorizontalAlignment(SwingConstants.CENTER);
	lblEnterDieCode.setBounds(48, 124, 99, 31);
	panel2.add(lblEnterDieCode);

	txtDC = new JTextField();
	txtDC.setBounds(61, 154, 86, 20);
	panel2.add(txtDC);
	txtDC.setColumns(10);
	
	lblDc = new JLabel("DC");
	lblDc.setHorizontalAlignment(SwingConstants.RIGHT);
	lblDc.setBounds(10, 154, 49, 20);
	panel2.add(lblDc);
	{
	    JPanel buttonPane = new JPanel();
	    buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
	    getContentPane().add(buttonPane, BorderLayout.SOUTH);
	    {
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {

			if (isValidData()) {

			    String dies = "";

			    if (chbInternal.isSelected()) {
				dies += " internal ";
			    }
			    if (chbExternal.isSelected()) {
				dies += " external ";
			    }

			    String message = "You wish to add: \n" + "DC"
				    + txtDC.getText() + "\nRepeat: "
				    + cboRepeat.getSelectedItem().toString()
				    + "\nDie Selection: " + dies;

			    JOptionPane.showConfirmDialog(null, message,
				    "Confirm", JOptionPane.OK_CANCEL_OPTION);

			    DatabaseConnection.insertDieCode(chbInternal.isSelected(),
				    chbExternal.isSelected(), txtDC.getText(), cboRepeat.getSelectedItem().toString());

			}
		    }
		});
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
	    }
	    {

		JButton cancelButton = new JButton("Close");
		cancelButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {

			dispose();
		    }
		});

		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
	    }
	}
    }

    /**
     * Checks if data entered is valid
     */
    protected boolean isValidData() {

	if (!chbInternal.isSelected() && !chbExternal.isSelected()) {
	    JOptionPane.showMessageDialog(null,
		    "You have not selected any dies", "Error",
		    JOptionPane.ERROR_MESSAGE);
	    return false;
	}

	if (txtDC.getText().equals("")) {
	    JOptionPane.showMessageDialog(null, "You must enter a dc number",
		    "Error", JOptionPane.ERROR_MESSAGE);
	    return false;
	}

	return true;
    }
}
