package ie.rapdb;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import java.util.Date;

import com.toedter.calendar.JDateChooser;
import java.awt.Color;
import java.awt.Font;

public class GUI_OrderDie extends JDialog {

	private JPanel contentPanel = new JPanel();
	private JTextField txtDieType;
	private JTextField txtDieCode;
	private JTextField txtMOD;
	private JTextField txtRepeat;
	private JTextField txtCost;
	private JComboBox cboManufacturer, cboQty;
	private JDateChooser dateChooserDueDate;
	private JComboBox cboReason;

	/**
	 * Create the dialog.
	 */
	public GUI_OrderDie(String manu, String dieType, String dieCode,
			String mod, String repeat) {

		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(112, 128, 144));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JPanel panelDieDetails = new JPanel();
		panelDieDetails.setSize(new Dimension(1024, 225));
		panelDieDetails.setBorder(new TitledBorder(null, "Die Details",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelDieDetails.setBounds(20, 47, 655, 63);
		contentPanel.add(panelDieDetails);
		panelDieDetails.setLayout(new GridLayout(2, 7, 0, 0));

		JLabel lblManufacturer = new JLabel("Manufacturer");
		lblManufacturer.setForeground(new Color(112, 128, 144));
		lblManufacturer.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblManufacturer.setHorizontalAlignment(SwingConstants.CENTER);
		panelDieDetails.add(lblManufacturer);

		JLabel lblDieType = new JLabel("Die Type");
		lblDieType.setForeground(new Color(112, 128, 144));
		lblDieType.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDieType.setHorizontalAlignment(SwingConstants.CENTER);
		panelDieDetails.add(lblDieType);

		JLabel lblDiecode = new JLabel("DieCode");
		lblDiecode.setForeground(new Color(112, 128, 144));
		lblDiecode.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDiecode.setHorizontalAlignment(SwingConstants.CENTER);
		panelDieDetails.add(lblDiecode);

		JLabel lblMod = new JLabel("MOD");
		lblMod.setForeground(new Color(112, 128, 144));
		lblMod.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMod.setHorizontalAlignment(SwingConstants.CENTER);
		panelDieDetails.add(lblMod);

		JLabel lblRepeat = new JLabel("Repeat");
		lblRepeat.setForeground(new Color(112, 128, 144));
		lblRepeat.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblRepeat.setHorizontalAlignment(SwingConstants.CENTER);
		panelDieDetails.add(lblRepeat);

		cboManufacturer = new JComboBox(DatabaseConnection.getManufacturer()
				.toArray());
		cboManufacturer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				calculatePrice();
			}
		});
		cboManufacturer.setSelectedItem(manu);
		panelDieDetails.add(cboManufacturer);

		txtDieType = new JTextField(dieType);
		txtDieType.setEditable(false);
		txtDieType.setHorizontalAlignment(SwingConstants.CENTER);
		panelDieDetails.add(txtDieType);
		txtDieType.setColumns(10);

		txtDieCode = new JTextField(dieCode);
		txtDieCode.setEditable(false);
		txtDieCode.setHorizontalAlignment(SwingConstants.CENTER);
		panelDieDetails.add(txtDieCode);
		txtDieCode.setColumns(10);

		txtMOD = new JTextField(mod);
		txtMOD.setEditable(false);
		txtMOD.setHorizontalAlignment(SwingConstants.CENTER);
		panelDieDetails.add(txtMOD);
		txtMOD.setColumns(10);

		txtRepeat = new JTextField(repeat);
		txtRepeat.setEditable(false);
		txtRepeat.setHorizontalAlignment(SwingConstants.CENTER);
		panelDieDetails.add(txtRepeat);
		txtRepeat.setColumns(10);

		JPanel panelCost = new JPanel();
		panelCost.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Cost",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelCost.setBounds(20, 132, 264, 63);
		contentPanel.add(panelCost);
		panelCost.setLayout(null);

		JLabel lblCost = new JLabel("Cost");
		lblCost.setForeground(new Color(112, 128, 144));
		lblCost.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCost.setHorizontalAlignment(SwingConstants.CENTER);
		lblCost.setBounds(160, 11, 86, 24);
		panelCost.add(lblCost);

		txtCost = new JTextField();
		txtCost.setHorizontalAlignment(SwingConstants.RIGHT);
		txtCost.setBounds(160, 36, 86, 20);
		panelCost.add(txtCost);
		txtCost.setColumns(10);

		JLabel lblQty = new JLabel("Qty");
		lblQty.setForeground(new Color(112, 128, 144));
		lblQty.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblQty.setBounds(10, 16, 140, 20);
		panelCost.add(lblQty);
		lblQty.setHorizontalAlignment(SwingConstants.CENTER);

		cboQty = new JComboBox();
		cboQty.setEditable(true);
		cboQty.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				calculatePrice();
			}
		});
		cboQty.setBounds(10, 36, 140, 20);
		panelCost.add(cboQty);
		cboQty.setModel(new DefaultComboBoxModel(new String[] { "1 PIECE",
				"2 PIECES", "REPEAT ORDER 1", "REPEAT ORDER 2" }));

		JPanel panelDueDate = new JPanel();
		panelDueDate.setBorder(new TitledBorder(null, "Due Date",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelDueDate.setBounds(538, 132, 137, 63);
		contentPanel.add(panelDueDate);
		panelDueDate.setLayout(null);

		dateChooserDueDate = new JDateChooser();
		dateChooserDueDate.setBounds(10, 36, 124, 20);
		panelDueDate.add(dateChooserDueDate);

		JPanel panelReason = new JPanel();
		panelReason.setBorder(new TitledBorder(null, "Reorder Reason",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelReason.setBounds(283, 132, 256, 63);
		contentPanel.add(panelReason);
		panelReason.setLayout(null);

		cboReason = new JComboBox(DatabaseConnection.getReasonsDies().toArray());
		cboReason.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (cboReason.getSelectedItem().equals("--ADD NEW--")) {

					try {
						String newReason = JOptionPane
								.showInputDialog("Enter new reason");

						if (!newReason.equals("")) {
							cboReason.setEditable(true);

							DatabaseConnection.insertReasonDies(newReason);

							cboReason.setModel(new DefaultComboBoxModel(
									DatabaseConnection.getReasonsDies()
											.toArray()));

							cboReason.setSelectedItem(newReason);
						}
					} catch (NullPointerException n) {
						// Do nothing
					}
				}
			}
		});
		cboReason.setBounds(10, 36, 236, 20);
		panelReason.add(cboReason);

		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		{
			JButton okButton = new JButton("OK");
			okButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					boolean internal = false, external = false;

					if (txtDieType.getText().equals("internal")) {
						internal = true;
					} else {
						external = true;
					}

					String qty = "2";

					if (cboQty.getSelectedItem().toString().equals("1 PIECE")
							|| cboQty.getSelectedItem().toString()
									.equals("REPEAT ORDER 1")) {
						qty = "1";
					}

					if (isValidData()) {
						// Add to database
						DatabaseConnection.insertMOD(internal, external,
								txtDieCode.getText(),
								(Date) dateChooserDueDate.getDate(),
								Double.parseDouble(txtCost.getText()),
								cboReason.getSelectedItem().toString(), Integer.parseInt(qty));

						// Update replacement ordered in database
						if (internal) {
							DatabaseConnection.updateReplacementOrdered(
									"internal", txtMOD.getText());
						} else {
							DatabaseConnection.updateReplacementOrdered(
									"external", txtMOD.getText());
						}

						dispose();
						Reports.createDieDataReport(txtDieType.getText(),
								cboManufacturer.getSelectedItem().toString(),
								txtDieCode.getText(), txtMOD.getText(), qty,
								txtCost.getText());
					}
				}
			});
			okButton.setActionCommand("OK");
			buttonPane.add(okButton);
			getRootPane().setDefaultButton(okButton);
		}
		{
			JButton cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					dispose();
				}
			});
			cancelButton.setActionCommand("Cancel");
			buttonPane.add(cancelButton);
		}

		calculatePrice();
	}

	/**
	 * Calculates the die price
	 */
	private void calculatePrice() {

		try {
			double cost = DatabaseConnection.getDiePrice(txtDieType.getText(),
					txtRepeat.getText(), cboQty.getSelectedItem().toString(),
					cboManufacturer.getSelectedItem().toString());

			txtCost.setText(String.valueOf(cost));
		} catch (NullPointerException n) {

		}

	}

	/**
	 * Validates user input
	 * 
	 * @return
	 */
	private boolean isValidData() {
		if (cboReason.getSelectedIndex() == 0) {
			JOptionPane.showMessageDialog(null, "You must select a reason",
					"Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		if (dateChooserDueDate.getDate() == null) {
			JOptionPane.showMessageDialog(null, "You must select a due date",
					"Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		return true;
	}
}
