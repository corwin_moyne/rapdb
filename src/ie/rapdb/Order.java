package ie.rapdb;

public class Order {
    
    protected String reference;
    protected String orderType;
    protected String orderName;
    protected String shiftCode;
    
    /**
     * 
     */
    public Order() {
	
	reference = new String();
	orderType = new String();
	orderName = new String();
	shiftCode = new String();
    }
    public Order(String reference, String orderName, String shiftCode) {
    	
    	this.reference = reference;
    	this.orderType = new String();
    	this.orderName = orderName;
    	this.shiftCode = shiftCode;
    }
    /**
     * @param orderType
     * @param orderName
     */
    public Order(String reference, String orderType, String orderName, String shiftCode) {
	
	this.reference = reference;
	this.orderType = orderType;
	this.orderName = orderName;
	this.shiftCode = shiftCode;
    }
    
    /**
     * @return the reference
     */
    public String getReference() {
        return reference;
    }
    /**
     * @param reference the reference to set
     */
    public void setReference(String reference) {
        this.reference = reference;
    }
    /**
     * @return the orderType
     */
    public String getOrderType() {
        return orderType;
    }
    /**
     * @param orderType the orderType to set
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
    /**
     * @return the orderName
     */
    public String getOrderName() {
        return orderName;
    }
    /**
     * @param orderName the orderName to set
     */
    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }
    /**
     * @return the shiftCode
     */
    public String getShiftCode() {
        return shiftCode;
    }
    /**
     * @param shiftCode the shiftCode to set
     */
    public void setShiftCode(String shiftCode) {
        this.shiftCode = shiftCode;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "Order [reference=" + reference + ", orderType=" + orderType
		+ ", orderName=" + orderName + ", shiftCode=" + shiftCode + "]";
    }
}
