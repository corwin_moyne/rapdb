/**
 * 
 */
package ie.rapdb;

import java.sql.Date;

/**
 * @author Corwin
 *
 */
public class PlateCheckin {
	
	private String reference;
	private String inkRef;
	private String plateType;
	private String yrNumber;
	private int stock;
	private Date checkinDate;
	private String checkinName;
	private String productCode;
	
	/**
	 * @param jobRef
	 * @param inkRef
	 * @param yrNumber
	 * @param stock
	 * @param checkinDate
	 * @param checkinName
	 */
	public PlateCheckin(String reference, int stock, String inkRef, String yrNumber,
			Date checkinDate, String checkinName) {
		this.reference = reference;
		this.inkRef = inkRef;
		this.plateType = new String();
		this.yrNumber = yrNumber;
		this.stock = stock;
		this.checkinDate = checkinDate;
		this.checkinName = checkinName;
	}
	
	/**
	 * @param reference
	 * @param plateType
	 * @param yrNumber
	 * @param stock
	 * @param checkinDate
	 * @param checkinName
	 */
	public PlateCheckin(String reference, String plateType, String yrNumber, 
			int stock, Date checkinDate, String checkinName, String productCode) {
		this.reference = reference;
		this.inkRef = new String();
		this.plateType = plateType;
		this.yrNumber = yrNumber;
		this.stock = stock;
		this.checkinDate = checkinDate;
		this.checkinName = checkinName;
		this.productCode = productCode;
	}

	/**
	 * @return the jobRef
	 */
	public String getJobRef() {
		
		if(reference == null)
		{
			return "";
		}
		
		return reference;
	}

	/**
	 * @param jobRef the jobRef to set
	 */
	public void setJobRef(String jobRef) {
		this.reference = jobRef;
	}

	/**
	 * @return the inkRef
	 */
	public String getInkRef() {
		return inkRef;
	}

	/**
	 * @param inkRef the inkRef to set
	 */
	public void setInkRef(String inkRef) {
		this.inkRef = inkRef;
	}
	
	/**
	 * @return the plateType
	 */
	public String getPlateType() {
		return plateType;
	}

	/**
	 * @param plateType the plateType to set
	 */
	public void setPlateType(String plateType) {
		this.plateType = plateType;
	}

	/**
	 * @return the yrNumber
	 */
	public String getYrNumber() {
		return yrNumber;
	}

	/**
	 * @param yrNumber the yrNumber to set
	 */
	public void setYrNumber(String yrNumber) {
		this.yrNumber = yrNumber;
	}

	/**
	 * @return the stock
	 */
	public int getStock() {
		return stock;
	}

	/**
	 * @param stock the stock to set
	 */
	public void setStock(int stock) {
		this.stock = stock;
	}

	/**
	 * @return the checkinDate
	 */
	public Date getCheckinDate() {
		return checkinDate;
	}

	/**
	 * @param checkinDate the checkinDate to set
	 */
	public void setCheckinDate(Date checkinDate) {
		this.checkinDate = checkinDate;
	}

	/**
	 * @return the checkinName
	 */
	public String getCheckinName() {
		return checkinName;
	}

	/**
	 * @param checkinName the checkinName to set
	 */
	public void setCheckinName(String checkinName) {
		this.checkinName = checkinName;
	}

	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		
		return productCode;
	}

	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PlateCheckin [jobRef=" + reference + ", inkRef=" + inkRef
				+ ", yrNumber=" + yrNumber + ", stock=" + stock
				+ ", checkinDate=" + checkinDate + ", checkinName="
				+ checkinName + "]";
	}
	
	

}
