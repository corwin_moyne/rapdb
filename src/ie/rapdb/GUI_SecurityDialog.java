/**
 * Corwin moyne
 * Takes in username and password
 */

package ie.rapdb;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class GUI_SecurityDialog extends JDialog {

    // ...a JPanel with a JTextField and JPasswordField...
    private JPanel jpTextFields;

    // ...and a JPanel with two JButtons
    private JPanel jpButtons;
    private JButton jbtOK, jbtCancel;
    private JTextField txtUsername;
    private JPasswordField textField;
    private JLabel lblPasswordError;
    private JLabel lblUsernameError;
    private static String userType = null;
    private boolean validData = false;

    // Constructor
    public GUI_SecurityDialog() {

	// Panel 2 - A JTextField and a JPasswordField
	jpTextFields = new JPanel();
	jpTextFields.setBackground(new Color(112, 128, 144));

	// jpButtons - Three JButtons
	jpButtons = new JPanel();

	// Add buttons to jpButtons
	jpButtons.add(jbtOK = new JButton("OK"));
	jbtOK.setFont(new Font("Arial", Font.PLAIN, 16));
	jpButtons.add(jbtCancel = new JButton("Cancel"));
	jbtCancel.setFont(new Font("Arial", Font.PLAIN, 16));
	jbtCancel.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		userType = null;
		dispose();
	    }
	});

	// Set up the JFrame
	getContentPane().add(jpTextFields, BorderLayout.CENTER);
	jpTextFields.setLayout(null);

	txtUsername = new JTextField();
	/**
	 * Validate username
	 */
	txtUsername.addFocusListener(new FocusAdapter() {
	    @Override
	    public void focusLost(FocusEvent arg0) {
		if (txtUsername.getText().equals("")) {
		    lblUsernameError.setText("Username cannot be blank");
		    txtUsername.setBackground(Color.RED);
		    validData = false;
		} else {
		    lblUsernameError.setText("");
		    txtUsername.setBackground(Color.WHITE);
		    validData = true;
		}
	    }
	});
	txtUsername.setBounds(103, 31, 200, 20);
	jpTextFields.add(txtUsername);
	txtUsername.setColumns(10);

	textField = new JPasswordField();
	textField.addFocusListener(new FocusAdapter() {
	    @Override
	    public void focusLost(FocusEvent arg0) {

		if (textField.getPassword().length == 0) {
		    lblPasswordError.setText("Password cannot be blank");
		    textField.setBackground(Color.RED);
		    validData = false;
		} else {
		    lblPasswordError.setText("");
		    textField.setBackground(Color.WHITE);
		    validData = true;
		}
	    }
	});
	textField.setColumns(10);
	textField.setBounds(103, 85, 200, 20);
	jpTextFields.add(textField);

	JLabel lblUsername = new JLabel("Username");
	lblUsername.setForeground(Color.WHITE);
	lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 14));
	lblUsername.setHorizontalAlignment(SwingConstants.CENTER);
	lblUsername.setBounds(0, 34, 99, 14);
	jpTextFields.add(lblUsername);

	JLabel lblPassword = new JLabel("Password");
	lblPassword.setForeground(Color.WHITE);
	lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
	lblPassword.setHorizontalAlignment(SwingConstants.CENTER);
	lblPassword.setBounds(0, 88, 99, 14);
	jpTextFields.add(lblPassword);

	lblPasswordError = new JLabel(" ");
	lblPasswordError.setForeground(Color.RED);
	lblPasswordError.setFont(new Font("Tahoma", Font.PLAIN, 14));
	lblPasswordError.setHorizontalAlignment(SwingConstants.CENTER);
	lblPasswordError.setBounds(103, 109, 200, 20);
	jpTextFields.add(lblPasswordError);

	lblUsernameError = new JLabel(" ");
	lblUsernameError.setHorizontalAlignment(SwingConstants.CENTER);
	lblUsernameError.setForeground(Color.RED);
	lblUsernameError.setFont(new Font("Tahoma", Font.PLAIN, 14));
	lblUsernameError.setBounds(103, 54, 200, 20);
	jpTextFields.add(lblUsernameError);
	getContentPane().add(jpButtons, BorderLayout.SOUTH);

	/**
	 * Validate user credentials
	 */
	jbtOK.addActionListener(new ActionListener() {
	    @Override
	    public void actionPerformed(ActionEvent e) {

		if (validData) {
		    
		    String validation = DatabaseConnection.validateUser(
			    txtUsername.getText(),
			    String.valueOf(textField.getPassword()));

		    // Validate user
		    if (validation.equals("Error")) {
			lblPasswordError
				.setText("Invalid username or password");
		    } else {
			lblPasswordError.setText("");
			userType = validation;
			txtUsername.setText("");
			textField.setText("");
			dispose();
		    }
		}

	    }
	});
	
	getRootPane().setDefaultButton(jbtOK);
    }//

    public static String getUserType() {
	return userType;
    }
}// End class