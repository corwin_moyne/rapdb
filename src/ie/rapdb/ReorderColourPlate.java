/**
 * 
 */
package ie.rapdb;

import java.sql.Timestamp;

/**
 * @author Corwin
 *
 */
public class ReorderColourPlate extends Plate{
	
	private String jobRef;
	private Timestamp timeStamp;
	private String name;
	private String shift;
	
	public ReorderColourPlate() {}
	
	public ReorderColourPlate(int id, String jobRef, String inkRef, String reorderReason,
			String yr_number, Timestamp timeStamp, String name, int orderQty, String shift) {
		
		super(id, inkRef, reorderReason, yr_number, orderQty);
		this.jobRef = jobRef;
		this.timeStamp = timeStamp;
		this.name = name;
		this.shift = shift;
	}

	/**
	 * @return the jobRef
	 */
	public String getJobRef() {
		return jobRef;
	}

	/**
	 * @param jobRef the jobRef to set
	 */
	public void setJobRef(String jobRef) {
		this.jobRef = jobRef;
	}

	/**
	 * @return the timeStamp
	 */
	public Timestamp getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(Timestamp timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the shift
	 */
	public String getShift() {
		return shift;
	}

	/**
	 * @param shift the shift to set
	 */
	public void setShift(String shift) {
		this.shift = shift;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ReorderColourPlate [jobRef=" + jobRef + ", timeStamp="
				+ timeStamp + ", name=" + name + ", shift=" + shift + "]";
	}
	
	

}
