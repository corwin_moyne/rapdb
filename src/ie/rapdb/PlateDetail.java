/**
 * 
 */
package ie.rapdb;

/**
 * @author Corwin
 * 
 */
public class PlateDetail {

	private int id;
	private String jobRefString;
	private String inkRefString;
	private String yrNumber;
	private int stock;

	/**
     * 
     */
	public PlateDetail() {

		this.id = 0;
		this.jobRefString = new String();
		this.inkRefString = new String();
		this.yrNumber = new String();
		this.stock = 0;
	}

	/**
	 * @param inkRefString
	 * @param yrNumber
	 */
	public PlateDetail(String inkRefString, String yrNumber) {

		this.id = 0;
		this.inkRefString = inkRefString;
		this.yrNumber = yrNumber;
		this.stock = 0;
	}

	/**
	 * @param id
	 * @param jobRefString
	 * @param inkRefString
	 * @param yrNumber
	 */
	public PlateDetail(int id, String jobRefString, String inkRefString,
			String yrNumber, int stock) {

		this.id = id;
		this.jobRefString = jobRefString;
		this.inkRefString = inkRefString;
		this.yrNumber = yrNumber;
		this.stock = stock;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the jobRefString
	 */
	public String getJobRefString() {
		return jobRefString;
	}

	/**
	 * @param jobRefString
	 *            the jobRefString to set
	 */
	public void setJobRefString(String jobRefString) {
		this.jobRefString = jobRefString;
	}

	/**
	 * @return the inkRefString
	 */
	public String getInkRefString() {
		return inkRefString;
	}

	/**
	 * @param inkRefString
	 *            the inkRefString to set
	 */
	public void setInkRefString(String inkRefString) {
		this.inkRefString = inkRefString;
	}

	/**
	 * @return the yrNumber
	 */
	public String getYrNumber() {
		return yrNumber;
	}

	/**
	 * @param yrNumber
	 *            the yrNumber to set
	 */
	public void setYrNumber(String yrNumber) {
		this.yrNumber = yrNumber;
	}
	
	

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Colour [id=" + id + ", jobRefString=" + jobRefString
				+ ", inkRefString=" + inkRefString + ", yrNumber=" + yrNumber
				+ "]";
	}
}
