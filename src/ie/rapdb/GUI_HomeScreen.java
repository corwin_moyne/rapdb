package ie.rapdb;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JDateChooserCellEditor;

public class GUI_HomeScreen extends JFrame {

	/**
	 * ArrayLists
	 */
	private static ArrayList<Die> internalDies;
	private static ArrayList<Die> externalDies;
	private ArrayList<Plate> plates;
	private ArrayList<Plate> colourPlatesForOrder;

	/**
	 * JPanels
	 */
	private JPanel contentPane, panelDiesByProductCode, panelControl,
			panelInfo;

	/**
	 * JTabbedPane
	 */
	private JTabbedPane internalTabbedPaneDies;

	/**
	 * JComboboxes
	 */
	private JComboBox cboProductCode, cboExtDC, cboInternalDC,
			cboColourComboBox;
	private static JComboBox cboMOD;
	private JComboBox cboPerformanceUpdate;
	private JComboBox cboPressUpdate;
	private JComboBox cboProductCodeOrder;
	private JComboBox cboShiftCodeOrder;
	private JComboBox cboReason;
	private JComboBox cboJobRefOrder;
	private JComboBox cboQtyCommonOrder;
	private JComboBox cboReasonCommonOrder, cboReferenceReport, cboYrReport,
			cboColourReport, cboReasonReport, cboNameReport,
			cboDecrementCommon;

	/**
	 * Text fields
	 */
	private JTextField txtSize, txtRepeat, txtInternalDC, txtExternalDC,
			txtSelectedIntMOD, txtSelectedExtMOD, txtInternalDC2;
	private static JTextField txtSelectedMOD2;
	private JTextField txtExtRepeat;
	private JTextField txtExtDC;
	private static JTextField txtExtSelectedMOD;
	private JTextField txtDcUpdate;
	private JTextField txtRevolutionsUpdate;
	private JTextField txtLifetimeStart;
	private JTextField txtLifeTimeEnd;
	private JTextField txtMeters;
	private JTextField txtJobRevolutions;
	private JTextField txtNameUpdate;
	private JTextField txtRepeatUpdate;
	private JTextField txtNameOrder;
	private JTextField txtYrCommon;

	/**
	 * JTables
	 */
	private JTable tableInternalMOD, tableExternalMOD, tableIntPerformance,
			tableExtPerformance;
	private static JTable tableInternalPerformance2;
	private static JTable tableExtMOD2;
	private static JTable tableExtPerformance2;
	private static JTable tableInternalDie2;
	private JTable tableOrderPlates;

	/**
	 * JLabels
	 */
	private JLabel lblProductCode, lblExternalDC, lblSize, lblInternalDieCode,
			lblRepeat, lblInternalDC2, lblSelectedMOD2, lblExternalDieCode,
			lblExtDC, lblCurrentRevolutions, lblLifetimeStart, lblLifetimeEnd,
			lblMeters, lblJobRevolutions, lblName, lblExtSelectedMOD,
			lblJobReferenceOrder, lblNameOrder, lblQuantityCommonOrder,
			lblYrReferenceCommon, lblReorderReasonCommon, lblCurrentlyConne,
			lblNotLoggedIn, lblColour, lblDecrement;

	/**
	 * JRadioButton
	 */
	private JRadioButton rdbtnInternalUpdate, rdbtnExternalUpdate, rdbtnColour,
			rdbtnVarnish, rdbtnAdhesive, rdbtnAntimist, rdbtnDatalase,
			rdbtnInternalDieReport, rdbtnExternalDieReport, rdBtnColourReport,
			rdBtnVarnishReport, rdBtnAntimistReport, rdBtnAdhesiveReport,
			rdBtnDatalaseReport, rdbtnRemovedDies, rdbtnReorderedDies;

	/**
	 * JButton
	 */
	private JButton btnRemoveFromProduction, btnCopy;

	/**
	 * JCheckBox
	 */
	private JCheckBox chckbxShowRemovedDies, chckbxUpdateCommentsOnly,
			chckbxShowDiesOn, chckbxstEmail, chckbxndEmail;

	/**
	 * JScrollPane
	 */
	private JScrollPane scrollPaneIntPerformance2, scrollPaneExtPerformance2,
			scrollPaneOrderPlates;

	/**
	 * Other
	 */

	private JDateChooser dateChooserUpdate, dateChooserStartDate,
			dateChooserEndDate, dateChooserStartDateReports,
			dateChooserEndDateReports;
	private JTextArea txtComments;
	private ButtonGroup bgUpdate = new ButtonGroup();
	private ButtonGroup bg = new ButtonGroup();
	private final ButtonGroup buttonGroup = new ButtonGroup();
	// private static SimpleDateFormat dateFormat = new
	// SimpleDateFormat("E d/M/y ");

	// Date endDate = rs.getDate("EndDate");
	// String fmEndDate = dateFormat.format(endDate);

	/**
	 * Global Variables
	 */
	private ColourOrder colourOrder;
	private JLabel lblDeliveryTimelineCommon;
	private JComboBox cboTimelineCommon;
	private Plate plate;
	private JLabel label;
	private String dieType = new String();
	private GUI_ProgressBar frame;
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JPanel panelSelection;
	private JPanel panelDieDataInput;
	private JLabel lblBookIn;
	private JLabel lblReportProblem;
	private JLabel lblLogin;
	private String userType;
	private JLabel lblTile1;
	private JLabel lblTile2;
	private JLabel lblTile3;
	private JLabel lblTile4;
	private JLabel lblTile5;
	private JPanel panelBackground;
	private javax.swing.Timer animator;
	private int count = 1;
	private JButton btnUpdateManually;
	private Die dieUpdate = null;
	private JPanel panelReorderCombos;
	private JPanel panelReorderDatePicker;
	private JLabel lblStartDateReports;
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	private String originalColour;
	private final ButtonGroup buttonGroup_3 = new ButtonGroup();
	private CommonPlate commonPlate;
	private int commonPlateID;
	private JPanel panelForms;
	private JButton btnNewButton;
	private JButton btnExternalAddMod;
	private JButton btnInternalAddMod;

	/**
	 * Launch the application.
	 */
	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// // new DatabaseConnection();
	// // UIManager
	// // .setLookAndFeel("com.jtattoo.plaf.mint.MintLookAndFeel");
	//
	// // GUI_HomeScreen frame = new GUI_HomeScreen();
	// // frame.setSize(1280, 730);
	// // frame.setVisible(true);
	// // frame.setLocationRelativeTo(null);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	/**
	 * Create the frame.
	 */
	public GUI_HomeScreen() {

		// new DatabaseConnection();
		// try
		// {
		// UIManager
		// .setLookAndFeel("com.jtattoo.plaf.mint.MintLookAndFeel");
		// }
		// catch(Exception e)
		// {
		//
		// }
		setResizable(false);
		setSize(new Dimension(1280, 730));
		setIconImage(Toolkit.getDefaultToolkit().getImage(
				GUI_HomeScreen.class.getResource("/image/rap logo red.png")));
		// setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		/**
		 * Create the GUI
		 */
		createGUI();

		/**
		 * Run on application start
		 */
		onApplicationStart();
	}

	/**
	 * Creates the GUI
	 */
	private void createGUI() {
		contentPane.setLayout(null);

		JPanel panelShortCuts = new JPanel();
		panelShortCuts.setBorder(new LineBorder(new Color(162, 182, 182), 5));
		panelShortCuts.setBackground(new Color(250, 249, 212));
		panelShortCuts.setBounds(1038, 75, 216, 585);
		contentPane.add(panelShortCuts);
		panelShortCuts.setLayout(new GridLayout(10, 1, 10, 0));

		JLabel lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon(GUI_HomeScreen.class
				.getResource("/image/rap_logo_white.png")));
		lblLogo.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblLogo.setHorizontalAlignment(SwingConstants.CENTER);
		panelShortCuts.add(lblLogo);

		lblLogin = new JLabel("Login");
		lblLogin.setForeground(new Color(113, 130, 153));
		lblLogin.setIcon(new ImageIcon(GUI_HomeScreen.class
				.getResource("/image/login.png")));
		lblLogin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				if (lblLogin.getText().equals("Login")) {

					openSecurityDialog();

					userType = GUI_SecurityDialog.getUserType();

					if (userType != null) {

						openGUI_AddUpdate(userType);
						lblLogin.setText("Open");
						lblNotLoggedIn.setText("Logout");
					}

				} else if (lblLogin.getText().equals("Open")) {
					openGUI_AddUpdate(userType);
				}
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {

				lblLogin.setForeground(Color.red);
			}

			@Override
			public void mouseExited(MouseEvent e) {

				lblLogin.setForeground(new Color(113, 130, 153));
			}
		});
		panelShortCuts.add(lblLogin);
		lblLogin.setHorizontalAlignment(SwingConstants.LEFT);
		lblLogin.setFont(new Font("Tahoma", Font.PLAIN, 16));

		lblBookIn = new JLabel("Book Die In");
		lblBookIn.setForeground(new Color(113, 130, 153));
		lblBookIn.setIcon(new ImageIcon(GUI_HomeScreen.class
				.getResource("/image/bookIn.png")));
		lblBookIn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				openGUI_BookDieIn();
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				lblBookIn.setForeground(Color.red);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				lblBookIn.setForeground(new Color(113, 130, 153));
			}
		});
		panelShortCuts.add(lblBookIn);
		lblBookIn.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblBookIn.setHorizontalAlignment(SwingConstants.LEFT);

		lblReportProblem = new JLabel("Report Problem");
		lblReportProblem.setForeground(new Color(113, 130, 153));
		lblReportProblem.setIcon(new ImageIcon(GUI_HomeScreen.class
				.getResource("/image/Caution.png")));
		lblReportProblem.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				lblReportProblem.setForeground(Color.red);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				lblReportProblem.setForeground(new Color(113, 130, 153));
			}

			@Override
			public void mouseClicked(MouseEvent arg0) {

				openGUI_ReportProblem();
			}
		});
		panelShortCuts.add(lblReportProblem);
		lblReportProblem.setHorizontalAlignment(SwingConstants.LEFT);
		lblReportProblem.setFont(new Font("Tahoma", Font.PLAIN, 16));

		JLabel lblJavaLogo = new JLabel("");
		lblJavaLogo.setHorizontalAlignment(SwingConstants.CENTER);
		panelShortCuts.add(lblJavaLogo);

		lblTile1 = new JLabel("");
		lblTile1.setIcon(new ImageIcon(GUI_HomeScreen.class
				.getResource("/image/tile1.png")));
		panelShortCuts.add(lblTile1);

		lblTile2 = new JLabel("");
		lblTile2.setIcon(new ImageIcon(GUI_HomeScreen.class
				.getResource("/image/tile2.png")));
		panelShortCuts.add(lblTile2);

		lblTile3 = new JLabel("");
		lblTile3.setIcon(new ImageIcon(GUI_HomeScreen.class
				.getResource("/image/tile3.png")));
		panelShortCuts.add(lblTile3);

		lblTile4 = new JLabel("");
		panelShortCuts.add(lblTile4);

		lblTile5 = new JLabel("");
		lblTile5.setIcon(new ImageIcon(GUI_HomeScreen.class
				.getResource("/image/java.png")));
		panelShortCuts.add(lblTile5);

		lblCurrentlyConne = new JLabel("Connected to server");
		lblCurrentlyConne.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCurrentlyConne.setHorizontalAlignment(SwingConstants.CENTER);
		lblCurrentlyConne.setIcon(new ImageIcon(GUI_HomeScreen.class
				.getResource("/image/layer1.png")));
		lblCurrentlyConne.setForeground(Color.WHITE);
		lblCurrentlyConne.setBounds(1038, 659, 216, 33);
		contentPane.add(lblCurrentlyConne);

		panelBackground = new JPanel();
		panelBackground.setBackground(new Color(112, 128, 144));
		panelBackground.setBounds(10, 11, 1254, 681);
		contentPane.add(panelBackground);
		panelBackground.setLayout(null);

		JTabbedPane mainTabbedPane = new JTabbedPane(JTabbedPane.TOP);
		mainTabbedPane.setForeground(new Color(112, 128, 144));
		mainTabbedPane.setBounds(10, 11, 1008, 638);
		panelBackground.add(mainTabbedPane);
		mainTabbedPane.setFont(new Font("Tahoma", Font.PLAIN, 16));
		JPanel panelPlates = new JPanel();
		mainTabbedPane
				.addTab("PLATES",
						new ImageIcon(GUI_HomeScreen.class
								.getResource("/image/plateNew.png")),
						panelPlates, null);
		panelPlates.setLayout(null);

		JPanel panelRadioUpdate = new JPanel();
		panelRadioUpdate.setBorder(new LineBorder(new Color(128, 128, 128), 1,
				true));
		panelRadioUpdate.setBounds(35, 11, 922, 41);
		panelPlates.add(panelRadioUpdate);
		panelRadioUpdate.setLayout(new GridLayout(1, 6, 0, 0));

		rdbtnColour = new JRadioButton("Colour");
		rdbtnColour.setForeground(new Color(112, 128, 144));
		rdbtnColour.setFont(new Font("Tahoma", Font.PLAIN, 16));
		rdbtnColour.setHorizontalAlignment(SwingConstants.CENTER);
		rdbtnColour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				setStandardProductCodes(true);
				colour();
			}
		});
		rdbtnColour.setSelected(true);
		buttonGroup.add(rdbtnColour);
		panelRadioUpdate.add(rdbtnColour);

		rdbtnVarnish = new JRadioButton("Varnish");
		rdbtnVarnish.setForeground(new Color(112, 128, 144));
		rdbtnVarnish.setFont(new Font("Tahoma", Font.PLAIN, 16));
		rdbtnVarnish.setHorizontalAlignment(SwingConstants.CENTER);
		rdbtnVarnish.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				setStandardProductCodes(true);
				common();
				varnish();
			}
		});
		buttonGroup.add(rdbtnVarnish);
		panelRadioUpdate.add(rdbtnVarnish);

		rdbtnAntimist = new JRadioButton("Antimist");
		rdbtnAntimist.setForeground(new Color(112, 128, 144));
		rdbtnAntimist.setFont(new Font("Tahoma", Font.PLAIN, 16));
		rdbtnAntimist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				setStandardProductCodes(true);
				common();
				antimist();
			}
		});
		rdbtnAntimist.setHorizontalAlignment(SwingConstants.CENTER);
		buttonGroup.add(rdbtnAntimist);
		panelRadioUpdate.add(rdbtnAntimist);

		rdbtnAdhesive = new JRadioButton("Adhesive");
		rdbtnAdhesive.setForeground(new Color(112, 128, 144));
		rdbtnAdhesive.setFont(new Font("Tahoma", Font.PLAIN, 16));
		rdbtnAdhesive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				setStandardProductCodes(true);
				common();
				adhesive();
			}
		});
		rdbtnAdhesive.setHorizontalAlignment(SwingConstants.CENTER);
		buttonGroup.add(rdbtnAdhesive);
		panelRadioUpdate.add(rdbtnAdhesive);

		rdbtnDatalase = new JRadioButton("Datalase");
		rdbtnDatalase.setForeground(new Color(112, 128, 144));
		rdbtnDatalase.setFont(new Font("Tahoma", Font.PLAIN, 16));
		rdbtnDatalase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				setStandardProductCodes(false);
				common();
				datalase();
			}
		});
		rdbtnDatalase.setHorizontalAlignment(SwingConstants.CENTER);
		buttonGroup.add(rdbtnDatalase);
		panelRadioUpdate.add(rdbtnDatalase);

		cboProductCodeOrder = new JComboBox(DatabaseConnection
				.getProductCodes().toArray());
		cboProductCodeOrder.setMaximumRowCount(20);
		cboProductCodeOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cboJobRefOrder.setModel(new DefaultComboBoxModel(
						DatabaseConnection.getJobRefsPC(
								cboProductCodeOrder.getSelectedItem()
										.toString()).toArray()));

				// Colour
				if (rdbtnColour.isSelected()) {

					displayPlates();
				}

				// Varnish
				else if (rdbtnVarnish.isSelected()) {
					varnish();
				}

				// Antimist
				else if (rdbtnAntimist.isSelected()) {
					antimist();
				}
				// Adhesive
				else if (rdbtnAdhesive.isSelected()) {
					adhesive();
				}
				// Datalase
				else if (rdbtnDatalase.isSelected()) {
					datalase();
				}

			}
		});
		cboProductCodeOrder.setBounds(35, 125, 110, 20);
		panelPlates.add(cboProductCodeOrder);

		JLabel lblProductCodeOrder = new JLabel("Product Code");
		lblProductCodeOrder.setForeground(new Color(112, 128, 144));
		lblProductCodeOrder.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblProductCodeOrder.setBounds(35, 104, 110, 14);
		panelPlates.add(lblProductCodeOrder);

		try {
			cboJobRefOrder = new JComboBox(DatabaseConnection.getJobRefsPC(
					cboProductCodeOrder.getSelectedItem().toString()).toArray());

			cboJobRefOrder.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					if (rdbtnColour.isSelected()) {
						displayPlates();
					} else if (rdbtnVarnish.isSelected()) {
						varnish();
					}
				}
			});

			cboJobRefOrder.setMaximumRowCount(20);
			cboJobRefOrder.setBounds(35, 189, 110, 20);

			panelPlates.add(cboJobRefOrder);
		} catch (NullPointerException n) {
			// Do nothing
		}

		lblJobReferenceOrder = new JLabel("Job Reference");
		lblJobReferenceOrder.setForeground(new Color(112, 128, 144));
		lblJobReferenceOrder.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblJobReferenceOrder.setBounds(35, 170, 110, 14);
		panelPlates.add(lblJobReferenceOrder);

		scrollPaneOrderPlates = new JScrollPane();
		scrollPaneOrderPlates.setBackground(new Color(243, 245, 248));
		scrollPaneOrderPlates.setBounds(35, 270, 922, 212);
		panelPlates.add(scrollPaneOrderPlates);

		tableOrderPlates = new JTable();
		tableOrderPlates.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {

				try {
					originalColour = plates.get(
							tableOrderPlates.getSelectedRow()).getInkRef();
				} catch (ArrayIndexOutOfBoundsException a) {
					// Do nothing
				}
			}
		});

		tableOrderPlates.setFillsViewportHeight(true);
		scrollPaneOrderPlates.setViewportView(tableOrderPlates);

		txtNameOrder = new JTextField();
		txtNameOrder.setBounds(805, 189, 152, 20);
		panelPlates.add(txtNameOrder);
		txtNameOrder.setColumns(10);

		lblNameOrder = new JLabel("Name");
		lblNameOrder.setForeground(new Color(112, 128, 144));
		lblNameOrder.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNameOrder.setBounds(805, 168, 152, 14);
		panelPlates.add(lblNameOrder);

		JLabel lblShiftCodeOrder = new JLabel("Shift ");
		lblShiftCodeOrder.setForeground(new Color(112, 128, 144));
		lblShiftCodeOrder.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblShiftCodeOrder.setBounds(805, 104, 152, 14);
		panelPlates.add(lblShiftCodeOrder);

		cboShiftCodeOrder = new JComboBox();
		cboShiftCodeOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (cboShiftCodeOrder.getSelectedItem().toString()
						.equals("-ADD NEW-")) {
					cboShiftCodeOrder.setEditable(true);

					// Add new shift
					String shift = JOptionPane.showInputDialog(null,
							"Enter shift name");

					if (shift != null) {

						DatabaseConnection.insertShift(shift);

						populateShiftCbo();

						cboShiftCodeOrder.setSelectedItem(shift);
						cboShiftCodeOrder.setEditable(false);
					}
				}
			}
		});

		cboShiftCodeOrder.setMaximumRowCount(DatabaseConnection
				.getShiftDescription().size() + 1);
		cboShiftCodeOrder.setBounds(805, 125, 152, 20);
		panelPlates.add(cboShiftCodeOrder);

		JButton btnSubmitOrder = new JButton("Submit");
		btnSubmitOrder.setForeground(new Color(0, 0, 0));
		btnSubmitOrder.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnSubmitOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// Colour
				if (rdbtnColour.isSelected()) {
					if (validateColourPlateData() && validateShiftAndName()) {

						if (JOptionPane
								.showConfirmDialog(null, "Are you sure?", "?",
										JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

							createColourOrderAndUpdate();
						}
					}
				}

				// Varnish
				else if (rdbtnVarnish.isSelected()) {

					createCommonOrderAndUpdate("varnish");

				}

				// Antimist
				else if (rdbtnAntimist.isSelected()) {

					createCommonOrderAndUpdate("antimist");
				}

				// Adhesive
				else if (rdbtnAdhesive.isSelected()) {

					createCommonOrderAndUpdate("adhesive");
				}

				// Datalase
				else if (rdbtnDatalase.isSelected()) {

					createCommonOrderAndUpdate("datalase");
				}

			}
		});

		btnSubmitOrder.setBounds(868, 236, 89, 23);
		panelPlates.add(btnSubmitOrder);

		lblQuantityCommonOrder = new JLabel("Quantity");
		lblQuantityCommonOrder.setForeground(new Color(112, 128, 144));
		lblQuantityCommonOrder.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblQuantityCommonOrder.setBounds(368, 104, 110, 14);
		panelPlates.add(lblQuantityCommonOrder);

		cboQtyCommonOrder = new JComboBox(new Object[] {});
		cboQtyCommonOrder.setModel(new DefaultComboBoxModel(new String[] { "1",
				"2", "3", "4", "5" }));
		cboQtyCommonOrder.setSelectedIndex(0);
		cboQtyCommonOrder.setMaximumRowCount(20);
		cboQtyCommonOrder.setBounds(368, 125, 110, 20);
		panelPlates.add(cboQtyCommonOrder);

		txtYrCommon = new JTextField();
		txtYrCommon.setEditable(false);
		txtYrCommon.setBounds(197, 125, 110, 20);
		panelPlates.add(txtYrCommon);
		txtYrCommon.setColumns(10);

		lblYrReferenceCommon = new JLabel("YR Reference");
		lblYrReferenceCommon.setForeground(new Color(112, 128, 144));
		lblYrReferenceCommon.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblYrReferenceCommon.setBounds(197, 104, 110, 14);
		panelPlates.add(lblYrReferenceCommon);

		cboReasonCommonOrder = new JComboBox(DatabaseConnection.getReasons()
				.toArray());

		cboReasonCommonOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (cboReasonCommonOrder.getSelectedItem().toString()
						.equals("Other")) {
					try {
						String otherReason = JOptionPane
								.showInputDialog("Enter reason");
						cboReasonCommonOrder.setEditable(true);
						plate.setReorderReason(otherReason);
						cboReasonCommonOrder.setSelectedItem(otherReason);
					} catch (NullPointerException n) {
						n.printStackTrace();
						// cboReasonCommonOrder.setSelectedIndex(0);
					}

				}
			}
		});
		cboReasonCommonOrder.setMaximumRowCount(DatabaseConnection.getReasons()
				.size());
		cboReasonCommonOrder.setBounds(540, 125, 201, 20);
		panelPlates.add(cboReasonCommonOrder);

		lblReorderReasonCommon = new JLabel("Re-Order Reason");
		lblReorderReasonCommon.setForeground(new Color(112, 128, 144));
		lblReorderReasonCommon.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblReorderReasonCommon.setBounds(540, 104, 201, 14);
		panelPlates.add(lblReorderReasonCommon);

		lblDeliveryTimelineCommon = new JLabel("Delivery Timeline");
		lblDeliveryTimelineCommon.setForeground(new Color(112, 128, 144));
		lblDeliveryTimelineCommon.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDeliveryTimelineCommon.setBounds(540, 171, 201, 14);
		panelPlates.add(lblDeliveryTimelineCommon);

		cboTimelineCommon = new JComboBox(DatabaseConnection.getTimelines()
				.toArray());

		cboTimelineCommon.setMaximumRowCount(9);
		cboTimelineCommon.setBounds(540, 189, 201, 20);
		panelPlates.add(cboTimelineCommon);

		btnCopy = new JButton("Copy");
		btnCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (tableOrderPlates.getRowCount() > 0) {
					if (isValidCopy()) {
						int qty = (int) tableOrderPlates.getValueAt(0, 2);
						int dec = (int) tableOrderPlates.getValueAt(0, 3);
						String reason = (String) tableOrderPlates.getValueAt(0,
								4);
						String timeline = (String) tableOrderPlates.getValueAt(
								0, 5);

						for (Plate plate : plates) {
							plate.setOrderQty(qty);
							plate.setDecrement(dec);
							;
							plate.setReorderReason(reason);
							plate.setDeliveryTimeline(timeline);
							plate.setAddToOrder(true);
						}

						tableOrderPlates.setModel(new TableModel_ColourPlates(
								plates));
						createColourCombos();
					}
				}
			}
		});
		btnCopy.setBounds(868, 489, 89, 23);
		panelPlates.add(btnCopy);

		lblDecrement = new JLabel("Decrement");
		lblDecrement.setForeground(new Color(112, 128, 144));
		lblDecrement.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDecrement.setBounds(368, 172, 110, 14);
		panelPlates.add(lblDecrement);

		cboDecrementCommon = new JComboBox();
		cboDecrementCommon.setModel(new DefaultComboBoxModel(new String[] {
				"0", "1", "2", "3", "4", "5" }));
		cboDecrementCommon.setSelectedIndex(1);
		cboDecrementCommon.setBounds(368, 189, 110, 20);
		panelPlates.add(cboDecrementCommon);

		JPanel panelDies = new JPanel();
		mainTabbedPane.addTab(
				"DIES",
				new ImageIcon(GUI_HomeScreen.class
						.getResource("/image/die.png")), panelDies, null);

		internalTabbedPaneDies = new JTabbedPane(JTabbedPane.TOP);

		JPanel panelUpdate = new JPanel();
		internalTabbedPaneDies.addTab("Update Die", null, panelUpdate, null);
		panelUpdate.setLayout(null);

		panelInfo = new JPanel();
		panelInfo.setBorder(new TitledBorder(null, "Die information",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelInfo.setBounds(10, 67, 978, 467);
		panelUpdate.add(panelInfo);
		panelInfo.setLayout(null);

		panelDieDataInput = new JPanel();
		panelDieDataInput.setBounds(227, 52, 524, 332);
		panelInfo.add(panelDieDataInput);
		panelDieDataInput.setLayout(null);

		txtDcUpdate = new JTextField();
		txtDcUpdate.setBounds(0, 20, 113, 23);
		panelDieDataInput.add(txtDcUpdate);
		txtDcUpdate.setEditable(false);
		txtDcUpdate.setText("");
		txtDcUpdate.setColumns(10);

		JLabel lblDieCode_1 = new JLabel("Die Code");
		lblDieCode_1.setForeground(new Color(112, 128, 144));
		lblDieCode_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblDieCode_1.setBounds(0, 0, 113, 14);
		panelDieDataInput.add(lblDieCode_1);

		lblCurrentRevolutions = new JLabel("Current Revolutions");
		lblCurrentRevolutions.setForeground(new Color(112, 128, 144));
		lblCurrentRevolutions.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblCurrentRevolutions.setBounds(246, 0, 149, 14);
		panelDieDataInput.add(lblCurrentRevolutions);

		txtRevolutionsUpdate = new JTextField();
		txtRevolutionsUpdate.setBounds(246, 20, 149, 23);
		panelDieDataInput.add(txtRevolutionsUpdate);
		txtRevolutionsUpdate.setEditable(false);
		txtRevolutionsUpdate.setText("");
		txtRevolutionsUpdate.setColumns(10);

		lblLifetimeStart = new JLabel("Life Time Start");
		lblLifetimeStart.setForeground(new Color(112, 128, 144));
		lblLifetimeStart.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblLifetimeStart.setBounds(0, 157, 113, 14);
		panelDieDataInput.add(lblLifetimeStart);

		txtLifetimeStart = new JTextField();
		txtLifetimeStart.setBounds(0, 177, 113, 20);
		panelDieDataInput.add(txtLifetimeStart);
		txtLifetimeStart.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				String input = txtLifetimeStart.getText();

				char myChar = input.charAt(input.length() - 1);

				if (!Character.isDigit(myChar)) {
					JOptionPane.showMessageDialog(null, "Numbers only!",
							"Error", JOptionPane.ERROR_MESSAGE);
					txtLifetimeStart.setText(input.substring(0,
							input.length() - 1));
				}
			}
		});
		txtLifetimeStart.setHorizontalAlignment(JTextField.LEFT);
		txtLifetimeStart.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {

				calculateMeters();
			}
		});
		txtLifetimeStart.setText("");
		txtLifetimeStart.setColumns(10);

		lblLifetimeEnd = new JLabel("Life Time End");
		lblLifetimeEnd.setForeground(new Color(112, 128, 144));
		lblLifetimeEnd.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblLifetimeEnd.setBounds(123, 157, 113, 14);
		panelDieDataInput.add(lblLifetimeEnd);

		txtLifeTimeEnd = new JTextField();
		txtLifeTimeEnd.setBounds(123, 177, 113, 20);
		panelDieDataInput.add(txtLifeTimeEnd);
		txtLifeTimeEnd.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				String input = txtLifeTimeEnd.getText();

				char myChar = input.charAt(input.length() - 1);

				if (!Character.isDigit(myChar)) {
					JOptionPane.showMessageDialog(null, "Numbers only!",
							"Error", JOptionPane.ERROR_MESSAGE);
					txtLifeTimeEnd.setText(input.substring(0,
							input.length() - 1));
				}
			}
		});
		txtLifeTimeEnd.setHorizontalAlignment(JTextField.LEFT);
		txtLifeTimeEnd.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {

				calculateMeters();
			}
		});
		txtLifeTimeEnd.setText("");
		txtLifeTimeEnd.setColumns(10);

		lblMeters = new JLabel("Meters");
		lblMeters.setForeground(new Color(112, 128, 144));
		lblMeters.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblMeters.setBounds(0, 219, 113, 14);
		panelDieDataInput.add(lblMeters);

		txtMeters = new JTextField();
		txtMeters.setBounds(0, 239, 113, 20);
		panelDieDataInput.add(txtMeters);
		txtMeters.setText("");
		txtMeters.setEditable(false);
		txtMeters.setColumns(10);

		lblJobRevolutions = new JLabel("Job Revolutions");
		lblJobRevolutions.setForeground(new Color(112, 128, 144));
		lblJobRevolutions.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblJobRevolutions.setBounds(123, 219, 113, 14);
		panelDieDataInput.add(lblJobRevolutions);

		txtJobRevolutions = new JTextField();
		txtJobRevolutions.setBounds(123, 239, 113, 20);
		panelDieDataInput.add(txtJobRevolutions);
		txtJobRevolutions.setText("");
		txtJobRevolutions.setEditable(false);
		txtJobRevolutions.setColumns(10);

		dateChooserUpdate = new JDateChooser();
		dateChooserUpdate.setBounds(246, 177, 149, 20);
		panelDieDataInput.add(dateChooserUpdate);

		JLabel lblRunDate = new JLabel("Date");
		lblRunDate.setForeground(new Color(112, 128, 144));
		lblRunDate.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblRunDate.setBounds(246, 160, 149, 14);
		panelDieDataInput.add(lblRunDate);

		JLabel lblPerformance = new JLabel("Performance");
		lblPerformance.setForeground(new Color(112, 128, 144));
		lblPerformance.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPerformance.setBounds(0, 283, 113, 14);
		panelDieDataInput.add(lblPerformance);

		cboPerformanceUpdate = new JComboBox();
		cboPerformanceUpdate.setBounds(0, 300, 113, 20);
		panelDieDataInput.add(cboPerformanceUpdate);
		cboPerformanceUpdate.setModel(new DefaultComboBoxModel(new String[] {
				"Please Select", "Good", "Average", "Poor" }));
		cboPerformanceUpdate.setSelectedIndex(0);

		JLabel lblPress = new JLabel("Press");
		lblPress.setForeground(new Color(112, 128, 144));
		lblPress.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPress.setBounds(123, 283, 113, 14);
		panelDieDataInput.add(lblPress);

		cboPressUpdate = new JComboBox();
		cboPressUpdate.setBounds(123, 300, 113, 20);
		panelDieDataInput.add(cboPressUpdate);
		cboPressUpdate.setModel(new DefaultComboBoxModel(new String[] {
				"Please Select", "Comco", "Omet" }));
		cboPressUpdate.setSelectedIndex(0);

		lblName = new JLabel("Name");
		lblName.setForeground(new Color(112, 128, 144));
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblName.setBounds(405, 160, 119, 14);
		panelDieDataInput.add(lblName);

		txtNameUpdate = new JTextField();
		txtNameUpdate.setBounds(405, 177, 119, 20);
		panelDieDataInput.add(txtNameUpdate);
		txtNameUpdate.setText("");
		txtNameUpdate.setColumns(10);

		JLabel lblRepeatUpdate = new JLabel("Repeat");
		lblRepeatUpdate.setForeground(new Color(112, 128, 144));
		lblRepeatUpdate.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblRepeatUpdate.setBounds(123, 0, 113, 14);
		panelDieDataInput.add(lblRepeatUpdate);

		txtRepeatUpdate = new JTextField();
		txtRepeatUpdate.setBounds(123, 20, 113, 23);
		panelDieDataInput.add(txtRepeatUpdate);
		txtRepeatUpdate.setText("");
		txtRepeatUpdate.setEditable(false);
		txtRepeatUpdate.setColumns(10);

		JLabel lblComments = new JLabel("Comments");
		lblComments.setForeground(new Color(112, 128, 144));
		lblComments.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblComments.setBounds(246, 219, 113, 14);
		panelDieDataInput.add(lblComments);

		chckbxUpdateCommentsOnly = new JCheckBox("Update comments only");
		chckbxUpdateCommentsOnly.setForeground(new Color(112, 128, 144));
		chckbxUpdateCommentsOnly.setFont(new Font("Tahoma", Font.PLAIN, 16));
		chckbxUpdateCommentsOnly.setBounds(0, 90, 236, 23);
		panelDieDataInput.add(chckbxUpdateCommentsOnly);

		JScrollPane scrollPaneComments = new JScrollPane();
		scrollPaneComments.setBounds(246, 239, 278, 81);
		panelDieDataInput.add(scrollPaneComments);

		txtComments = new JTextArea();
		txtComments.setWrapStyleWord(true);
		txtComments.setLineWrap(true);
		scrollPaneComments.setViewportView(txtComments);

		btnUpdateManually = new JButton("Update Manually");
		btnUpdateManually.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int revs = Integer.valueOf(JOptionPane.showInputDialog(null,
						"Enter new revolutions"));

				if (rdbtnInternalUpdate.isSelected()) {
					DatabaseConnection.updateTotalRevolutionsManually(
							"internal", revs, dieUpdate.getId());
				} else {
					DatabaseConnection.updateTotalRevolutionsManually(
							"external", revs, dieUpdate.getId());
				}

				populateDcRevs();
			}
		});
		btnUpdateManually.setBounds(405, 20, 119, 23);
		panelDieDataInput.add(btnUpdateManually);
		chckbxUpdateCommentsOnly.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				toggleUpdateComponents();
			}
		});

		btnRemoveFromProduction = new JButton("Remove Die");
		btnRemoveFromProduction.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (validateTxtAndName()) {
					if (JOptionPane.showConfirmDialog(null,
							"Are you sure you want to remove this die?",
							"WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

						runProgressBar();

						new Thread(new Runnable() {
							public void run() {

								if (rdbtnInternalUpdate.isSelected()) {
									removeDie("internal", cboMOD.getSelectedItem().toString());
									
									

									GUI_Administrator
											.getRemovedMods("internal");

									SendMail.sendRemovedDieEmail("Internal",
											txtDcUpdate.getText(), cboMOD
													.getSelectedItem()
													.toString(),
											txtRevolutionsUpdate.getText(),
											txtComments.getText(),
											txtNameUpdate.getText());
									
									cboMOD.setModel(new DefaultComboBoxModel(
											DatabaseConnection.getIntMODs()
													.toArray()));
								} else {
									removeDie("external", cboMOD.getSelectedItem().toString());
									

									GUI_Administrator
											.getRemovedMods("external");

									SendMail.sendRemovedDieEmail("External",
											txtDcUpdate.getText(), cboMOD
													.getSelectedItem()
													.toString(),
											txtRevolutionsUpdate.getText(),
											txtComments.getText(),
											txtNameUpdate.getText());
									
									cboMOD.setModel(new DefaultComboBoxModel(
											DatabaseConnection.getExtMODs()
													.toArray()));
								}

								frame.dispose();

								JOptionPane.showMessageDialog(null,
										"Die removed", "Success",
										JOptionPane.INFORMATION_MESSAGE);

								populateDcRevs();

								resetShiftAndName();

								cancelUpdate();
							}
						}).start();

					}
				}
			}
		});
		btnRemoveFromProduction.setBounds(515, 395, 113, 23);
		panelInfo.add(btnRemoveFromProduction);

		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (validateUpdateData()) {

					if (rdbtnInternalUpdate.isSelected()) {
						dieType = "internal";
					} else {
						dieType = "external";
					}

					int jobRevs = Integer.parseInt(txtJobRevolutions.getText());

					DatabaseConnection.insertDiePerformance(dieType,
							(java.util.Date) dateChooserUpdate.getDate(),
							txtComments.getText(), cboPerformanceUpdate
									.getSelectedItem().toString(), cboMOD
									.getSelectedItem().toString(), jobRevs,
							txtNameUpdate.getText(), cboPressUpdate
									.getSelectedItem().toString());

					updateTotalRevolutions(dieType);

					populateDcRevs();

					new Thread(new Runnable() {
						public void run() {

							if (cboPerformanceUpdate.getSelectedItem().equals(
									"Poor")) {
								System.out.println("poor");
								SendMail.sendPoorPerformanceEmail(dieType,
										txtDcUpdate.getText(), cboMOD
												.getSelectedItem().toString(),
										txtRevolutionsUpdate.getText(),
										txtComments.getText(), txtNameUpdate
												.getText());
							}

							else if (Integer.parseInt(txtRevolutionsUpdate
									.getText()) > 500000
									&& !chckbxstEmail.isSelected()) {
								SendMail.sendDieUpdateEmail(500000, dieType,
										txtDcUpdate.getText(), cboMOD
												.getSelectedItem().toString(),
										txtRevolutionsUpdate.getText(),
										txtComments.getText(), txtNameUpdate
												.getText());

								DatabaseConnection.updateEmailSent(dieType,
										cboMOD.getSelectedItem().toString(),
										"first");
							}

							else if (Integer.parseInt(txtRevolutionsUpdate
									.getText()) > 300000
									&& !chckbxndEmail.isSelected()) {
								SendMail.sendDieUpdateEmail(300000, dieType,
										txtDcUpdate.getText(), cboMOD
												.getSelectedItem().toString(),
										txtRevolutionsUpdate.getText(),
										txtComments.getText(), txtNameUpdate
												.getText());

								DatabaseConnection.updateEmailSent(dieType,
										cboMOD.getSelectedItem().toString(),
										"second");
							}
							populateDcRevs();
							cancelUpdate();
						}
					}).start();

				}
			}
		});
		btnUpdate.setBounds(638, 395, 113, 23);
		panelInfo.add(btnUpdate);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cancelUpdate();
			}
		});
		btnCancel.setBounds(392, 395, 113, 23);
		panelInfo.add(btnCancel);

		rdbtnInternalUpdate = new JRadioButton("Internal");
		rdbtnInternalUpdate.setForeground(new Color(112, 128, 144));
		rdbtnInternalUpdate.setFont(new Font("Tahoma", Font.PLAIN, 16));
		rdbtnInternalUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cboMOD.setModel(new DefaultComboBoxModel(DatabaseConnection
						.getIntMODs().toArray()));
				populateDcRevs();
			}
		});
		rdbtnInternalUpdate.setSelected(true);
		bgUpdate.add(rdbtnInternalUpdate);
		rdbtnInternalUpdate.setBounds(10, 7, 115, 23);
		panelUpdate.add(rdbtnInternalUpdate);

		rdbtnExternalUpdate = new JRadioButton("External");
		rdbtnExternalUpdate.setForeground(new Color(112, 128, 144));
		rdbtnExternalUpdate.setFont(new Font("Tahoma", Font.PLAIN, 16));
		rdbtnExternalUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				cboMOD.setModel(new DefaultComboBoxModel(DatabaseConnection
						.getExtMODs().toArray()));
				populateDcRevs();
			}
		});
		bgUpdate.add(rdbtnExternalUpdate);
		rdbtnExternalUpdate.setBounds(127, 7, 137, 23);
		panelUpdate.add(rdbtnExternalUpdate);

		JLabel lblSelectMod = new JLabel("Select MOD");
		lblSelectMod.setForeground(new Color(112, 128, 144));
		lblSelectMod.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSelectMod.setBounds(10, 37, 97, 19);
		panelUpdate.add(lblSelectMod);

		cboMOD = new JComboBox();
		cboMOD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				populateDcRevs();
			}
		});
		cboMOD.setMaximumRowCount(30);
		cboMOD.setBounds(107, 37, 157, 20);
		panelUpdate.add(cboMOD);

		chckbxstEmail = new JCheckBox("1st email");
		chckbxstEmail.setVisible(false);
		chckbxstEmail.setBounds(350, 35, 97, 23);
		panelUpdate.add(chckbxstEmail);

		chckbxndEmail = new JCheckBox("2nd email");
		chckbxndEmail.setVisible(false);
		chckbxndEmail.setBounds(449, 35, 97, 23);
		panelUpdate.add(chckbxndEmail);

		JPanel panelDiesByDieCode = new JPanel();
		internalTabbedPaneDies.addTab("By Die Code", null, panelDiesByDieCode,
				null);
		panelDiesByDieCode.setLayout(null);

		lblInternalDieCode = new JLabel("Internal Die Code");
		lblInternalDieCode.setForeground(new Color(112, 128, 144));
		lblInternalDieCode.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblInternalDieCode.setBounds(10, 13, 125, 13);
		panelDiesByDieCode.add(lblInternalDieCode);

		cboInternalDC = new JComboBox();
		cboInternalDC.setMaximumRowCount(16);
		cboInternalDC.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getIntCodes().toArray()));
		cboInternalDC.setSelectedIndex(1);
		cboInternalDC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				populateIntData();
			}
		});
		cboInternalDC.setBounds(10, 28, 102, 20);
		panelDiesByDieCode.add(cboInternalDC);

		lblRepeat = new JLabel("Repeat");
		lblRepeat.setForeground(new Color(112, 128, 144));
		lblRepeat.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblRepeat.setBounds(361, 12, 119, 14);
		panelDiesByDieCode.add(lblRepeat);

		txtRepeat = new JTextField();
		txtRepeat.setEditable(false);
		txtRepeat.setBounds(361, 28, 119, 20);
		panelDiesByDieCode.add(txtRepeat);
		txtRepeat.setColumns(10);

		lblInternalDC2 = new JLabel("Internal Die Code");
		lblInternalDC2.setForeground(new Color(112, 128, 144));
		lblInternalDC2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblInternalDC2.setBounds(173, 11, 124, 14);
		panelDiesByDieCode.add(lblInternalDC2);

		txtInternalDC2 = new JTextField();
		txtInternalDC2.setEditable(false);
		txtInternalDC2.setColumns(10);
		txtInternalDC2.setBounds(173, 27, 124, 20);
		panelDiesByDieCode.add(txtInternalDC2);

		lblSelectedMOD2 = new JLabel("Selected MOD");
		lblSelectedMOD2.setForeground(new Color(112, 128, 144));
		lblSelectedMOD2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSelectedMOD2.setBounds(490, 11, 102, 14);
		panelDiesByDieCode.add(lblSelectedMOD2);

		txtSelectedMOD2 = new JTextField();
		txtSelectedMOD2.setEditable(false);
		txtSelectedMOD2.setColumns(10);
		txtSelectedMOD2.setBounds(490, 27, 102, 20);
		panelDiesByDieCode.add(txtSelectedMOD2);

		JScrollPane scrollPaneInternalDie = new JScrollPane();
		scrollPaneInternalDie.setBounds(10, 50, 470, 155);
		panelDiesByDieCode.add(scrollPaneInternalDie);

		tableInternalDie2 = new JTable();
		tableInternalDie2.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {

				if (chckbxShowRemovedDies.isSelected()) {
					JTable table = (JTable) e.getSource();
					Point point = e.getPoint();
					int row = table.rowAtPoint(point);
					table.setToolTipText((String) table.getValueAt(row, 4));
				}
			}
		});
		tableInternalDie2.setAutoCreateRowSorter(true);
		tableInternalDie2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				//Die die = internalDies.get(tableInternalDie2.getSelectedRow());
				
				Die die = internalDies.get(tableInternalDie2.convertRowIndexToModel(tableInternalDie2.getSelectedRow()));

				if (chckbxShowRemovedDies.isSelected()
						|| chckbxShowDiesOn.isSelected()) {

					String intDc = die.getDC_Number();

					displayIntRepeat(intDc);
					
					if(tableInternalDie2.getSelectedColumn() == 3)
					{
						if(JOptionPane.showConfirmDialog(null, "Are you sure?") == JOptionPane.YES_OPTION)
						{
						//Login
						if (userType == null) {
							openSecurityDialog();
							userType = GUI_SecurityDialog.getUserType();
							if (userType != null) {
								lblLogin.setText("Open");
								lblNotLoggedIn.setText("Logout");
								
								//Delete 
								DatabaseConnection.deleteMod("internal", die.getId());
							}
						}
						}
						else
						{
							//Delete 
							DatabaseConnection.deleteMod("internal", die.getId());
						}
						
						showDueDies();
					}
				}
				else {
					if (tableInternalDie2.getSelectedColumn() == 2) {
						// Order new die
						String manufacturer = new String();

						if (die.getModNo().startsWith("SB")) {
							manufacturer = "Spilker";
						} else {
							manufacturer = "Rotometrics";
						}

						openGUI_OrderDie(manufacturer, "internal",
								cboInternalDC.getSelectedItem().toString(),
								die.getModNo(), txtRepeat.getText().toString());
					}
					
				}

				String mod = die.getModNo();
				txtSelectedMOD2.setText(mod);
				displayIntPerformance2(mod);
			}
		});
		scrollPaneInternalDie.setViewportView(tableInternalDie2);

		scrollPaneIntPerformance2 = new JScrollPane();
		scrollPaneIntPerformance2.setBounds(490, 50, 498, 155);
		panelDiesByDieCode.add(scrollPaneIntPerformance2);

		tableInternalPerformance2 = new JTable();
		tableInternalPerformance2
				.addMouseMotionListener(new MouseMotionAdapter() {
					@Override
					public void mouseMoved(MouseEvent e) {

						JTable table = (JTable) e.getSource();
						Point point = e.getPoint();
						int row = table.rowAtPoint(point);
						table.setToolTipText((String) table.getValueAt(row, 2));
					}
				});
		tableInternalPerformance2.setAutoCreateRowSorter(true);
		scrollPaneIntPerformance2.setViewportView(tableInternalPerformance2);

		lblExtDC = new JLabel("External Die Code");
		lblExtDC.setForeground(new Color(112, 128, 144));
		lblExtDC.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblExtDC.setBounds(10, 250, 141, 13);
		panelDiesByDieCode.add(lblExtDC);

		cboExtDC = new JComboBox();
		cboExtDC.setMaximumRowCount(16);
		cboExtDC.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getExtCodes().toArray()));
		cboExtDC.setSelectedIndex(1);
		cboExtDC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				populateExtData();

				// if (rdbtnExternal.isSelected()) {
				// txtDieCode.setText(cboExtDC.getSelectedItem().toString());
				// }
			}
		});
		cboExtDC.setBounds(10, 265, 102, 20);
		panelDiesByDieCode.add(cboExtDC);

		JLabel lblExtRepeat = new JLabel("Repeat");
		lblExtRepeat.setForeground(new Color(112, 128, 144));
		lblExtRepeat.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblExtRepeat.setBounds(361, 250, 119, 14);
		panelDiesByDieCode.add(lblExtRepeat);

		txtExtRepeat = new JTextField();
		txtExtRepeat.setEditable(false);
		txtExtRepeat.setColumns(10);
		txtExtRepeat.setBounds(361, 265, 119, 20);
		panelDiesByDieCode.add(txtExtRepeat);

		lblExternalDieCode = new JLabel("External Die Code");
		lblExternalDieCode.setForeground(new Color(112, 128, 144));
		lblExternalDieCode.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblExternalDieCode.setBounds(173, 250, 141, 14);
		panelDiesByDieCode.add(lblExternalDieCode);

		txtExtDC = new JTextField();
		txtExtDC.setEditable(false);
		txtExtDC.setColumns(10);
		txtExtDC.setBounds(173, 265, 124, 20);
		panelDiesByDieCode.add(txtExtDC);

		lblExtSelectedMOD = new JLabel("Selected MOD");
		lblExtSelectedMOD.setForeground(new Color(112, 128, 144));
		lblExtSelectedMOD.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblExtSelectedMOD.setBounds(490, 250, 102, 14);
		panelDiesByDieCode.add(lblExtSelectedMOD);

		txtExtSelectedMOD = new JTextField();
		txtExtSelectedMOD.setEditable(false);
		txtExtSelectedMOD.setColumns(10);
		txtExtSelectedMOD.setBounds(490, 265, 102, 20);
		panelDiesByDieCode.add(txtExtSelectedMOD);

		JScrollPane scrollPaneExtMOD2 = new JScrollPane();
		scrollPaneExtMOD2.setBounds(10, 287, 470, 155);
		panelDiesByDieCode.add(scrollPaneExtMOD2);

		tableExtMOD2 = new JTable();
		tableExtMOD2.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {

				if (chckbxShowRemovedDies.isSelected()) {
					JTable table = (JTable) e.getSource();
					Point point = e.getPoint();
					int row = table.rowAtPoint(point);
					table.setToolTipText((String) table.getValueAt(row, 4));
				}
			}
		});
		tableExtMOD2.setAutoCreateRowSorter(true);
		tableExtMOD2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				Die die = null;
				try {
					//die = externalDies.get(tableExtMOD2.getSelectedRow());
					die = externalDies.get(tableExtMOD2.convertRowIndexToModel(tableExtMOD2.getSelectedRow()));
				} catch (IndexOutOfBoundsException i) {
					// Do nothing
				}

				if (chckbxShowRemovedDies.isSelected()
						|| chckbxShowDiesOn.isSelected()) {

					String dc = die.getDC_Number();
					displayExtRepeat(dc);
					
					if(tableExtMOD2.getSelectedColumn() == 3)
					{
						//Login
						if (userType == null) {
							openSecurityDialog();
							userType = GUI_SecurityDialog.getUserType();
							if (userType != null) {
								lblLogin.setText("Open");
								lblNotLoggedIn.setText("Logout");
								
								//Delete 
								DatabaseConnection.deleteMod("external", die.getId());
							}
						}
						else
						{
							//Delete 
							DatabaseConnection.deleteMod("external", die.getId());
						}
						
						showDueDies();
					}
				} else {
					if (tableExtMOD2.getSelectedColumn() == 2) {
						// Order new die
						String manufacturer = new String();

						if (die.getModNo().startsWith("SB")) {
							manufacturer = "Spilker";
						} else {
							manufacturer = "Rotometrics";
						}

						openGUI_OrderDie(manufacturer, "external", cboExtDC
								.getSelectedItem().toString(), die.getModNo(),
								txtExtRepeat.getText().toString());
					}
				}

				String mod = die.getModNo();
				txtExtSelectedMOD.setText(mod);
				displayExtPerformance2(mod);
			}
		});
		scrollPaneExtMOD2.setViewportView(tableExtMOD2);

		scrollPaneExtPerformance2 = new JScrollPane();
		scrollPaneExtPerformance2.setBounds(490, 287, 498, 155);
		panelDiesByDieCode.add(scrollPaneExtPerformance2);

		tableExtPerformance2 = new JTable();
		tableExtPerformance2.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {

				JTable table = (JTable) e.getSource();
				Point point = e.getPoint();
				int row = table.rowAtPoint(point);
				table.setToolTipText((String) table.getValueAt(row, 2));
			}
		});
		tableExtPerformance2.setAutoCreateRowSorter(true);
		scrollPaneExtPerformance2.setViewportView(tableExtPerformance2);

		panelControl = new JPanel();
		panelControl.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelControl.setBounds(155, 490, 688, 35);
		panelDiesByDieCode.add(panelControl);
		panelControl.setLayout(new GridLayout(1, 3, 0, 10));

		chckbxShowRemovedDies = new JCheckBox("Show Removed Dies Only");
		chckbxShowRemovedDies.setForeground(Color.BLACK);
		chckbxShowRemovedDies.setFont(new Font("Tahoma", Font.PLAIN, 16));
		chckbxShowRemovedDies.setHorizontalAlignment(SwingConstants.CENTER);
		panelControl.add(chckbxShowRemovedDies);

		chckbxShowDiesOn = new JCheckBox("Show dies on order");
		chckbxShowDiesOn.setForeground(Color.BLACK);
		chckbxShowDiesOn.setFont(new Font("Tahoma", Font.PLAIN, 16));
		chckbxShowDiesOn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (chckbxShowDiesOn.isSelected()) {
					showDueDies();
					toggleComponents(false);
					togglePerformanceData(false);
				} else {
					populateIntData();
					populateExtData();
					toggleComponents(true);
					togglePerformanceData(true);
				}
			}
		});
		chckbxShowDiesOn.setHorizontalAlignment(SwingConstants.CENTER);
		panelControl.add(chckbxShowDiesOn);

		btnNewButton = new JButton("Add DC Numbers");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				openGUI_InsertNewDC();
				cboInternalDC.setModel(new DefaultComboBoxModel(
						DatabaseConnection.getIntCodes().toArray()));
				cboInternalDC.setSelectedIndex(1);

				cboExtDC.setModel(new DefaultComboBoxModel(DatabaseConnection
						.getExtCodes().toArray()));
				cboExtDC.setSelectedIndex(1);

			}
		});
		panelControl.add(btnNewButton);

		btnExternalAddMod = new JButton("Add MOD");
		btnExternalAddMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (userType == null) {
					openSecurityDialog();
					userType = GUI_SecurityDialog.getUserType();
					if (userType != null) {
						lblLogin.setText("Open");
						lblNotLoggedIn.setText("Logout");
						addMod(false, true);
					}

				} else {
					addMod(false, true);
				}
			}
		});
		btnExternalAddMod.setBounds(391, 447, 89, 23);
		panelDiesByDieCode.add(btnExternalAddMod);

		btnInternalAddMod = new JButton("Add MOD");
		btnInternalAddMod.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (userType == null) {
					openSecurityDialog();
					userType = GUI_SecurityDialog.getUserType();
					if (userType != null) {
						lblLogin.setText("Open");
						lblNotLoggedIn.setText("Logout");
						addMod(true, false);
					}
				} else {
					addMod(true, false);
				}
			}
		});
		btnInternalAddMod.setBounds(391, 210, 89, 23);
		panelDiesByDieCode.add(btnInternalAddMod);
		chckbxShowRemovedDies.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (chckbxShowRemovedDies.isSelected()) {

					if (chckbxShowDiesOn.isSelected()) {
						chckbxShowDiesOn.setSelected(false);
					}

					toggleComponents(false);
					togglePerformanceData(true);

					displayRemovedInt();
					displayIntRepeat(internalDies.get(0).getDC_Number());
					txtSelectedMOD2.setText(tableInternalDie2.getValueAt(0, 1)
							.toString());
					displayIntPerformance2(txtSelectedMOD2.getText());

					displayRemovedExt();
					displayExtRepeat(externalDies.get(0).getDC_Number());
					txtExtSelectedMOD.setText(tableExtMOD2.getValueAt(0, 1)
							.toString());
					displayExtPerformance2(txtExtSelectedMOD.getText());

				} else {

					toggleComponents(true);

					displayIntDies2(cboInternalDC.getSelectedItem().toString());
					displayExtDies2(cboExtDC.getSelectedItem().toString());
				}

			}
		});

		panelDiesByProductCode = new JPanel();
		internalTabbedPaneDies.addTab("By Product Code", null,
				panelDiesByProductCode, null);

		cboProductCode = new JComboBox();
		cboProductCode.setMaximumRowCount(20);
		cboProductCode.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getProductCodes().toArray()));
		cboProductCode.setBounds(10, 25, 102, 20);

		cboProductCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				refineByProductCode();
			}
		});

		lblProductCode = new JLabel("Product Code");
		lblProductCode.setForeground(new Color(112, 128, 144));
		lblProductCode.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblProductCode.setBounds(10, 11, 102, 13);

		lblSize = new JLabel("Size");
		lblSize.setForeground(new Color(112, 128, 144));
		lblSize.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSize.setBounds(297, 10, 102, 14);

		txtSize = new JTextField();
		txtSize.setBounds(297, 25, 102, 20);
		txtSize.setEditable(false);
		txtSize.setColumns(10);

		JLabel lblInternalDC = new JLabel("Internal Die Code");
		lblInternalDC.setForeground(new Color(112, 128, 144));
		lblInternalDC.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblInternalDC.setBounds(10, 56, 131, 14);

		txtInternalDC = new JTextField();
		txtInternalDC.setBounds(10, 71, 102, 20);
		txtInternalDC.setEditable(false);
		txtInternalDC.setColumns(10);

		lblExternalDC = new JLabel("External Die Code");
		lblExternalDC.setForeground(new Color(112, 128, 144));
		lblExternalDC.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblExternalDC.setBounds(10, 304, 131, 14);

		txtExternalDC = new JTextField();
		txtExternalDC.setBounds(10, 319, 102, 20);
		txtExternalDC.setEditable(false);
		txtExternalDC.setColumns(10);

		JScrollPane internalDieScrollPane = new JScrollPane();
		internalDieScrollPane.setBounds(10, 97, 269, 155);

		JScrollPane externalDieScrollPane = new JScrollPane();
		externalDieScrollPane.setBounds(10, 345, 269, 155);

		tableExternalMOD = new JTable();
		tableExternalMOD.setAutoCreateRowSorter(true);
		tableExternalMOD.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				//Die die = externalDies.get(tableExternalMOD.getSelectedRow());
				Die die = externalDies.get(tableExternalMOD.convertRowIndexToModel(tableExternalMOD.getSelectedRow()));
				String mod = die.getModNo();
				txtSelectedExtMOD.setText(mod);
				displayExtPerformance(mod);
			}
		});
		externalDieScrollPane.setViewportView(tableExternalMOD);

		tableInternalMOD = new JTable();
		tableInternalMOD.setAutoCreateRowSorter(true);
		tableInternalMOD.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {

				//Die die = internalDies.get(tableInternalMOD.getSelectedRow());
				Die die = internalDies.get(tableInternalMOD.convertRowIndexToModel(tableInternalMOD.getSelectedRow()));
				String mod = die.getModNo();
				txtSelectedIntMOD.setText(mod);
				displayIntPerformance(txtSelectedIntMOD.getText());
			}
		});
		panelDiesByProductCode.setLayout(null);
		internalDieScrollPane.setViewportView(tableInternalMOD);
		panelDiesByProductCode.add(internalDieScrollPane);
		panelDiesByProductCode.add(txtInternalDC);
		panelDiesByProductCode.add(lblInternalDC);
		panelDiesByProductCode.add(lblProductCode);
		panelDiesByProductCode.add(cboProductCode);
		panelDiesByProductCode.add(lblSize);
		panelDiesByProductCode.add(txtSize);
		panelDiesByProductCode.add(lblExternalDC);
		panelDiesByProductCode.add(txtExternalDC);
		panelDiesByProductCode.add(externalDieScrollPane);

		JScrollPane scrollPaneIntPerformance = new JScrollPane();
		scrollPaneIntPerformance.setBounds(297, 97, 691, 155);
		panelDiesByProductCode.add(scrollPaneIntPerformance);

		tableIntPerformance = new JTable();
		tableIntPerformance.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {

				JTable table = (JTable) e.getSource();
				Point point = e.getPoint();
				int row = table.rowAtPoint(point);
				table.setToolTipText((String) table.getValueAt(row, 2));
			}
		});
		tableIntPerformance.setAutoCreateRowSorter(true);
		scrollPaneIntPerformance.setViewportView(tableIntPerformance);

		JLabel lblSelectedIntMOD = new JLabel("Selected MOD");
		lblSelectedIntMOD.setForeground(new Color(112, 128, 144));
		lblSelectedIntMOD.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSelectedIntMOD.setBounds(297, 56, 102, 14);
		panelDiesByProductCode.add(lblSelectedIntMOD);

		txtSelectedIntMOD = new JTextField();
		txtSelectedIntMOD.setBounds(297, 71, 102, 20);
		txtSelectedIntMOD.setEditable(false);
		txtSelectedIntMOD.setColumns(10);
		panelDiesByProductCode.add(txtSelectedIntMOD);

		JScrollPane scrollPaneExternalPerformance = new JScrollPane();
		scrollPaneExternalPerformance.setBounds(297, 345, 691, 155);
		panelDiesByProductCode.add(scrollPaneExternalPerformance);

		tableExtPerformance = new JTable();
		tableExtPerformance.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {

				JTable table = (JTable) e.getSource();
				Point point = e.getPoint();
				int row = table.rowAtPoint(point);
				table.setToolTipText((String) table.getValueAt(row, 2));
			}
		});
		tableExtPerformance.setAutoCreateRowSorter(true);
		scrollPaneExternalPerformance.setViewportView(tableExtPerformance);

		JLabel lblSelectedExtMOD = new JLabel("Selected MOD");
		lblSelectedExtMOD.setForeground(new Color(112, 128, 144));
		lblSelectedExtMOD.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSelectedExtMOD.setBounds(297, 304, 102, 14);
		panelDiesByProductCode.add(lblSelectedExtMOD);

		txtSelectedExtMOD = new JTextField();
		txtSelectedExtMOD.setBounds(297, 319, 102, 20);
		txtSelectedExtMOD.setEditable(false);
		txtSelectedExtMOD.setColumns(10);
		panelDiesByProductCode.add(txtSelectedExtMOD);
		GroupLayout gl_panelDies = new GroupLayout(panelDies);
		gl_panelDies.setHorizontalGroup(gl_panelDies.createParallelGroup(
				Alignment.LEADING).addComponent(internalTabbedPaneDies,
				GroupLayout.DEFAULT_SIZE, 1003, Short.MAX_VALUE));
		gl_panelDies.setVerticalGroup(gl_panelDies.createParallelGroup(
				Alignment.LEADING)
				.addGroup(
						gl_panelDies
								.createSequentialGroup()
								.addContainerGap()
								.addComponent(internalTabbedPaneDies,
										GroupLayout.DEFAULT_SIZE, 585,
										Short.MAX_VALUE)));

		panelDies.setLayout(gl_panelDies);

		JPanel panelReports = new JPanel();
		mainTabbedPane
				.addTab("Reports",
						new ImageIcon(GUI_HomeScreen.class
								.getResource("/image/reports.png")),
						panelReports, null);
		panelReports.setLayout(null);

		JTabbedPane reportsTabbedPane = new JTabbedPane(JTabbedPane.TOP);
		reportsTabbedPane.setBounds(10, 11, 973, 575);
		panelReports.add(reportsTabbedPane);

		JPanel panelReOrderedPlates = new JPanel();
		reportsTabbedPane.addTab("Plates", null, panelReOrderedPlates, null);
		panelReOrderedPlates.setLayout(null);

		JPanel panelOtherReports = new JPanel();
		panelOtherReports.setBounds(756, 184, 193, 205);
		panelReOrderedPlates.add(panelOtherReports);
		panelOtherReports.setLayout(new GridLayout(6, 1, 5, 10));

		JButton btnShowMostOrdered = new JButton("Most Ordered Plates");
		btnShowMostOrdered.setToolTipText("You can select dates if you wish");
		btnShowMostOrdered.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Reports.createMostOrderedPlates(
						(Date) dateChooserStartDateReports.getDate(),
						(Date) dateChooserEndDateReports.getDate());
			}
		});
		panelOtherReports.add(btnShowMostOrdered);

		JButton btnShowHighestReasons = new JButton("Highest Reasons");
		btnShowHighestReasons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Reports.createHighestReasons(
						(Date) dateChooserStartDateReports.getDate(),
						(Date) dateChooserEndDateReports.getDate());
			}
		});
		btnShowHighestReasons
				.setToolTipText("You can select dates if you wish");
		panelOtherReports.add(btnShowHighestReasons);

		JButton btnColourCheckin = new JButton("All Colour Received");
		btnColourCheckin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				DatabaseConnection.getCheckedInPlates("colour",
						(Date) dateChooserStartDateReports.getDate(),
						(Date) dateChooserEndDateReports.getDate(), false);
			}
		});
		panelOtherReports.add(btnColourCheckin);

		JButton btnCommonCheckin = new JButton("All Common Received");
		btnCommonCheckin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				DatabaseConnection.getCheckedInPlates("common",
						(Date) dateChooserStartDateReports.getDate(),
						(Date) dateChooserEndDateReports.getDate(), false);
			}
		});
		panelOtherReports.add(btnCommonCheckin);
		
		JButton btnColourRemakes = new JButton("Colour Remakes");
		btnColourRemakes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				DatabaseConnection.getCheckedInPlates("colour",
						(Date) dateChooserStartDateReports.getDate(),
						(Date) dateChooserEndDateReports.getDate(), true);
			}
		});
		panelOtherReports.add(btnColourRemakes);
		
		JButton btnCommonRemakes = new JButton("Common Remakes");
		btnCommonRemakes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				DatabaseConnection.getCheckedInPlates("common",
						(Date) dateChooserStartDateReports.getDate(),
						(Date) dateChooserEndDateReports.getDate(), true);
			}
		});
		panelOtherReports.add(btnCommonRemakes);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(128, 128, 128), 1,

		true));
		panel.setBounds(23, 11, 922, 41);
		panelReOrderedPlates.add(panel);
		panel.setLayout(new GridLayout(1, 6, 0, 0));

		rdBtnColourReport = new JRadioButton("Colour");
		buttonGroup_2.add(rdBtnColourReport);
		rdBtnColourReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				isColour();
				populateReportReasonShiftNameColour();
			}
		});
		rdBtnColourReport.setSelected(true);
		rdBtnColourReport.setHorizontalAlignment(SwingConstants.CENTER);
		rdBtnColourReport.setForeground(new Color(112, 128, 144));
		rdBtnColourReport.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel.add(rdBtnColourReport);

		rdBtnVarnishReport = new JRadioButton("Varnish");
		buttonGroup_2.add(rdBtnVarnishReport);
		rdBtnVarnishReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetReorderReportCombos();
				isVarnish();
				populateReportReasonShiftNameCommon("varnish");
			}
		});
		rdBtnVarnishReport.setHorizontalAlignment(SwingConstants.CENTER);
		rdBtnVarnishReport.setForeground(new Color(112, 128, 144));
		rdBtnVarnishReport.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel.add(rdBtnVarnishReport);

		rdBtnAntimistReport = new JRadioButton("Antimist");
		rdBtnAntimistReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetReorderReportCombos();
				isAntimist();
				populateReportReasonShiftNameCommon("antimist");
			}
		});
		buttonGroup_2.add(rdBtnAntimistReport);
		rdBtnAntimistReport.setHorizontalAlignment(SwingConstants.CENTER);
		rdBtnAntimistReport.setForeground(new Color(112, 128, 144));
		rdBtnAntimistReport.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel.add(rdBtnAntimistReport);

		rdBtnAdhesiveReport = new JRadioButton("Adhesive");
		rdBtnAdhesiveReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetReorderReportCombos();
				isAdhesive();
				populateReportReasonShiftNameCommon("adhesive");
			}
		});
		buttonGroup_2.add(rdBtnAdhesiveReport);
		rdBtnAdhesiveReport.setHorizontalAlignment(SwingConstants.CENTER);
		rdBtnAdhesiveReport.setForeground(new Color(112, 128, 144));
		rdBtnAdhesiveReport.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel.add(rdBtnAdhesiveReport);

		rdBtnDatalaseReport = new JRadioButton("Datalase");
		rdBtnDatalaseReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resetReorderReportCombos();
				isDatalase();
				populateReportReasonShiftNameCommon("datalase");
			}
		});
		buttonGroup_2.add(rdBtnDatalaseReport);
		rdBtnDatalaseReport.setHorizontalAlignment(SwingConstants.CENTER);
		rdBtnDatalaseReport.setForeground(new Color(112, 128, 144));
		rdBtnDatalaseReport.setFont(new Font("Tahoma", Font.PLAIN, 16));
		panel.add(rdBtnDatalaseReport);

		JPanel panelReorderData = new JPanel();
		panelReorderData.setBounds(23, 184, 734, 216);
		panelReOrderedPlates.add(panelReorderData);
		panelReorderData.setLayout(null);

		JPanel panelReportRefinePlates = new JPanel();
		panelReportRefinePlates.setBounds(0, 0, 87, 206);
		panelReorderData.add(panelReportRefinePlates);
		panelReportRefinePlates.setLayout(new GridLayout(6, 1, 0, 10));

		JLabel lblReference = new JLabel("Reference:");
		lblReference.setVerticalAlignment(SwingConstants.BOTTOM);
		lblReference.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblReference.setHorizontalAlignment(SwingConstants.LEFT);
		panelReportRefinePlates.add(lblReference);

		JLabel lblYrNumber = new JLabel("YR Number:");
		lblYrNumber.setVerticalAlignment(SwingConstants.BOTTOM);
		lblYrNumber.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblYrNumber.setHorizontalAlignment(SwingConstants.LEFT);
		panelReportRefinePlates.add(lblYrNumber);

		lblColour = new JLabel("Colour:");
		lblColour.setVerticalAlignment(SwingConstants.BOTTOM);
		lblColour.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblColour.setHorizontalAlignment(SwingConstants.LEFT);
		panelReportRefinePlates.add(lblColour);

		JLabel lblReason = new JLabel("Reason:");
		lblReason.setVerticalAlignment(SwingConstants.BOTTOM);
		lblReason.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblReason.setHorizontalAlignment(SwingConstants.LEFT);
		panelReportRefinePlates.add(lblReason);

		JLabel lblName_1 = new JLabel("Name:");
		lblName_1.setVerticalAlignment(SwingConstants.BOTTOM);
		lblName_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblName_1.setHorizontalAlignment(SwingConstants.LEFT);
		panelReportRefinePlates.add(lblName_1);

		panelReorderCombos = new JPanel();
		panelReorderCombos.setBounds(97, 0, 193, 206);
		panelReorderData.add(panelReorderCombos);
		panelReorderCombos.setLayout(new GridLayout(7, 1, 0, 10));

		cboReferenceReport = new JComboBox();
		cboReferenceReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (rdBtnColourReport.isSelected()) {
					populateReportCombosColour();
				} else if (rdBtnVarnishReport.isSelected()) {
					populateReportCombos("varnish");
				} else if (rdBtnAntimistReport.isSelected()) {
					populateReportCombos("antimist");
				} else if (rdBtnAdhesiveReport.isSelected()) {
					populateReportCombos("adhesive");
				} else {
					populateReportCombos("datalase");
				}
			}
		});
		panelReorderCombos.add(cboReferenceReport);

		cboYrReport = new JComboBox();
		panelReorderCombos.add(cboYrReport);

		cboColourReport = new JComboBox();
		panelReorderCombos.add(cboColourReport);

		cboReasonReport = new JComboBox();
		panelReorderCombos.add(cboReasonReport);

		cboNameReport = new JComboBox();
		panelReorderCombos.add(cboNameReport);

		JButton btnRunPlateReport = new JButton("Standard Report");
		panelReorderCombos.add(btnRunPlateReport);
		
		JButton btnExcelDocument = new JButton("Excel Document");
		btnExcelDocument.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if(rdBtnColourReport.isSelected())
				{
					DatabaseConnection.getReorderedColourPlates("colour", dateChooserStartDateReports.getDate(), dateChooserEndDateReports.getDate(),
							cboReferenceReport.getSelectedItem().toString(), cboYrReport.getSelectedItem().toString(), cboReasonReport.getSelectedItem()
							.toString(), cboColourReport.getSelectedItem().toString(),cboNameReport.getSelectedItem().toString());
				}
			}
		});
		panelReorderCombos.add(btnExcelDocument);
		btnRunPlateReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				runProgressBar();

				try {
					new Thread(new Runnable() {
						public void run() {

							if (rdBtnColourReport.isSelected()) {
								Reports.createColourReport(cboReferenceReport
										.getSelectedItem().toString(),
										cboYrReport.getSelectedItem()
												.toString(), cboColourReport
												.getSelectedItem().toString(),
										cboReasonReport.getSelectedItem()
												.toString(), cboNameReport
												.getSelectedItem().toString(),
										dateChooserStartDateReports.getDate(),
										dateChooserEndDateReports.getDate());
							} else if (rdBtnVarnishReport.isSelected()) {
								Reports.createReport("/jrxml/Varnish_A4.jrxml",
										cboReferenceReport.getSelectedItem()
												.toString(), cboYrReport
												.getSelectedItem().toString(),
										cboReasonReport.getSelectedItem()
												.toString(), cboNameReport
												.getSelectedItem().toString(),
										dateChooserStartDateReports.getDate(),
										dateChooserEndDateReports.getDate());
							} else if (rdBtnAntimistReport.isSelected()) {
								Reports.createReport(
										"/jrxml/Antimist_A4.jrxml",
										cboReferenceReport.getSelectedItem()
												.toString(), cboYrReport
												.getSelectedItem().toString(),
										cboReasonReport.getSelectedItem()
												.toString(), cboNameReport
												.getSelectedItem().toString(),
										dateChooserStartDateReports.getDate(),
										dateChooserEndDateReports.getDate());
							} else if (rdBtnAdhesiveReport.isSelected()) {
								Reports.createReport(
										"/jrxml/Adhesive_A4.jrxml",
										cboReferenceReport.getSelectedItem()
												.toString(), cboYrReport
												.getSelectedItem().toString(),
										cboReasonReport.getSelectedItem()
												.toString(), cboNameReport
												.getSelectedItem().toString(),
										dateChooserStartDateReports.getDate(),
										dateChooserEndDateReports.getDate());
							} else {
								Reports.createReport(
										"/jrxml/Datalase_A4.jrxml",
										cboReferenceReport.getSelectedItem()
												.toString(), cboYrReport
												.getSelectedItem().toString(),
										cboReasonReport.getSelectedItem()
												.toString(), cboNameReport
												.getSelectedItem().toString(),
										dateChooserStartDateReports.getDate(),
										dateChooserEndDateReports.getDate());
							}

							frame.dispose();
						}
					}).start();
				} catch (NullPointerException n) {

				}
			}
		});

		panelReorderDatePicker = new JPanel();
		panelReorderDatePicker.setBounds(215, 94, 537, 23);
		panelReOrderedPlates.add(panelReorderDatePicker);
		panelReorderDatePicker.setLayout(new GridLayout(1, 2, 10, 15));

		lblStartDateReports = new JLabel("Start Date:");
		lblStartDateReports.setHorizontalAlignment(SwingConstants.RIGHT);
		panelReorderDatePicker.add(lblStartDateReports);

		dateChooserStartDateReports = new JDateChooser();
		panelReorderDatePicker.add(dateChooserStartDateReports);

		JLabel lblEndDateReports = new JLabel("End Date:");
		lblEndDateReports.setHorizontalAlignment(SwingConstants.RIGHT);
		panelReorderDatePicker.add(lblEndDateReports);

		dateChooserEndDateReports = new JDateChooser();
		panelReorderDatePicker.add(dateChooserEndDateReports);

		JPanel panelDieReports = new JPanel();
		reportsTabbedPane.addTab("Dies", null, panelDieReports, null);
		panelDieReports.setLayout(null);

		panelSelection = new JPanel();
		panelSelection.setBounds(311, 143, 346, 138);
		panelDieReports.add(panelSelection);
		panelSelection.setLayout(null);

		dateChooserStartDate = new JDateChooser();
		dateChooserStartDate.setBounds(21, 70, 119, 20);
		panelSelection.add(dateChooserStartDate);

		JLabel lblStartDate = new JLabel("Start Date");
		lblStartDate.setForeground(new Color(112, 128, 144));
		lblStartDate.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblStartDate.setBounds(21, 51, 119, 14);
		panelSelection.add(lblStartDate);

		JLabel lblEndDate = new JLabel("End date");
		lblEndDate.setForeground(new Color(112, 128, 144));
		lblEndDate.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblEndDate.setBounds(207, 51, 119, 14);
		panelSelection.add(lblEndDate);

		dateChooserEndDate = new JDateChooser();
		dateChooserEndDate.setBounds(207, 70, 119, 20);
		panelSelection.add(dateChooserEndDate);

		JButton btnRunReport = new JButton("Run Report");
		btnRunReport.setBounds(237, 115, 89, 23);
		panelSelection.add(btnRunReport);

		rdbtnInternalDieReport = new JRadioButton("Internal");
		rdbtnInternalDieReport.setForeground(new Color(112, 128, 144));
		rdbtnInternalDieReport.setFont(new Font("Tahoma", Font.PLAIN, 16));
		rdbtnInternalDieReport.setBounds(21, 0, 109, 23);
		panelSelection.add(rdbtnInternalDieReport);
		rdbtnInternalDieReport.setSelected(true);
		buttonGroup_1.add(rdbtnInternalDieReport);

		rdbtnExternalDieReport = new JRadioButton("External");
		rdbtnExternalDieReport.setForeground(new Color(112, 128, 144));
		rdbtnExternalDieReport.setFont(new Font("Tahoma", Font.PLAIN, 16));
		rdbtnExternalDieReport.setBounds(207, 0, 109, 23);
		panelSelection.add(rdbtnExternalDieReport);
		buttonGroup_1.add(rdbtnExternalDieReport);

		rdbtnRemovedDies = new JRadioButton("Removed Dies");
		rdbtnRemovedDies.setHorizontalAlignment(SwingConstants.RIGHT);
		buttonGroup_3.add(rdbtnRemovedDies);
		rdbtnRemovedDies.setFont(new Font("Tahoma", Font.PLAIN, 16));
		rdbtnRemovedDies.setBounds(504, 39, 153, 23);
		panelDieReports.add(rdbtnRemovedDies);

		rdbtnReorderedDies = new JRadioButton("Re-Ordered Dies");
		rdbtnReorderedDies.setSelected(true);
		buttonGroup_3.add(rdbtnReorderedDies);
		rdbtnReorderedDies.setFont(new Font("Tahoma", Font.PLAIN, 16));
		rdbtnReorderedDies.setBounds(311, 39, 160, 23);
		panelDieReports.add(rdbtnReorderedDies);

		panelForms = new JPanel();
		mainTabbedPane.addTab(
				"Forms",
				new ImageIcon(GUI_HomeScreen.class
						.getResource("/image/form.png")), panelForms, null);
		panelForms.setLayout(null);

		JButton btnColourStockCheck = new JButton("Create Stock Check Form");
		btnColourStockCheck.setBounds(385, 46, 233, 23);
		panelForms.add(btnColourStockCheck);
		
		JButton btnCreateDieStock = new JButton("Create Die Stock Check Forms");
		btnCreateDieStock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				runProgressBar();
				
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						Reports.createInternalStockCheckForm();
						Reports.createExternalStockCheckForm();
						frame.dispose();
					}
				}).start();
			}
		});
		btnCreateDieStock.setBounds(385, 123, 233, 23);
		panelForms.add(btnCreateDieStock);
		btnColourStockCheck.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				runProgressBar();

				new Thread(new Runnable() {

					@Override
					public void run() {
						Reports.createColourStockCheckForm();
						frame.dispose();
					}
				}).start();

			}
		});

		label = new JLabel("2.7.3");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		label.setBounds(1198, 48, 46, 14);
		panelBackground.add(label);
		label.setForeground(Color.WHITE);
		label.setVerticalAlignment(SwingConstants.TOP);
		label.setHorizontalAlignment(SwingConstants.RIGHT);

		lblNotLoggedIn = new JLabel("");
		lblNotLoggedIn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				lblNotLoggedIn.setText("");
				lblLogin.setText("Login");
				userType = null;
			}
		});
		lblNotLoggedIn.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNotLoggedIn.setForeground(Color.WHITE);
		lblNotLoggedIn.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNotLoggedIn.setBounds(1125, 3, 119, 25);
		panelBackground.add(lblNotLoggedIn);
		btnRunReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (isValidDates()) {

					runProgressBar();

					new Thread(new Runnable() {

						@Override
						public void run() {
							Date startDate = dateChooserStartDate.getDate();
							Date endDate = dateChooserEndDate.getDate();

							if (rdbtnRemovedDies.isSelected()) {
								if (rdbtnInternalDieReport.isSelected()) {
									Reports.createDieReport("internal",
											"/jrxml/RemovedIntDies_A4.jrxml",
											startDate, endDate);
								} else {

									Reports.createDieReport("external",
											"/jrxml/RemovedExtDies_A4.jrxml",
											startDate, endDate);
								}
							} else if (rdbtnReorderedDies.isSelected()) {
								if (rdbtnInternalDieReport.isSelected()) {
									Reports.createReorderedDieReport(
											"/jrxml/ReorderedDiesInt_A4.jrxml",
											startDate, endDate);
								} else {
									Reports.createReorderedDieReport(
											"/jrxml/ReorderedDiesExt_A4.jrxml",
											startDate, endDate);
								}
							}

							frame.dispose();
						}
					}).start();

				}
			}
		});
	}

	private void populateReportCombos(String plateType) {

		if (plateType.equals("varnish")) {
			cboYrReport.removeAllItems();
			cboYrReport.setModel(new DefaultComboBoxModel(DatabaseConnection
					.getVarnishYrFromReorder(
							cboReferenceReport.getSelectedItem().toString())
					.toArray()));
		} else {
			cboYrReport.removeAllItems();
			cboYrReport.addItem(DatabaseConnection.getCommonYr(plateType,
					cboReferenceReport.getSelectedItem().toString()));
		}

		cboReasonReport.removeAllItems();
		cboReasonReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getCommonReorderReasons(plateType,
						cboReferenceReport.getSelectedItem().toString())
				.toArray()));

		cboNameReport.removeAllItems();
		cboNameReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getCommonReorderName(plateType,
						cboReferenceReport.getSelectedItem().toString())
				.toArray()));
	}

	private void populateReportCombosColour() {

		cboYrReport.removeAllItems();
		cboYrReport.addItem(DatabaseConnection.getColourYr(cboReferenceReport
				.getSelectedItem().toString()));

		cboColourReport.removeAllItems();
		cboColourReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getColours(cboReferenceReport.getSelectedItem().toString())
				.toArray()));

		cboReasonReport.removeAllItems();
		cboReasonReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getColourReorderReasons(
						cboReferenceReport.getSelectedItem().toString())
				.toArray()));

		cboNameReport.removeAllItems();
		cboNameReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getColourName(cboReferenceReport.getSelectedItem().toString())
				.toArray()));
	}

	/**
	 * Run when the application starts
	 */
	private void onApplicationStart() {

		refineByProductCode();
		populateIntData();
		populateExtData();
		cboMOD.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getIntMODs().toArray()));
		populateDcRevs();
		txtMeters.setText("0");
		txtJobRevolutions.setText("0");
		displayPlates();
		colour();
		populateShiftCbo();
		UIManager.getLookAndFeelDefaults().put("TableHeader.foreground",
				new Color(112, 128, 144));

		MyListener listener = new MyListener(lblCurrentlyConne, count);

		animator = new javax.swing.Timer(100, listener);
		animator.start();

		plate = new Plate();

		isColour();
		populateReportReasonShiftNameColour();

		// DatabaseConnection.getAllJobRefs();
	}

	/**
	 * Populates the internal die code data
	 */
	private void populateIntData() {
		txtInternalDC2.setText(cboInternalDC.getSelectedItem().toString());
		displayIntRepeat(cboInternalDC.getSelectedItem().toString());
		displayIntDies2(cboInternalDC.getSelectedItem().toString());
	}

	/**
	 * Populates the external die code data
	 */
	private void populateExtData() {
		txtExtDC.setText(cboExtDC.getSelectedItem().toString());
		displayExtRepeat(cboExtDC.getSelectedItem().toString());
		displayExtDies2(cboExtDC.getSelectedItem().toString());
	}

	/**
	 * Opens GUI_BookDieIn so user can book a die in
	 */
	private void openGUI_BookDieIn() {

		GUI_BookDieIn dialog = new GUI_BookDieIn();
		dialog.setTitle("Enter die information");
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setLocationRelativeTo(null);
		dialog.setModal(true);
		dialog.setVisible(true);
	}

	/**
	 * Refines the dies shown by product code
	 */
	private void refineByProductCode() {
		getProductDetails(cboProductCode.getSelectedItem().toString());
		displayIntDies(txtInternalDC.getText());
		displayExtDies(txtExternalDC.getText());
	}

	/**
	 * Retrieves product details based on product code
	 */
	private void getProductDetails(String pc) {

		ProductCode productCode = DatabaseConnection.getProductDetails(pc);

		txtSize.setText(productCode.getSize());
		txtInternalDC.setText(productCode.getIntDC());
		txtExternalDC.setText(productCode.getExtDC());

	}

	/**
	 * Displays internal dies refined by dc
	 * 
	 * @param intDC
	 */
	private void displayIntDies(String intDC) {

		String[] columnNames = { "MOD Number", "Revolutions" };

		internalDies = DatabaseConnection.getInternalMOD(intDC);
		tableInternalMOD.setModel(new TableModel_MOD(internalDies, columnNames,
				"standard"));
		try {
			txtSelectedIntMOD.setText((String) tableInternalMOD
					.getValueAt(0, 0));
			displayIntPerformance((String) tableInternalMOD.getValueAt(0, 0));
		} catch (IndexOutOfBoundsException i) {
			// Do nothing
		}
	}

	/**
	 * Displays internal dies refined by dc in refine by die code view
	 * 
	 * @param intDC
	 */
	public static void displayIntDies2(String intDC) {

		String[] columnNames = { "MOD Number", "Revolutions", "" };

		internalDies = DatabaseConnection.getInternalMOD(intDC);
		tableInternalDie2.setModel(new TableModel_MOD(internalDies,
				columnNames, "standard"));
		tableInternalDie2.getColumn("").setCellRenderer(
				new UserButtonRenderer());
		try {
			txtSelectedMOD2
					.setText((String) tableInternalDie2.getValueAt(0, 0));
			displayIntPerformance2((String) tableInternalDie2.getValueAt(0, 0));
		} catch (IndexOutOfBoundsException i) {
			// Do nothing
		}
	}

	/**
	 * Displays removed internal dies
	 */
	private void displayRemovedInt() {

		String[] columnNames = { "DC Number", "MOD Number", "Revolutions",
				"Removed Date", "Removed Reason" };

		internalDies = DatabaseConnection.getRemovedInternalMOD(true);
		tableInternalDie2.setModel(new TableModel_MOD(internalDies,
				columnNames, "removed"));
		try {
			txtSelectedMOD2
					.setText((String) tableInternalDie2.getValueAt(0, 0));
			displayIntPerformance2((String) tableInternalDie2.getValueAt(0, 0));
		} catch (IndexOutOfBoundsException i) {
			// Do nothing
		}
	}

	/**
	 * Displays removed external dies
	 */
	private void displayRemovedExt() {

		String[] columnNames = { "DC Number", "MOD Number", "Revolutions",
				"Removed Date", "Removed Reason" };

		externalDies = DatabaseConnection.getRemovedExternalMOD(true);
		tableExtMOD2.setModel(new TableModel_MOD(externalDies, columnNames,
				"removed"));
		try {
			txtExtSelectedMOD.setText((String) tableExtMOD2.getValueAt(0, 0));
			displayExtPerformance2((String) tableExtMOD2.getValueAt(0, 0));
		} catch (IndexOutOfBoundsException i) {
			// Do nothing
		}
	}

	/**
	 * Displays internal dies refined by dc
	 * 
	 * @param intDC
	 */
	private void displayExtDies(String extDC) {

		String[] columnNames = { "MOD Number", "Revolutions" };

		externalDies = DatabaseConnection.getExternalMOD(extDC);
		tableExternalMOD.setModel(new TableModel_MOD(externalDies, columnNames,
				"standard"));

		try {
			txtSelectedExtMOD.setText((String) tableExternalMOD
					.getValueAt(0, 0));
			displayExtPerformance((String) tableExternalMOD.getValueAt(0, 0));
		} catch (IndexOutOfBoundsException i) {
			// Do nothing
		}
	}

	/**
	 * Displays internal dies refined by dc - die code view
	 * 
	 * @param intDC
	 */
	public static void displayExtDies2(String extDC) {

		String[] columnNames = { "MOD Number", "Revolutions", "" };

		externalDies = DatabaseConnection.getExternalMOD(extDC);
		tableExtMOD2.setModel(new TableModel_MOD(externalDies, columnNames,
				"standard"));
		tableExtMOD2.getColumn("").setCellRenderer(new UserButtonRenderer());
		try {
			txtExtSelectedMOD.setText((String) tableExtMOD2.getValueAt(0, 0));
			displayExtPerformance2((String) tableExtMOD2.getValueAt(0, 0));
		} catch (IndexOutOfBoundsException i) {

			// Do nothing
		}
	}

	/**
	 * Displays ext die performance refined by mod
	 * 
	 * @param mod
	 *            mod to refine by
	 */
	private void displayExtPerformance(String mod) {

		tableExtPerformance.setModel(new TableModelDiePerformance(
				DatabaseConnection.getExtDiePerformance(mod)));
	}

	/**
	 * Displays ext die performance refined by mod - die code view
	 * 
	 * @param mod
	 *            mod to refine by
	 */
	protected static void displayExtPerformance2(String mod) {

		tableExtPerformance2.setModel(new TableModelDiePerformance(
				DatabaseConnection.getExtDiePerformance(mod)));
	}

	/**
	 * Displays int die performance refined by mod
	 * 
	 * @param mod
	 *            mod to refine by
	 */
	private void displayIntPerformance(String mod) {

		tableIntPerformance.setModel(new TableModelDiePerformance(
				DatabaseConnection.getIntDiePerformance(mod)));
	}

	/**
	 * Displays int die performance refined by mod - die code view
	 * 
	 * @param mod
	 *            mod to refine by
	 */
	protected static void displayIntPerformance2(String mod) {

		tableInternalPerformance2.setModel(new TableModelDiePerformance(
				DatabaseConnection.getIntDiePerformance(mod)));
	}

	/**
	 * Toggles components visible/invisible as required
	 * 
	 * @param toggle
	 *            true/false
	 */
	private void toggleComponents(boolean toggle) {
		cboInternalDC.setVisible(toggle);
		cboExtDC.setVisible(toggle);
		txtInternalDC2.setVisible(toggle);
		txtExtDC.setVisible(toggle);
		lblInternalDieCode.setVisible(toggle);
		lblInternalDC2.setVisible(toggle);
		lblExtDC.setVisible(toggle);
		lblExternalDieCode.setVisible(toggle);
		btnInternalAddMod.setVisible(toggle);
		btnExternalAddMod.setVisible(toggle);
	}

	/**
	 * Populates dc number, repeat and revolutions of selected die
	 */
	private void populateDcRevs() {

		if (rdbtnInternalUpdate.isSelected()) {
			dieUpdate = DatabaseConnection.getIntDcRevs(cboMOD
					.getSelectedItem().toString());
			txtRepeatUpdate.setText(String.valueOf(DatabaseConnection
					.getIntRepeat(dieUpdate.getDC_Number())));

		}
		if (rdbtnExternalUpdate.isSelected()) {
			dieUpdate = DatabaseConnection.getExtDcRevs(cboMOD
					.getSelectedItem().toString());
			txtRepeatUpdate.setText(String.valueOf(DatabaseConnection
					.getExtRepeat(dieUpdate.getDC_Number())));
		}

		txtDcUpdate.setText(dieUpdate.getDC_Number());
		txtRevolutionsUpdate.setText(String.valueOf(dieUpdate.getTotalRevs()));

		chckbxstEmail.setSelected(dieUpdate.isFirstEmail());
		chckbxndEmail.setSelected(dieUpdate.isSecondEmail());
	}

	/**
	 * Calculates meters and job revolutions
	 */
	private void calculateMeters() {

		try {

			double start = 1;

			if (!txtLifetimeStart.getText().equals("")) {
				start = Integer.parseInt(txtLifetimeStart.getText());
			}

			double end = 1;
			if (!txtLifeTimeEnd.getText().equals("")) {
				end = Integer.parseInt(txtLifeTimeEnd.getText());
			}

			double meters = end - start;
			int intMeters = (int) meters;
			txtMeters.setText(Integer.toString(intMeters));

			double repeat = Double.parseDouble(txtRepeatUpdate.getText()) / 1000;

			double revolutions = meters / repeat;
			int intRevolutions = (int) revolutions;
			txtJobRevolutions.setText(Integer.toString(intRevolutions));

		} catch (NumberFormatException n) {

			// JOptionPane.showMessageDialog(null,
			// "Die repeat cannot be null. Ask Darren to enter repeat before trying again.",
			// "Error", JOptionPane.ERROR_MESSAGE );
		}
	}

	/**
	 * Cancels user input on update tab
	 */
	private void cancelUpdate() {
		txtLifetimeStart.setText("");
		txtLifeTimeEnd.setText("");
		txtMeters.setText("0");
		txtJobRevolutions.setText("0");
		dateChooserUpdate.setDate(null);
		cboPerformanceUpdate.setSelectedIndex(0);
		cboPressUpdate.setSelectedIndex(0);
		txtNameUpdate.setText("");
		txtComments.setText("");
	}

	/**
	 * Validates user input before updating die
	 * 
	 * @return
	 */
	private boolean validateUpdateData() {
		if (chckbxUpdateCommentsOnly.isSelected()) {
			validateTxtAndName();

		} else {
			if (txtJobRevolutions.getText().equals("0")) {
				JOptionPane.showMessageDialog(null,
						"Please enter life time figures", "Error",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}

			if (cboPerformanceUpdate.getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(null,
						"Please select die performance", "Error",
						JOptionPane.ERROR_MESSAGE);
				return false;
			}

			if (cboPressUpdate.getSelectedIndex() == 0) {
				JOptionPane.showMessageDialog(null, "Please select press",
						"Error", JOptionPane.ERROR_MESSAGE);
				return false;
			}

			validateTxtAndName();
		}

		return true;
	}

	/**
	 * Validates comments and name on update tab
	 * 
	 * @return
	 */
	private boolean validateTxtAndName() {

		if (txtComments.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Please enter comments",
					"Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		if (dateChooserUpdate.getDate() == null) {
			JOptionPane.showMessageDialog(null, "Please select a date",
					"Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		if (txtNameUpdate.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Please enter your name",
					"Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		return true;
	}

	/**
	 * Updates a dies total revolutions
	 * 
	 * @param dieType
	 *            Internal/External
	 */
	private void updateTotalRevolutions(String dieType) {
		DatabaseConnection.updateTotalRevolutions(dieType,
				Integer.parseInt(txtJobRevolutions.getText().toString()),
				cboMOD.getSelectedItem().toString());
	}

	/**
	 * Marks a die as removed
	 * 
	 * @param dieType
	 *            Internal/External
	 */
	private void removeDie(String dieType, String mod) {
		DatabaseConnection.removeDie(dieType, mod, txtComments.getText(),
				(java.util.Date) dateChooserUpdate.getDate());
	}

	/**
	 * Retrieves dies on order and displays
	 * 
	 * @param dieType
	 *            Internal/External
	 */
	private void showDueDies() {
		if (chckbxShowRemovedDies.isSelected()) {
			chckbxShowRemovedDies.setSelected(false);
		}

		String[] columnNames = { "DC Number", "MOD Number", "Due Date", ""};

		internalDies = DatabaseConnection.getDueDies("internal");
		externalDies = DatabaseConnection.getDueDies("external");

		tableInternalDie2.setModel(new TableModel_MOD(internalDies,
				columnNames, "due", "internal"));

		// Date chooser column
		TableColumn dateChooserInternal = tableInternalDie2.getColumnModel()
				.getColumn(2);
		dateChooserInternal.setCellRenderer(new JDateChooserRenderer());
		dateChooserInternal.setCellEditor(new JDateChooserCellEditor());
		
		//Button
		tableInternalDie2.getColumn("").setCellRenderer(
				new UserButtonRenderer());

		tableExtMOD2.setModel(new TableModel_MOD(externalDies, columnNames,
				"due", "external"));

		// Date chooser column
		TableColumn dateChooserExternal = tableExtMOD2.getColumnModel()
				.getColumn(2);
		dateChooserExternal.setCellRenderer(new JDateChooserRenderer());
		dateChooserExternal.setCellEditor(new JDateChooserCellEditor());
		
		//Button
		tableExtMOD2.getColumn("").setCellRenderer(
				new UserButtonRenderer());
		
		try {
			displayIntRepeat(internalDies.get(0).getDC_Number());
			displayExtRepeat(externalDies.get(0).getDC_Number());
		} catch (IndexOutOfBoundsException i) {
			// Do nothing
		}

	}

	/**
	 * Shows/Hides performance table and related data
	 * 
	 * @param toggle
	 *            true/false
	 */
	private void togglePerformanceData(boolean toggle) {
		scrollPaneIntPerformance2.setVisible(toggle);
		scrollPaneExtPerformance2.setVisible(toggle);
		txtSelectedMOD2.setVisible(toggle);
		txtExtSelectedMOD.setVisible(toggle);
		lblSelectedMOD2.setVisible(toggle);
		lblExtSelectedMOD.setVisible(toggle);
	}

	/**
	 * Displays internal repeat
	 * 
	 * @param dc
	 */
	private void displayIntRepeat(String dc) {
		try {
			txtRepeat.setText(DatabaseConnection.getIntRepeat(dc));
		} catch (Exception e) {
			// Do nothing
		}
	}

	/**
	 * Displays external repeat
	 * 
	 * @param dc
	 */
	private void displayExtRepeat(String dc) {
		try {
			txtExtRepeat.setText(DatabaseConnection.getExtRepeat(dc));
		} catch (Exception e) {
			// Do nothing
		}
	}

	/**
	 * Displays plates when filtering using job ref combo
	 */
	private void displayPlates() {

		try {
			plates = DatabaseConnection.getPlates(cboJobRefOrder
					.getSelectedItem().toString());
		} catch (NullPointerException n) {
			// Do nothing
		}

		if (cboJobRefOrder.getSelectedItem() != null) {

			tableOrderPlates.setModel(new TableModel_ColourPlates(plates));
			createColourCombos();

		} else {

			tableOrderPlates.setModel(new DefaultTableModel());
		}
	}

	/**
	 * Creates the combo boxes in tableOrderPlates
	 */
	private void createColourCombos() {

		cboColourComboBox = new JComboBox(DatabaseConnection.getColours()
				.toArray());
		TableColumn colourColumn = tableOrderPlates.getColumnModel().getColumn(
				0);
		colourColumn.setCellEditor(new DefaultCellEditor(cboColourComboBox));
		cboColourComboBox.setEditable(true);
		cboColourComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String other = cboColourComboBox.getSelectedItem().toString();
				// int tableRow = tableColours.getSelectedRow();

				if (other.equals("-ADD NEW-")) {

					String newColour = JOptionPane
							.showInputDialog("Enter colour");

					if (newColour != null) {
						if (JOptionPane
								.showConfirmDialog(
										null,
										"Would you like to add this colour to the list?",
										"New colour", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
							addNewColour(newColour);
						}

						cboColourComboBox.setSelectedItem(newColour);
					} else {
						cboColourComboBox.setSelectedItem(originalColour);
					}
				}
			}
		});

		// Qty combo
		TableColumn qtyColumn = tableOrderPlates.getColumnModel().getColumn(2);
		String[] quantity = { "1", "2", "3", "4", "5" };
		JComboBox qtyComboBox = new JComboBox(quantity);
		qtyColumn.setCellEditor(new DefaultCellEditor(qtyComboBox));

		// Qty combo
		TableColumn decColumn = tableOrderPlates.getColumnModel().getColumn(3);
		String[] dec = { "0", "1", "2", "3", "4", "5" };
		JComboBox cboDecrement = new JComboBox(dec);
		decColumn.setCellEditor(new DefaultCellEditor(cboDecrement));

		// Reason combo
		TableColumn reasonColumn = tableOrderPlates.getColumnModel().getColumn(
				4);
		cboReason = new JComboBox(DatabaseConnection.getReasons().toArray());
		cboReason.setMaximumRowCount(DatabaseConnection.getReasons().size());
		reasonColumn.setCellEditor(new DefaultCellEditor(cboReason));
		reasonColumn.setPreferredWidth(170);
		cboReason.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String reason = cboReason.getSelectedItem().toString();
				int tableRow = tableOrderPlates.getSelectedRow();

				if (reason.equals("Other")) {
					String otherReason = JOptionPane
							.showInputDialog("Enter reason");
					plates.get(tableRow).setReorderReason(otherReason);
				}
			}
		});

		// Timeline combo
		TableColumn timelineColumn = tableOrderPlates.getColumnModel()
				.getColumn(5);
		JComboBox timelineComboBox = new JComboBox(DatabaseConnection
				.getTimelines().toArray());
		timelineColumn.setCellEditor(new DefaultCellEditor(timelineComboBox));
		timelineColumn.setPreferredWidth(170);
	}

	/**
	 * Populates the shift combobox
	 */
	private void populateShiftCbo() {

		cboShiftCodeOrder.removeAllItems();
		cboShiftCodeOrder.addItem("Please select");
		for (String item : DatabaseConnection.getShiftDescription()) {
			cboShiftCodeOrder.addItem(item);
		}
	}

	/**
	 * Validates data before sending an email
	 * 
	 * @return
	 */
	private boolean validateColourPlateData() {
		int checkedCount = 0;

		for (Plate plate : plates) {
			if (plate.isAddToOrder()) {
				checkedCount++;

				if (plate.getReorderReason().equals("")) {
					String message = "Enter a reason for " + plate.getInkRef();
					JOptionPane.showMessageDialog(null, message, "Error",
							JOptionPane.ERROR_MESSAGE);
					return false;
				}

				if (plate.getDeliveryTimeline().equals("")) {
					String message = "Enter a delivery timeline for "
							+ plate.getInkRef();
					JOptionPane.showMessageDialog(null, message, "Error",
							JOptionPane.ERROR_MESSAGE);
					return false;
				}
			}
		}

		if (checkedCount == 0) {
			JOptionPane.showMessageDialog(null,
					"You have not selected any plates", "Error",
					JOptionPane.ERROR_MESSAGE);
			return false;
		}

		return true;
	}

	/**
	 * Validates data before sending an email
	 * 
	 * @return
	 */
	private boolean validateShiftAndName() {

		if (cboShiftCodeOrder.getSelectedIndex() == 0) {
			JOptionPane.showMessageDialog(null, "Please select a shift",
					"Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		if (txtNameOrder.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Please enter your name",
					"Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		return true;
	}

	/**
	 * Validates the yr number text box
	 * 
	 * @return
	 */
	private boolean validateCommon() {
		if (txtYrCommon.getText().equals("")) {
			JOptionPane
					.showMessageDialog(
							null,
							"YR Reference cannot be blank.\nPlease see the quality technician before continuing.",
							"Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		return true;
	}

	/**
	 * Resets the order information after a colour plate has been ordered.
	 */
	private void clearColourOrder() {
		txtNameOrder.setText("");
		displayPlates();
		cboShiftCodeOrder.setSelectedIndex(0);
	}

	/**
	 * Makes a call to the database to update the reordered colour plates table
	 * 
	 * @param colourOrder
	 */
	// private void updateReorderedColour(ColourOrder colourOrder) {
	// DatabaseConnection.updateReorderedColour(colourOrder);
	// }

	/**
	 * Makes a call to the database to update the reordered common plates table
	 * 
	 * @param colourOrder
	 */
//	private void updateReorderedCommon(String plateType, CommonOrder commonOrder) {
//
//		DatabaseConnection.updateReorderedCommon(plateType, commonOrder);
//	}

	/**
	 * When colour radio button is selected
	 */
	private void colour() {

		btnCopy.setVisible(true);
		scrollPaneOrderPlates.setVisible(true);
		lblJobReferenceOrder.setVisible(true);
		cboJobRefOrder.setVisible(true);
		lblQuantityCommonOrder.setVisible(false);
		lblDecrement.setVisible(false);
		cboQtyCommonOrder.setVisible(false);
		cboDecrementCommon.setVisible(false);
		lblYrReferenceCommon.setVisible(false);
		txtYrCommon.setVisible(false);
		lblDeliveryTimelineCommon.setVisible(false);
		cboTimelineCommon.setVisible(false);
		lblReorderReasonCommon.setVisible(false);
		cboReasonCommonOrder.setVisible(false);

	}

	/**
	 * When common radio buttons are selected
	 */
	private void common() {

		btnCopy.setVisible(false);
		lblJobReferenceOrder.setVisible(false);
		cboJobRefOrder.setVisible(false);
		scrollPaneOrderPlates.setVisible(false);
		lblQuantityCommonOrder.setVisible(true);
		lblDecrement.setVisible(true);
		cboQtyCommonOrder.setVisible(true);
		cboDecrementCommon.setVisible(true);
		lblYrReferenceCommon.setVisible(true);
		txtYrCommon.setVisible(true);
		lblDeliveryTimelineCommon.setVisible(true);
		cboTimelineCommon.setVisible(true);
		lblReorderReasonCommon.setVisible(true);
		cboReasonCommonOrder.setVisible(true);
	}

	/**
	 * When varnish radio button is selected
	 */
	private void varnish() {

		lblJobReferenceOrder.setVisible(true);
		cboJobRefOrder.setVisible(true);

		try {
			commonPlate = DatabaseConnection.getCommonYrAndId("varnish",
					cboJobRefOrder.getSelectedItem().toString());
			txtYrCommon.setText(commonPlate.getYr());
			commonPlateID = commonPlate.getId();

			// txtYrCommon.setText(DatabaseConnection.getVarnishYr(cboJobRefOrder
			// .getSelectedItem().toString()));
		} catch (NullPointerException n) {
			txtYrCommon.setText("");
		}
	}

	/**
	 * When antimist radio button is selected
	 */
	private void antimist() {

		try {
			commonPlate = DatabaseConnection.getCommonYrAndId("antimist",
					cboProductCodeOrder.getSelectedItem().toString());
			txtYrCommon.setText(commonPlate.getYr());
			commonPlateID = commonPlate.getId();

			// txtYrCommon.setText(DatabaseConnection.getCommonYr("antimist",
			// cboProductCodeOrder.getSelectedItem().toString()));
		} catch (NullPointerException n) {
			txtYrCommon.setText("");
		}
	}

	/**
	 * When adhesive radio button is selected
	 */
	private void adhesive() {

		try {
			commonPlate = DatabaseConnection.getCommonYrAndId("adhesive",
					cboProductCodeOrder.getSelectedItem().toString());
			txtYrCommon.setText(commonPlate.getYr());
			commonPlateID = commonPlate.getId();

			// txtYrCommon.setText(DatabaseConnection.getCommonYr("adhesive",
			// cboProductCodeOrder.getSelectedItem().toString()));
		} catch (NullPointerException n) {
			txtYrCommon.setText("");
		}
	}

	/**
	 * When datalase radio button is selected
	 */
	private void datalase() {

		try {
			commonPlate = DatabaseConnection.getCommonYrAndId("datalase",
					cboProductCodeOrder.getSelectedItem().toString());
			txtYrCommon.setText(commonPlate.getYr());
			commonPlateID = commonPlate.getId();

			// txtYrCommon.setText(DatabaseConnection.getCommonYr("datalase",
			// cboProductCodeOrder.getSelectedItem().toString()));
		} catch (NullPointerException n) {
			txtYrCommon.setText("");
		}
	}

	/**
	 * Resets shift combo and name text field
	 */
	private void resetShiftAndName() {
		cboShiftCodeOrder.setSelectedIndex(0);
		txtNameOrder.setText("");
		cboReasonCommonOrder.setSelectedIndex(0);
		cboTimelineCommon.setSelectedIndex(0);
	}

	/**
	 * Creates the colour order and updates database
	 */
	private void createColourOrderAndUpdate() {

		runProgressBar();

		new Thread(new Runnable() {

			@Override
			public void run() {

				colourPlatesForOrder = new ArrayList<Plate>();

				for (Plate plate : plates) {
					if (plate.isAddToOrder()) {
						colourPlatesForOrder.add(plate);
					}
				}

				colourOrder = new ColourOrder(cboJobRefOrder.getSelectedItem()
						.toString(), "Colour plate order", txtNameOrder
						.getText(), colourPlatesForOrder, cboShiftCodeOrder
						.getSelectedItem().toString());

				if (SendMail.sendPlateNotificationEmail()) {
					DatabaseConnection.insertColourPlateOrder(colourOrder);

					clearColourOrder();

					frame.dispose();

					JOptionPane
							.showMessageDialog(
									null,
									"Your request has been sent to management for approval",
									"Success", JOptionPane.INFORMATION_MESSAGE);
				} else {
					frame.dispose();
				}

				// update stock
				// for (Plate plate : plates) {
				// if (plate.isAddToOrder()) {
				// DatabaseConnection
				// .updatePlateStock(plate);
				// }
				// }
				//
				// updateReorderedColour(colourOrder);

			}
		}).start();
	}

	/**
	 * Creates order for common plates, orders by email and updates tables
	 * 
	 * @param plateType
	 */
	private void createCommonOrderAndUpdate(final String plateType) {

		if (validateCommon() && validateShiftAndName()) {

			runProgressBar();

			new Thread(new Runnable() {

				@Override
				public void run() {

					plate = new Plate(txtYrCommon.getText(), Integer
							.parseInt(cboQtyCommonOrder.getSelectedItem()
									.toString()), 1, cboReasonCommonOrder
							.getSelectedItem().toString(), cboTimelineCommon
							.getSelectedItem().toString());

					CommonOrder commonOrder = new CommonOrder(
							cboProductCodeOrder.getSelectedItem().toString(),
							plateType, txtNameOrder.getText(),
							cboShiftCodeOrder.getSelectedItem().toString(),
							plate);

					if (SendMail.sendPlateNotificationEmail()) {
						
						DatabaseConnection.insertCommonPlateOrder(commonOrder);

						resetShiftAndName();

						frame.dispose();

						JOptionPane
								.showMessageDialog(
										null,
										"Your request has been sent to management for approval",
										"Success",
										JOptionPane.INFORMATION_MESSAGE);
					} else {
						frame.dispose();
					}

					// SendMail.sendCommonEmail(commonOrder);
					//
					// updateReorderedCommon(plateType, commonOrder);
					//
					// DatabaseConnection.updateCommonPlateStock(Integer
					// .valueOf(cboDecrementCommon.getSelectedItem()
					// .toString()), commonPlateID);

				}
			}).start();

		}
	}

	/**
	 * Changes the list of product codes in the product code combo from standard
	 * to datalase
	 * 
	 * @param standard
	 */
	private void setStandardProductCodes(boolean standard) {
		if (standard) {
			cboProductCodeOrder.setModel(new DefaultComboBoxModel(
					DatabaseConnection.getProductCodes().toArray()));
		} else {
			cboProductCodeOrder.setModel(new DefaultComboBoxModel(
					DatabaseConnection.getDatalaseProductCodes().toArray()));
		}
	}

	/**
	 * Validates the user by checking username and password and checking user
	 * type
	 */
	private void openSecurityDialog() {

		GUI_SecurityDialog dialog = new GUI_SecurityDialog();
		dialog.setTitle("Login");
		dialog.setSize(350, 200);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setLocationRelativeTo(null);
		dialog.setModal(true);
		dialog.setVisible(true);
	}

	/**
	 * Opens GUI_AddUpdate
	 */
	private void openGUI_AddUpdate(String userType) {

		GUI_AddUpdate frame = new GUI_AddUpdate(userType);
		frame.setTitle("Control Panel");
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.setSize(1280, 730);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	/**
	 * Opens GUI_OrderDie
	 */
	private void openGUI_OrderDie(String manu, String dieType, String dieCode,
			String mod, String repeat) {
		GUI_OrderDie dialog = new GUI_OrderDie(manu, dieType, dieCode, mod,
				repeat);
		dialog.setTitle("Order Die");
		dialog.setSize(710, 300);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setLocationRelativeTo(null);
		dialog.setModal(true);
		dialog.setVisible(true);
	}

	/**
	 * Opens GUI_ReportProblem
	 */
	private void openGUI_ReportProblem() {
		GUI_ReportProblem dialog = new GUI_ReportProblem();
		dialog.setTitle("Report Problem");
		dialog.setSize(450, 300);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setLocationRelativeTo(null);
		dialog.setModal(true);
		dialog.setVisible(true);
	}

	/**
	 * Opens GUI_InsertNewDC so user can add new dc numbers
	 */
	private void openGUI_InsertNewDC() {

		GUI_InsertNewDC dialog = new GUI_InsertNewDC();
		dialog.setTitle("Enter die information");
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setLocationRelativeTo(null);
		dialog.setModal(true);
		dialog.setVisible(true);
	}

	/**
	 * Updates cboMod
	 * 
	 * @param dieType
	 */
	public static void upDateCboMod(String dieType) {
		if (dieType.equals("internal")) {
			cboMOD.setModel(new DefaultComboBoxModel(DatabaseConnection
					.getIntMODs().toArray()));
		} else {
			cboMOD.setModel(new DefaultComboBoxModel(DatabaseConnection
					.getExtMODs().toArray()));
		}
	}

	/**
	 * Toggles some components editable/uneditable
	 * 
	 * @param toggle
	 */
	private void toggleUpdateComponents() {
		txtLifetimeStart.setEnabled(!chckbxUpdateCommentsOnly.isSelected());
		txtLifeTimeEnd.setEnabled(!chckbxUpdateCommentsOnly.isSelected());
		cboPerformanceUpdate.setEnabled(!chckbxUpdateCommentsOnly.isSelected());
		cboPressUpdate.setEnabled(!chckbxUpdateCommentsOnly.isSelected());
	}

	/**
	 * Starts progress bar
	 */
	private void runProgressBar() {
		frame = new GUI_ProgressBar();
		frame.setSize(380, 120);
		frame.setUndecorated(true);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/**
	 * Validates dates before running die reports
	 */
	private boolean isValidDates() {
		if (dateChooserStartDate.getDate() == null) {
			JOptionPane.showMessageDialog(null, "Please select a start date",
					"Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		if (dateChooserEndDate.getDate() == null) {
			JOptionPane.showMessageDialog(null, "Please select an end date",
					"Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		return true;
	}

	/**
	 * Populates the report reference combo with job refs
	 */
	private void isColour() {
		cboReferenceReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getJobRefsReordered().toArray()));
		isVisibleColour(true);
	}

	/**
	 * Populates the report reference combo with product codes
	 */
	private void isVarnish() {
		cboReferenceReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getVarnishProductCodesReordered().toArray()));
		isVisibleColour(false);
	}

	/**
	 * Populates the report reference combo with product codes
	 */
	private void isAntimist() {
		cboReferenceReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getAntimistProductCodesReordered().toArray()));
		isVisibleColour(false);
	}

	/**
	 * Populates the report reference combo with product codes
	 */
	private void isAdhesive() {
		cboReferenceReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getAdhesiveProductCodesReordered().toArray()));
		isVisibleColour(false);
	}

	/**
	 * Populates the report reference combo with product codes
	 */
	private void isDatalase() {
		cboReferenceReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getDatalaseProductCodesReordered().toArray()));
		isVisibleColour(false);
	}

	/**
	 * 
	 * Toggles label and combo colour visible / invisible
	 * 
	 * @param toggle
	 */
	private void isVisibleColour(boolean toggle) {
		lblColour.setVisible(toggle);
		cboColourReport.setVisible(toggle);
	}

	/**
	 * Resets Reorder Report Combos
	 */
	private void resetReorderReportCombos() {
		cboReferenceReport.setModel(new DefaultComboBoxModel());
		cboYrReport.setModel(new DefaultComboBoxModel());
		cboColourReport.setModel(new DefaultComboBoxModel());
		cboReasonReport.setModel(new DefaultComboBoxModel());
		cboNameReport.setModel(new DefaultComboBoxModel());
	}

	/**
	 * Populates Report Reason Shift and Name combos
	 * 
	 * @param plateType
	 */
	private void populateReportReasonShiftNameCommon(String plateType) {
		cboYrReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getAllCommonReorderYr(plateType).toArray()));
		// cboShiftCodeOrder.setModel(new
		// DefaultComboBoxModel(DatabaseConnection
		// .getAllCommonReorderYr(plateType).toArray()));
		cboReasonReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getAllCommonReorderReasons(plateType).toArray()));
		cboNameReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getAllCommonReorderName(plateType).toArray()));
	}

	/**
	 * Populates Report Reason Shift and Name combos - colours
	 * 
	 * @param plateType
	 */
	private void populateReportReasonShiftNameColour() {

		cboYrReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getAllColourYr().toArray()));
		cboColourReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getAllColours().toArray()));
		cboReasonReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getAllColourReorderReasons().toArray()));
		cboNameReport.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getAllColourName().toArray()));
	}

	/**
	 * Makes sure the user has selected all required plates before using the
	 * copy function
	 * 
	 * @return
	 */
	private boolean isValidCopy() {

		try {
			if (plates.get(0).getReorderReason().equals("")
					|| plates.get(0).getDeliveryTimeline().equals("")) {
				JOptionPane
						.showMessageDialog(
								null,
								"Complete the details of the first plate before using the copy function",
								"Error", JOptionPane.ERROR_MESSAGE);
				return false;
			}
		} catch (IndexOutOfBoundsException i) {
			// Do nothing
		}

		return true;
	}

	/**
	 * Adds a new colour to colour table
	 * 
	 * @param
	 */
	private void addNewColour(String colour) {
		DatabaseConnection.insertColour(colour);
		cboColourComboBox.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getColours().toArray()));
	}

	/**
	 * Adds a new mod number
	 */
	private void addMod(boolean internal, boolean external) {

		String mod = null;
		String name = null;

		try {
			mod = JOptionPane.showInputDialog("Enter mod number:");

			if (mod != null) {
				name = JOptionPane.showInputDialog("Enter check in name:");
			}

			if (mod != null && name != null) {

				if (internal) {
					DatabaseConnection.insertManualMOD(internal, external,
							cboInternalDC.getSelectedItem().toString(), mod,
							name, new Date());
				} else {
					DatabaseConnection.insertManualMOD(internal, external,
							cboExtDC.getSelectedItem().toString(), mod, name,
							new Date());
				}

				populateIntData();
				populateExtData();
			}
		} catch (NullPointerException n) {

		}
	}
}// End class

class MyListener implements ActionListener {

	private int count = 1;

	private JLabel globe;

	public MyListener(JLabel globe, int count) {
		this.globe = globe;
		this.count = count;
	}

	public void actionPerformed(ActionEvent e) {

		globe.setIcon(new ImageIcon(GUI_HomeScreen.class
				.getResource("/image/layer" + count + ".png")));

		count++;

		if (count == 25) {
			count = 1;
		}
	}
}
