package ie.rapdb;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

public class GUI_Administrator extends JFrame {

	/**
	 * Local components
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<User> users;
	private JTextField txtUserName, txtPassword, txtPrice;
	private JComboBox comBoxUsertype, cboAddDiscountType, cboManufacturer,
			cboDieType, cboRepeat;
	private static JComboBox cboMOD;
	private JTable userJTable, tableDiePrices;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JRadioButton rdbtnInternal, rdbtnExternal;

	/**
	 * Create the frame.
	 */
	public GUI_Administrator() {
		setResizable(false);
		getContentPane().setBackground(new Color(112, 128, 144));
		setSize(new Dimension(1280, 730));

		getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(162, 182, 182), 3));

		panel.setBounds(35, 11, 1193, 670);
		getContentPane().add(panel);
		panel.setLayout(null);

		// ********User List Panel with buttons*******//
		JPanel panelUserList = new JPanel();
		panelUserList.setBounds(20, 11, 1152, 207);
		panel.add(panelUserList);
		panelUserList.setBorder(new TitledBorder(null, "User List",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelUserList.setLayout(null);

		JScrollPane userScrollPane = new JScrollPane();
		userScrollPane.setBounds(10, 21, 736, 172);
		panelUserList.add(userScrollPane);

		userJTable = new JTable();

		// Mouse listener to delete selected row
		userJTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (userJTable.getSelectedColumn() == 3) {

					int row = userJTable.getSelectedRow();
					deleteUser(row);
				}
			}
		});
		userScrollPane.setViewportView(userJTable);

		JButton btnAddUser = new JButton("Add");

		btnAddUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				addUser();
			}
		});
		btnAddUser.setBounds(232, 130, 89, 23);

		JPanel addUsersPanel = new JPanel();
		addUsersPanel.setBounds(769, 11, 373, 185);
		panelUserList.add(addUsersPanel);
		addUsersPanel.setBorder(new TitledBorder(UIManager
				.getBorder("TitledBorder.border"), "Add Users",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		addUsersPanel.setLayout(null);

		JLabel lblUserType = new JLabel("User Type ");

		lblUserType.setBounds(59, 95, 91, 26);
		addUsersPanel.add(lblUserType);

		JLabel lblUserName = new JLabel("User Name");

		lblUserName.setBounds(63, 21, 117, 26);
		addUsersPanel.add(lblUserName);

		txtUserName = new JTextField();
		txtUserName.setBounds(208, 24, 113, 20);
		addUsersPanel.add(txtUserName);
		txtUserName.setColumns(15);

		JLabel lblPassword = new JLabel("Password  ");

		lblPassword.setBounds(63, 58, 117, 26);
		addUsersPanel.add(lblPassword);

		txtPassword = new JTextField();
		txtPassword.setBounds(208, 61, 113, 20);
		addUsersPanel.add(txtPassword);
		txtPassword.setColumns(15);

		addUsersPanel.add(btnAddUser);

		comBoxUsertype = new JComboBox(DatabaseConnection.getUserTypes()
				.toArray());
		comBoxUsertype.setBounds(208, 95, 113, 24);
		addUsersPanel.add(comBoxUsertype);

		JPanel panelUnremoveDie = new JPanel();
		panelUnremoveDie.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Restore Die", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelUnremoveDie.setBounds(789, 229, 383, 140);
		panel.add(panelUnremoveDie);
		panelUnremoveDie.setLayout(null);

		rdbtnInternal = new JRadioButton("Internal");
		rdbtnInternal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				getRemovedMods("internal");
			}
		});
		buttonGroup.add(rdbtnInternal);
		rdbtnInternal.setSelected(true);
		rdbtnInternal.setBounds(6, 20, 109, 23);
		panelUnremoveDie.add(rdbtnInternal);

		rdbtnExternal = new JRadioButton("External");
		rdbtnExternal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				getRemovedMods("external");
			}
		});
		buttonGroup.add(rdbtnExternal);
		rdbtnExternal.setBounds(6, 59, 109, 23);
		panelUnremoveDie.add(rdbtnExternal);

		cboMOD = new JComboBox();
		cboMOD.setBounds(242, 40, 121, 20);
		panelUnremoveDie.add(cboMOD);

		JLabel lblModNumber = new JLabel("MOD Number");
		lblModNumber.setBounds(242, 20, 121, 14);
		panelUnremoveDie.add(lblModNumber);

		JButton btnRemove = new JButton("Unremove");
		btnRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String radio = new String();

				if (rdbtnInternal.isSelected()) {
					radio = "internal";
				} else {
					radio = "external";
				}

				String message = "Are you sure you want to unremove" + "\n"
						+ radio + " " + cboMOD.getSelectedItem().toString();

				if (JOptionPane.showConfirmDialog(null, message, "?",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					DatabaseConnection.unremoveMod(radio, cboMOD
							.getSelectedItem().toString());

					getRemovedMods(radio);

					GUI_HomeScreen.upDateCboMod(radio);
				}
			}
		});
		btnRemove.setBounds(274, 94, 89, 23);
		panelUnremoveDie.add(btnRemove);

		JPanel panelDiePrices = new JPanel();
		panelDiePrices.setBorder(new TitledBorder(null, "Die Prices",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelDiePrices.setBounds(20, 229, 748, 312);
		panel.add(panelDiePrices);
		panelDiePrices.setLayout(null);

		JScrollPane scrollPaneDiePrices = new JScrollPane();
		scrollPaneDiePrices.setBounds(10, 21, 728, 280);
		panelDiePrices.add(scrollPaneDiePrices);

		tableDiePrices = new JTable();
		scrollPaneDiePrices.setViewportView(tableDiePrices);
		tableDiePrices.setAutoCreateRowSorter(true);

		JPanel panelAddDiePrice = new JPanel();
		panelAddDiePrice.setBorder(new TitledBorder(null, "Add Die Price",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelAddDiePrice.setBounds(20, 552, 748, 107);
		panel.add(panelAddDiePrice);
		panelAddDiePrice.setLayout(null);

		cboAddDiscountType = new JComboBox(DatabaseConnection.getDiscountType()
				.toArray());
		cboAddDiscountType.setBounds(21, 46, 188, 20);
		panelAddDiePrice.add(cboAddDiscountType);

		JLabel lblDiscountType = new JLabel("Discount Type");
		lblDiscountType.setBounds(21, 27, 188, 14);
		panelAddDiePrice.add(lblDiscountType);

		JLabel lblManufacturer = new JLabel("Manufacturer");
		lblManufacturer.setBounds(244, 27, 130, 14);
		panelAddDiePrice.add(lblManufacturer);

		cboManufacturer = new JComboBox(DatabaseConnection.getManufacturer()
				.toArray());
		cboManufacturer.setBounds(244, 46, 130, 20);
		panelAddDiePrice.add(cboManufacturer);

		JLabel lblDieType = new JLabel("Die Type");
		lblDieType.setBounds(402, 27, 95, 14);
		panelAddDiePrice.add(lblDieType);

		cboDieType = new JComboBox(DatabaseConnection.getDieType().toArray());
		cboDieType.setBounds(402, 46, 95, 20);
		panelAddDiePrice.add(cboDieType);

		JLabel lblRepeat = new JLabel("Repeat");
		lblRepeat.setBounds(519, 27, 95, 14);
		panelAddDiePrice.add(lblRepeat);

		cboRepeat = new JComboBox(DatabaseConnection.getRepeatList().toArray());
		cboRepeat.setBounds(519, 46, 95, 20);
		panelAddDiePrice.add(cboRepeat);

		txtPrice = new JTextField();
		txtPrice.setBounds(636, 46, 86, 20);
		panelAddDiePrice.add(txtPrice);
		txtPrice.setColumns(10);

		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(636, 27, 86, 14);
		panelAddDiePrice.add(lblPrice);

		JButton btnNewButton = new JButton("Add");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (txtPrice.getText().equals("")) {
					JOptionPane.showMessageDialog(null,
							"You must enter a price", "Error",
							JOptionPane.ERROR_MESSAGE);
				} else {
					String message = "Are you sure you want to add the following price:"
							+ "\n"
							+ cboAddDiscountType.getSelectedItem().toString()
							+ "\n"
							+ cboManufacturer.getSelectedItem().toString()
							+ "\n"
							+ cboDieType.getSelectedItem().toString()
							+ "\n"
							+ cboRepeat.getSelectedItem().toString()
							+ "\n" + txtPrice.getText();

					if (JOptionPane.showConfirmDialog(null, message,
							"Add Price?", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
						DiePrice diePrice = new DiePrice(cboDieType
								.getSelectedItem().toString(), cboRepeat
								.getSelectedItem().toString(), cboManufacturer
								.getSelectedItem().toString(), Double
								.parseDouble(txtPrice.getText()),
								cboAddDiscountType.getSelectedItem().toString());

						DatabaseConnection.insertDiePrice(diePrice);

						txtPrice.setText("");

						displayDiePrices();
					}
				}
			}
		});
		btnNewButton.setBounds(636, 77, 86, 23);
		panelAddDiePrice.add(btnNewButton);

		// Pass methods to main frame
		getUsers();
		getRemovedMods("internal");
		displayDiePrices();
	}

	// *****************************
	//
	// User Panel Methods
	//
	// *****************************
	// Add new user to the database
	private void addUser() {

		if (isValidData()) {
			DatabaseConnection.addUser(txtUserName.getText(), txtPassword
					.getText(), comBoxUsertype.getSelectedItem().toString());
			getUsers();

			clearUserTextFields();

			txtUserName.requestFocus();
		}
	}

	/**
	 * Validates new user data
	 * 
	 * @return
	 */
	private boolean isValidData() {

		String userName = txtUserName.getText();
		String password = txtPassword.getText();
		String userType = (String) comBoxUsertype.getSelectedItem();

		if (txtUserName.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Please enter a username",
					"Error", JOptionPane.INFORMATION_MESSAGE);
			return false;
		}

		if (txtPassword.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "Please enter a password",
					"Error", JOptionPane.INFORMATION_MESSAGE);
			return false;

		}

		return true;
	}

	/**
	 * Delete user
	 * 
	 * @param row
	 *            selected row to delete
	 */
	private void deleteUser(int row) {

		try {

			User selectedUser = users.get(row);

			DatabaseConnection.deleteUser(selectedUser.getUserName());

			getUsers();
			JOptionPane.showMessageDialog(null, "User removed");
		} catch (Exception i) {
			JOptionPane.showMessageDialog(null, "Please select user !",
					"Error", JOptionPane.INFORMATION_MESSAGE);
		}
	}

	// Get users from the database
	private void getUsers() {

		users = new ArrayList<User>();
		users = DatabaseConnection.getUsers();

		userJTable.setModel(new UserTableModel(users));
		// Get column for delete button
		userJTable.getColumn("").setCellRenderer(new UserButtonRenderer());

		// User type combo
		TableColumn userColumn = userJTable.getColumnModel().getColumn(2);

		JComboBox cboUserType = new JComboBox(DatabaseConnection.getUserTypes()
				.toArray());

		userColumn.setCellEditor(new DefaultCellEditor(cboUserType));
	}

	// Methods to Return JTextFields
	public JTextField getUserName() {
		return txtUserName;
	}

	public JTextField getPassword() {
		return txtPassword;
	}

	@SuppressWarnings("rawtypes")
	public JComboBox getUserType() {
		return comBoxUsertype;
	}

	public JCheckBox isCurrentlyAvailable() {
		return null;

	}

	// Method to clear user text fields
	public void clearUserTextFields() {
		getUserName().setText("");
		getPassword().setText("");
		getUserType().setToolTipText("");
	}

	public static void getRemovedMods(String dieType) {
		try {
			if (dieType.equals("internal")) {
				cboMOD.setModel(new DefaultComboBoxModel(DatabaseConnection
						.getRemovedModNumbers("internal").toArray()));
			} else {
				cboMOD.setModel(new DefaultComboBoxModel(DatabaseConnection
						.getRemovedModNumbers("external").toArray()));
			}
		} catch (NullPointerException n) {
			// Do nothing
		}

	}

	/**
	 * Displays the die prices in the table
	 */
	private void displayDiePrices() {
		tableDiePrices.setModel(new TableModel_DieCost(DatabaseConnection
				.getDieCost()));

		// Discount combo
		TableColumn discountColumn = tableDiePrices.getColumnModel().getColumn(
				0);
		JComboBox cboDiscount = new JComboBox(DatabaseConnection
				.getDiscountType().toArray());
		discountColumn.setCellEditor(new DefaultCellEditor(cboDiscount));
		discountColumn.setPreferredWidth(110);

		// Manufacturer combo
		TableColumn manuColumn = tableDiePrices.getColumnModel().getColumn(1);
		JComboBox cboManu = new JComboBox(DatabaseConnection.getManufacturer()
				.toArray());
		manuColumn.setCellEditor(new DefaultCellEditor(cboManu));
		manuColumn.setPreferredWidth(70);

		// Die type combo
		TableColumn dieTypeColumn = tableDiePrices.getColumnModel()
				.getColumn(2);
		JComboBox cboDieType = new JComboBox(DatabaseConnection.getDieType()
				.toArray());
		dieTypeColumn.setCellEditor(new DefaultCellEditor(cboDieType));
		dieTypeColumn.setPreferredWidth(50);

		// Repeat combo
		TableColumn repeatColumn = tableDiePrices.getColumnModel().getColumn(3);
		JComboBox cboRepeat = new JComboBox(DatabaseConnection.getRepeatList()
				.toArray());
		repeatColumn.setCellEditor(new DefaultCellEditor(cboRepeat));
		repeatColumn.setPreferredWidth(50);

		// Price column
		TableColumn priceColumn = tableDiePrices.getColumnModel().getColumn(4);
		priceColumn.setPreferredWidth(50);

		// JButton
		tableDiePrices.getColumn("").setCellRenderer(new UserButtonRenderer());
	}
}
