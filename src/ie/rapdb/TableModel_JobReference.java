package ie.rapdb;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.table.AbstractTableModel;

public class TableModel_JobReference extends AbstractTableModel{
    
    private ArrayList<JobReference> jobReferences;

    private String[] columnNames = { "Job Reference", "Customer", "Varnish YR", "Obsolete" };

    public TableModel_JobReference(ArrayList<JobReference> jobReferences) {
	this.jobReferences = jobReferences;
    }

    @Override
    public int getColumnCount() {
	return columnNames.length;
    }

    @Override
    public int getRowCount() {
	return jobReferences.size();
    }

    @Override
    public String getColumnName(int col) {
	return columnNames[col];
    }

    @Override
    public Object getValueAt(int row, int col) {

	JobReference j = jobReferences.get(row);

	switch (col) {

	case 0:
	    return j.getJobReference();
	case 1:
	    return j.getCustomer();
	case 2:
	    return j.getVarnishYr();
	case 3:
	    return j.isObsolete();
	}

	return null;
    }

    public void setValueAt(Object value, int row, int col) {
	
	JobReference j = jobReferences.get(row);
	
	switch (col) {
	
	case 1:
	    j.setCustomer((String) value);
	    break;
	case 2:
	    j.setVarnishYr((String) value);
	    break;
	case 3:
		j.setObsolete((Boolean) value);
		break;
	}
	
	DatabaseConnection.updateJobReference(j);
    }

    // getColumnClass
    @Override
    public Class<?> getColumnClass(int col) {
    	
    	//return getValueAt(0, col).getClass();

	Class[] columns = new Class[] { String.class, JComboBox.class,
		String.class, Boolean.class };

	return columns[col];
    }

    @Override
    public boolean isCellEditable(int row, int col) {

	if (col == 0) {
	    return false;
	}
	return true;

    }

}
