/**
 * 
 */
package ie.rapdb;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

/**
 * @author Corwin
 *
 */
public class TableModelDiePerformance extends AbstractTableModel{
    
    private ArrayList<DiePerformance> dies;

    private String[] columnNames = { "Run Date", "MOD Number", "Comments", "Job Revolutions", "Update Name"};

    public TableModelDiePerformance(ArrayList<DiePerformance> dies) {
	this.dies = dies;
    }

    @Override
    public int getColumnCount() {
	return columnNames.length;
    }

    @Override
    public String getColumnName(int col) {
	return columnNames[col];
    }

    @Override
    public int getRowCount() {
	return dies.size();
    }

    // Get values for JTable
    @Override
    public Object getValueAt(int row, int col) {

	DiePerformance d = dies.get(row);

	switch (col) {
	
	case 0:
	    return d.getRunDate();
	case 1:
	    return d.getModNo();
	case 2:
	    return d.getComments();
	case 3:
	    return d.getJobRevs();
	case 4:
	    return d.getUpdateName();
	}

	return null;
    }

}
