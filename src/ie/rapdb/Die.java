/**
 * 
 */
package ie.rapdb;

import java.sql.Date;

/**
 * @author Corwin
 *
 */
public class Die {
	
    private int id;
    private String DC_Number;
    private String modNo;
    private int TotalRevs;
    private Date dueDate;
    private Date removedDate;
    private String removedReason;
    private boolean firstEmail;
    private boolean secondEmail;
    private double dieCost;
    
    /**
     * @param modNo
     * @param totalRevs
     */
    public Die() {
    	
    id = 0;	
	DC_Number = new String();
	modNo = new String();
	TotalRevs = 0;
	dueDate = new Date(0);
	removedDate = new Date(0);
	removedReason = new String();
	firstEmail = false;
	secondEmail = false;
	dieCost = 0;
    }
    
    public Die(int id, String DC_Number, Date dueDate) {
        
        this.id = id;
    	this.DC_Number = DC_Number;
    	this.modNo = new String();
    	TotalRevs = 0;
    	this.dueDate = dueDate;
    	removedDate = new Date(0);
    	removedReason = new String();
    	firstEmail = false;
    	secondEmail = false;
    	dieCost = 0;
        }
    
    /**
     * @param dC_Number
     * @param totalRevs
     */
    public Die(int id, String dC_Number, int totalRevs, boolean firstEmail, boolean secondEmail) {
    	
    this.id = id;
	DC_Number = dC_Number;
	this.modNo = new String();
	TotalRevs = totalRevs;
	dueDate = new Date(0);
	removedDate = new Date(0);
	removedReason = new String();
	this.firstEmail = firstEmail;
	this.secondEmail = secondEmail;
	dieCost = 0;
    }

    /**
     * @param modNo
     * @param totalRevs
     */
    public Die(String modNo, int totalRevs, Date dueDate) {
    
    id = 0;
	DC_Number = new String();
	this.modNo = modNo;
	TotalRevs = totalRevs;
	this.dueDate = dueDate;
	removedDate = new Date(0);
	removedReason = new String();
	firstEmail = false;
	secondEmail = false;
	dieCost = 0;
    }
    
    /**
     * @param dC_Number
     * @param modNo
     * @param totalRevs
     */
    public Die(String dC_Number, String modNo, int totalRevs, Date removedDate, String removedReason) {
    	
    id = 0;
	DC_Number = dC_Number;
	this.modNo = modNo;
	TotalRevs = totalRevs;
	this.removedDate = removedDate;
	this.removedReason = removedReason;
	dueDate = new Date(0);
	firstEmail = false;
	secondEmail = false;
	dieCost = 0;
    }
    
    public Die(int id, String dC_Number, String modNo, Date dueDate) {
    	
    this.id = id;	
	DC_Number = dC_Number;
	this.modNo = modNo;
	TotalRevs = 0;
	this.removedDate = new Date(0);
	this.removedReason = new String();
	this.dueDate = dueDate;
	firstEmail = false;
	secondEmail = false;
	dieCost = 0;
    }

    public Die(String dC_Number, String modNo, Date removedDate, String removedReason,
			double dieCost) {
    	
    	id = 0;
		this.DC_Number = dC_Number;
		this.modNo = modNo;
		this.removedDate = removedDate;
		this.removedReason = removedReason;
		this.dieCost = dieCost;
	}
    
    

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
     * @return the dC_Number
     */
    public String getDC_Number() {
        return DC_Number;
    }
    /**
     * @param dC_Number the dC_Number to set
     */
    public void setDC_Number(String dC_Number) {
        DC_Number = dC_Number;
    }
    /**
     * @return the modNo
     */
    public String getModNo() {
        return modNo;
    }
    /**
     * @param modNo the modNo to set
     */
    public void setModNo(String modNo) {
        this.modNo = modNo;
    }
    /**
     * @return the totalRevs
     */
    public int getTotalRevs() {
        return TotalRevs;
    }
    /**
     * @param totalRevs the totalRevs to set
     */
    public void setTotalRevs(int totalRevs) {
        TotalRevs = totalRevs;
    }
    /**
     * @return the updateName
     */
    public Date getDueDate() {
        return dueDate;
    }
    /**
     * @param updateName the updateName to set
     */
    public void setDate(Date dueDate) {
        this.dueDate = dueDate;
    }
    
    /**
     * @return the removedDate
     */
    public Date getRemovedDate() {
        return removedDate;
    }
    /**
     * @param removedDate the removedDate to set
     */
    public void setRemovedDate(Date removedDate) {
        this.removedDate = removedDate;
    }
    
    
    public String getRemovedReason() {
		return removedReason;
	}

	public void setRemovedReason(String removedReason) {
		this.removedReason = removedReason;
	}

	public boolean isFirstEmail() {
		return firstEmail;
	}

	public void setFirstEmail(boolean firstEmail) {
		this.firstEmail = firstEmail;
	}

	public boolean isSecondEmail() {
		return secondEmail;
	}

	public void setSecondEmail(boolean secondEmail) {
		this.secondEmail = secondEmail;
	}
	

	public double getDieCost() {
		return dieCost;
	}

	public void setDieCost(double dieCost) {
		this.dieCost = dieCost;
	}

	@Override
	public String toString() {
		return "Die [DC_Number=" + DC_Number + ", modNo=" + modNo
				+ ", TotalRevs=" + TotalRevs + ", dueDate=" + dueDate
				+ ", removedDate=" + removedDate + ", removedReason="
				+ removedReason + ", firstEmail=" + firstEmail
				+ ", secondEmail=" + secondEmail + ", dieCost=" + dieCost + "]";
	}

	
    
    
    
    
    
    
    
    
    
    

}
