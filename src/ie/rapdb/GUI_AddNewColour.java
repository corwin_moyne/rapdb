package ie.rapdb;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import java.awt.Color;

public class GUI_AddNewColour extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private DefaultTableModel colourTable = new DefaultTableModel(1, 2);
	private JTable tableColours;
	private JComboBox cboQty, cboColourComboBox;
	private String originalColour;
	private ArrayList<PlateDetail> plateDetails;
	private String jobReference;
	private static boolean complete;
	private String yrNo;

//	/**
//	 * Launch the application.
//	 */
//	public static void main(String[] args) {
//		try {
//			GUI_AddNewColour dialog = new GUI_AddNewColour(null);
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public GUI_AddNewColour(String jobRef) {

		jobReference = jobRef;

		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(112, 128, 144));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JScrollPane scrollPaneColours = new JScrollPane();
		scrollPaneColours.setBounds(10, 44, 414, 174);
		contentPanel.add(scrollPaneColours);

		tableColours = new JTable(colourTable);
		tableColours.addInputMethodListener(new InputMethodListener() {
			public void caretPositionChanged(InputMethodEvent arg0) {
			}

			public void inputMethodTextChanged(InputMethodEvent arg0) {

				//System.out.println("Test");
			}
		});
		tableColours.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {

				originalColour = (String) tableColours.getValueAt(
						tableColours.getSelectedRow(),
						tableColours.getSelectedColumn());
			}
		});
		String[] newIdentifiers = { "Colour", "YR Number" };
		colourTable.setColumnIdentifiers(newIdentifiers);
		cboColourComboBox = new JComboBox(DatabaseConnection.getColours()
				.toArray());

		TableColumn colourColumn = tableColours.getColumnModel().getColumn(0);
		colourColumn.setCellEditor(new DefaultCellEditor(cboColourComboBox));
		cboColourComboBox.setEditable(true);
		cboColourComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String other = new String();

				try {
					other = cboColourComboBox.getSelectedItem().toString();
				} catch (NullPointerException n) {
					// Do nothing
				}

				if (other.equals("-ADD NEW-")) {

					String newColour = JOptionPane
							.showInputDialog("Enter colour");

					if (newColour != null) {
						if (JOptionPane
								.showConfirmDialog(
										null,
										"Would you like to add this colour to the list?",
										"New colour", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
							addNewColour(newColour);
						}

						cboColourComboBox.setSelectedItem(newColour);
					} else {
						cboColourComboBox.setSelectedItem(originalColour);
					}
				}
			}
		});

		scrollPaneColours.setViewportView(tableColours);
		changeTableProperties();
		{
			cboQty = new JComboBox();
			cboQty.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

					colourTable.setRowCount(Integer.parseInt(cboQty
							.getSelectedItem().toString()));
				}
			});
			cboQty.setModel(new DefaultComboBoxModel(new String[] { "1", "2",
					"3", "4", "5", "6", "7", "8", "9", "10" }));
			cboQty.setSelectedIndex(0);
			cboQty.setBounds(129, 11, 87, 20);
			contentPanel.add(cboQty);
		}
		{
			JLabel lblNewLabel = new JLabel("No. of colours");
			lblNewLabel.setForeground(Color.WHITE);
			lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNewLabel.setBounds(10, 14, 109, 14);
			contentPanel.add(lblNewLabel);
		}
		{
			JLabel lblJobRef = new JLabel(jobReference);
			lblJobRef.setForeground(Color.WHITE);
			lblJobRef.setHorizontalAlignment(SwingConstants.CENTER);
			lblJobRef.setFont(new Font("Tahoma", Font.BOLD, 16));
			lblJobRef.setBounds(280, 11, 144, 20);
			contentPanel.add(lblJobRef);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						if (isDataValid()) {
							if (plateDetails.size() == 0) {

								JOptionPane.showMessageDialog(null,
										"You have not added any colours",
										"Error", JOptionPane.ERROR_MESSAGE);

							} else {
								
								String name = JOptionPane.showInputDialog("Enter checkin name").toUpperCase();
								
								String message = new String();

								message += "You are about to add the following to "
										+ jobReference + ":";

								for (PlateDetail plate : plateDetails) {
									message += "\nColour: "
											+ plate.getInkRefString()
											+ ", YR Number: "
											+ plate.getYrNumber();
								}

								message += "\nIs this correct?";

								if (JOptionPane.showConfirmDialog(null,
										message, "", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

									int[] ids = DatabaseConnection.addPlate(plateDetails,
											jobReference);
									
									int i = 0;
									for (PlateDetail plate : plateDetails) {
										DatabaseConnection.insertPlateCheckin(name, ids[i], false);
										DatabaseConnection.incrementColourPlateStock(ids[i++]);
									}

									complete = true;

									dispose();
								}
							}
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	/**
	 * Checks user data before adding to database
	 * @return
	 */
	private boolean isDataValid() {

		plateDetails = new ArrayList<PlateDetail>();

		for (int row = 0; row < colourTable.getRowCount(); row++) {

			if (colourTable.getValueAt(row, 0) != null) {

				if (colourTable.getValueAt(row, 1) == null) {

					JOptionPane.showMessageDialog(null, "YR cannot be blank",
							"Error", JOptionPane.ERROR_MESSAGE);
					return false;

				} else {
					plateDetails.add(new PlateDetail((String) colourTable
							.getValueAt(row, 0), (String) colourTable
							.getValueAt(row, 1)));
				}

			}
		}

		return true;

	}

	/**
	 * Adds a new customer to customer table
	 * 
	 * @param customerName
	 */
	private void addNewColour(String colour) {
		DatabaseConnection.insertColour(colour);
		cboColourComboBox.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getColours().toArray()));
	}

	/**
	 * Removes the need for hitting enter to validate data entered into table
	 */
	private void changeTableProperties() {
		// now hitting "enter" will trigger the "default button" action:
		// (see source code of JTable.CellEditorRemover.propertyChange() method
		// for more information)
		tableColours.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "none");
		// now you can click on the button without having to hit "enter" to
		// finish editing the row:
		// see
		// http://download.oracle.com/javase/tutorial/uiswing/misc/keybinding.html
		// for more information
		tableColours
				.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
	}

	/**
	 * Returns true when plates have been added successfully
	 * 
	 * @return
	 */
	public static boolean isComplete() {
		return complete;
	}
}
