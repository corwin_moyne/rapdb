/**
 * 
 */
package ie.rapdb;

import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * @author Corwin
 *
 */
public class LabelRenderer extends JLabel implements TableCellRenderer {

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
		    boolean isSelected, boolean hasFocus, int row, int col) {
		
		JLabel label = (JLabel) (value);
		
		return label;
	}

}
