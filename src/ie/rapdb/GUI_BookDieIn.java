package ie.rapdb;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.text.MaskFormatter;

public class GUI_BookDieIn extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txtName;
	private ButtonGroup bg = new ButtonGroup();
	private JComboBox cboDieCode;
	private JCheckBox chckbxDieLineCheck, chckbxDamagecheck, chckbxSampleCheck;
	private JRadioButton rdbtnInternal, rdbtnExternal;
	private JTextField txtDueDate;
	private ArrayList<Die> dies;
	private JFormattedTextField txtMOD;

	/**
	 * Create the dialog.
	 */
	public GUI_BookDieIn() {
		setBounds(100, 100, 549, 269);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(112, 128, 144));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel label = new JLabel("");
			label.setBounds(219, 21, 0, 0);
			contentPanel.add(label);
		}
		{
			JPanel panelChecks = new JPanel();
			panelChecks.setBorder(new LineBorder(new Color(162, 182, 182), 2));
			panelChecks.setBounds(25, 109, 167, 110);
			contentPanel.add(panelChecks);
			panelChecks.setLayout(new GridLayout(3, 1, 0, 0));
			{
				chckbxDieLineCheck = new JCheckBox("Die Line Check");
				chckbxDieLineCheck.setForeground(new Color(112, 128, 144));
				chckbxDieLineCheck.setFont(new Font("Tahoma", Font.PLAIN, 14));
				panelChecks.add(chckbxDieLineCheck);
			}
			{
				chckbxDamagecheck = new JCheckBox("Damage Check");
				chckbxDamagecheck.setForeground(new Color(112, 128, 144));
				chckbxDamagecheck.setFont(new Font("Tahoma", Font.PLAIN, 14));
				panelChecks.add(chckbxDamagecheck);
			}
			{
				chckbxSampleCheck = new JCheckBox("Sample Check");
				chckbxSampleCheck.setForeground(new Color(112, 128, 144));
				chckbxSampleCheck.setFont(new Font("Tahoma", Font.PLAIN, 14));
				panelChecks.add(chckbxSampleCheck);
			}
		}
		{
			JPanel panelRadios = new JPanel();
			panelRadios.setBorder(new LineBorder(new Color(162, 182, 182), 2));
			panelRadios.setBounds(25, 21, 167, 64);
			contentPanel.add(panelRadios);
			panelRadios.setLayout(new GridLayout(2, 1, 0, 0));
			{
				rdbtnInternal = new JRadioButton("Internal");
				rdbtnInternal.setForeground(new Color(112, 128, 144));
				rdbtnInternal.setFont(new Font("Tahoma", Font.PLAIN, 14));
				rdbtnInternal.setSelected(true);
				rdbtnInternal.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {

						getUncheckedDies();
						displayDieCodes();
						displayDieDueDate(0);
					}
				});
				bg.add(rdbtnInternal);
				panelRadios.add(rdbtnInternal);
			}
			{
				rdbtnExternal = new JRadioButton("External");
				rdbtnExternal.setForeground(new Color(112, 128, 144));
				rdbtnExternal.setFont(new Font("Tahoma", Font.PLAIN, 14));
				rdbtnExternal.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {

						getUncheckedDies();
						displayDieCodes();
						displayDieDueDate(0);
					}
				});
				bg.add(rdbtnExternal);
				panelRadios.add(rdbtnExternal);
			}
		}
		{
			JPanel panelOther = new JPanel();
			panelOther.setBorder(new LineBorder(new Color(162, 182, 182), 2));
			panelOther.setBounds(219, 21, 290, 110);
			contentPanel.add(panelOther);
			panelOther.setLayout(new GridLayout(4, 2, 5, 5));
			{
				JLabel lblDieCode = new JLabel("DieCode");
				lblDieCode.setForeground(new Color(112, 128, 144));
				lblDieCode.setFont(new Font("Tahoma", Font.PLAIN, 14));
				panelOther.add(lblDieCode);
				lblDieCode.setVerticalAlignment(SwingConstants.BOTTOM);
			}

			JLabel lblDate = new JLabel("Due Date");
			lblDate.setForeground(new Color(112, 128, 144));
			lblDate.setFont(new Font("Tahoma", Font.PLAIN, 14));
			panelOther.add(lblDate);
			lblDate.setVerticalAlignment(SwingConstants.BOTTOM);
			cboDieCode = new JComboBox();
			panelOther.add(cboDieCode);
			{

				txtDueDate = new JTextField();
				panelOther.add(txtDueDate);
				txtDueDate.setEditable(false);
				txtDueDate.setColumns(10);
			}

			JLabel lblName = new JLabel("Name");
			lblName.setForeground(new Color(112, 128, 144));
			lblName.setFont(new Font("Tahoma", Font.PLAIN, 14));
			panelOther.add(lblName);
			lblName.setVerticalAlignment(SwingConstants.BOTTOM);

			JLabel lblModNumber = new JLabel("MOD Number");
			lblModNumber.setForeground(new Color(112, 128, 144));
			lblModNumber.setFont(new Font("Tahoma", Font.PLAIN, 14));
			lblModNumber.setVerticalAlignment(SwingConstants.BOTTOM);
			panelOther.add(lblModNumber);
			{
				txtName = new JTextField();
				panelOther.add(txtName);
				txtName.setColumns(10);
			}

			txtMOD = new JFormattedTextField(
					createFormatter("AA'-##'-#####******"));
			panelOther.add(txtMOD);
			cboDieCode.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					displayDieDueDate(cboDieCode.getSelectedIndex());
				}
			});
		}

		JButton btnCancel = new JButton("Close");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				cancel();
				dispose();
			}
		});
		btnCancel.setBounds(444, 196, 65, 23);
		contentPanel.add(btnCancel);

		JButton btnSubmit = new JButton("Submit");
		getRootPane().setDefaultButton(btnSubmit);
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (cboDieCode.getSelectedIndex() == -1) {

					JOptionPane.showMessageDialog(null, "No die code selected",
							"Error", JOptionPane.ERROR_MESSAGE);

				}

				else if (validData()) {

					if (JOptionPane.showConfirmDialog(null, "Are you sure?",
							"WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

						String dieType = "";

						if (rdbtnInternal.isSelected()) {
							dieType = "internal";
						} else {
							dieType = "external";
						}
						upDateDieCheck(dieType);

						getUncheckedDies();
						displayDieCodes();

						cancel();
					}
				}

			}

		});
		btnSubmit.setBounds(371, 196, 65, 23);
		contentPanel.add(btnSubmit);

		JLabel lblChecks = new JLabel("Checks");
		lblChecks.setForeground(Color.WHITE);
		lblChecks.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblChecks.setBounds(25, 94, 167, 14);
		contentPanel.add(lblChecks);
		
		JButton btnEmail = new JButton("Email");
		btnEmail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				openGUI_EmailDarren();
			}
		});
		btnEmail.setBounds(296, 196, 65, 23);
		contentPanel.add(btnEmail);

		onFormLoad();

	}

	/**
	 * Runs when form loads
	 */
	private void onFormLoad() {
		getUncheckedDies();
		displayDieCodes();
		displayDieDueDate(0);
	}

	/**
	 * Updates the die check data on die
	 */
	private void upDateDieCheck(String dieType) {

		DatabaseConnection.updateDieCheckData(
				dies.get(cboDieCode.getSelectedIndex()).getId(), dieType,
				txtMOD.getText().toUpperCase(), txtName.getText());
	}

	/**
	 * Validate user input before sending to database
	 * 
	 * @return
	 */
	private boolean validData() {

		if (!chckbxDamagecheck.isSelected() || !chckbxDieLineCheck.isSelected()
				|| !chckbxSampleCheck.isSelected()) {
			JOptionPane.showMessageDialog(null,
					"You must complete all three checks to continue", "Error",
					JOptionPane.INFORMATION_MESSAGE);
			return false;
		}

		if (txtName.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "You must enter your name",
					"Error", JOptionPane.INFORMATION_MESSAGE);
			return false;
		}

		if (txtMOD.getText().equals("")) {
			JOptionPane.showMessageDialog(null, "You must enter an MOD Number",
					"Error", JOptionPane.INFORMATION_MESSAGE);
			return false;
		}

		return true;
	}

	/**
	 * Cancels user input and closes form
	 */
	private void cancel() {
		chckbxDamagecheck.setSelected(false);
		chckbxDieLineCheck.setSelected(false);
		chckbxSampleCheck.setSelected(false);
		txtName.setText("");
		txtMOD.setText("");
	}

	/**
	 * Retrieves unchecked dies from the database
	 */
	private void getUncheckedDies() {

		String dieType = "external";

		if (rdbtnInternal.isSelected()) {
			dieType = "internal";
		}

		dies = DatabaseConnection.getUncheckedMODs(dieType);
	}

	/**
	 * Displays die codes in the combo
	 */
	private void displayDieCodes() {

		String[] dieCodes = new String[dies.size()];

		for (int i = 0; i < dies.size(); i++) {
			dieCodes[i] = dies.get(i).getDC_Number();
		}

		cboDieCode.setModel(new DefaultComboBoxModel(dieCodes));
	}

	/**
	 * Displays the due date of the die selected in the combo
	 * 
	 * @param index
	 */
	private void displayDieDueDate(int index) {
		try
		{
			txtDueDate.setText(dies.get(index).getDueDate().toString());
		}
		catch(IndexOutOfBoundsException i)
		{
			//Do nothing
		}
	}

	// Creates a mask formatter used for the mod number
	protected MaskFormatter createFormatter(String s) {
		MaskFormatter formatter = null;
		try {
			formatter = new MaskFormatter(s);
		} catch (java.text.ParseException exc) {

		}
		return formatter;
	}
	
	/**
	 * Opens GUI_EmailDarren so user can book a die in
	 */
	private void openGUI_EmailDarren() {

		this.setModal(false);
		GUI_EmailDarren dialog = new GUI_EmailDarren();
		dialog.setSize(750, 300);
		dialog.setTitle("Enter die information");
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setLocationRelativeTo(null);
		dialog.setModal(true);
		dialog.setVisible(true);
	}
	
	public static void close()
	{
		//dispose();
	}
}// End class

