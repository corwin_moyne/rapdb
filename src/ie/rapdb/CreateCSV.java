package ie.rapdb;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * 
 */

/**
 * @author Corwin
 * 
 */
public class CreateCSV {

	private static FileWriter writer;
	private static ArrayList<PlateCheckin> plateCheckins;
	private static ArrayList<ReorderColourPlate> plates;

	public static void createCSV(String filename,
			ArrayList<PlateCheckin> myPlateCheckins) throws IOException {

		writer = new FileWriter(filename + ".csv");
		plateCheckins = myPlateCheckins;

		if (filename.equals("Colour")) {
			createHeaders();
			addData();
		} else {
			createCommonHeaders();
			addCommonData();
		}

		closeWriter();
	}

	public static void createReorderedColourCSV(String filename,
			ArrayList<ReorderColourPlate> reorderedPlates) throws IOException {

		writer = new FileWriter(filename + ".csv");
		plates = reorderedPlates;

		createReorderedColourHeaders();
		addReorderedColourData();

		closeWriter();
	}

	private static void createHeaders() throws IOException {
		writer.append("Reference");
		writer.append(",");
		writer.append("Ink Reference");
		writer.append(",");
		writer.append("YR Reference");
		writer.append(",");
		writer.append("Current Stock");
		writer.append(",");
		writer.append("Checkin Date");
		writer.append(",");
		writer.append("Checkin Name");
		writer.append("\n");
	}

	private static void createReorderedColourHeaders() throws IOException {
		writer.append("Reference");
		writer.append(",");
		writer.append("YR Reference");
		writer.append(",");
		writer.append("Ink Reference");
		writer.append(",");
		writer.append("Qty");
		writer.append(",");
		writer.append("Re-order Reason");
		writer.append(",");
		writer.append("Shift");
		writer.append(",");
		writer.append("Name");
		writer.append(",");
		writer.append("Date/Time");
		writer.append("\n");
	}

	private static void createCommonHeaders() throws IOException {
		writer.append("Product Code");
		writer.append(",");
		writer.append("Reference (varnish only)");
		writer.append(",");
		writer.append("Plate Type");
		writer.append(",");
		writer.append("YR Reference");
		writer.append(",");
		writer.append("Current Stock");
		writer.append(",");
		writer.append("Checkin Date");
		writer.append(",");
		writer.append("Checkin Name");
		writer.append("\n");
	}

	private static void addData() throws IOException {
		for (PlateCheckin plateCheckin : plateCheckins) {
			writer.append(plateCheckin.getJobRef());
			writer.append(",");
			writer.append(plateCheckin.getInkRef());
			writer.append(",");
			writer.append(plateCheckin.getYrNumber());
			writer.append(",");
			writer.append(String.valueOf(plateCheckin.getStock()));
			writer.append(",");
			writer.append(String.valueOf(plateCheckin.getCheckinDate()));
			writer.append(",");
			writer.append(plateCheckin.getCheckinName());
			writer.append(",");
			writer.append("\n");
		}
	}
	
	private static void addReorderedColourData() throws IOException {
		for (ReorderColourPlate plate : plates) {
			
			String date = new SimpleDateFormat("dd/MM/yyyy").format(plate.getTimeStamp());
			
			writer.append(plate.getJobRef());
			writer.append(",");
			writer.append(plate.getYr_number());
			writer.append(",");
			writer.append(plate.getInkRef());
			writer.append(",");
			writer.append(String.valueOf(plate.getOrderQty()));
			writer.append(",");
			writer.append(plate.getReorderReason());
			writer.append(",");
			writer.append(plate.getShift());
			writer.append(",");
			writer.append(plate.getName());
			writer.append(",");
			writer.append(date);
			writer.append("\n");
		}
	}

	private static void addCommonData() throws IOException {
		for (PlateCheckin plateCheckin : plateCheckins) {
			writer.append(plateCheckin.getProductCode());
			writer.append(",");
			writer.append(plateCheckin.getJobRef());
			writer.append(",");
			writer.append(plateCheckin.getPlateType());
			writer.append(",");
			writer.append(plateCheckin.getYrNumber());
			writer.append(",");
			writer.append(String.valueOf(plateCheckin.getStock()));
			writer.append(",");
			writer.append(String.valueOf(plateCheckin.getCheckinDate()));
			writer.append(",");
			writer.append(plateCheckin.getCheckinName());
			writer.append("\n");
		}
	}

	private static void closeWriter() throws IOException {
		writer.flush();
		writer.close();
	}

}
