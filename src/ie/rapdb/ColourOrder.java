/**
 * 
 */
package ie.rapdb;

import java.util.ArrayList;

/**
 * @author Corwin
 *
 */
public class ColourOrder extends Order{
    
    private ArrayList<Plate> plates;
    
    

    /**
     * @param plates
     */
    public ColourOrder(ArrayList<Plate> plates) {
	super();
	this.plates = plates;
    }

   
    /**
     * @param orderType
     * @param orderName
     */
    public ColourOrder(String reference, String orderType, String orderName, ArrayList<Plate> plates, String shiftCode) {
	super(reference, orderType, orderName, shiftCode);
	this.plates = plates;
    }


    /**
     * @return the plates
     */
    public ArrayList<Plate> getPlates() {
        return plates;
    }


    /**
     * @param plates the plates to set
     */
    public void setPlates(ArrayList<Plate> plates) {
        this.plates = plates;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "ColourOrder [plates=" + plates + ", reference=" + reference
		+ ", orderType=" + orderType + ", orderName=" + orderName
		+ ", shiftCode=" + shiftCode + "]";
    }
    
}
