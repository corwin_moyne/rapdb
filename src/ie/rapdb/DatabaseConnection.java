/**
 * 
 */
package ie.rapdb;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JOptionPane;

/**
 * @author Corwin
 * 
 */
public class DatabaseConnection {

	private static Connection con = null;
	private static Statement st = null;
	private static ResultSet rs = null;
	private static SimpleDateFormat dateFormat;

	/**
	 * Connects to the database
	 * 
	 * @return
	 */
	public static Connection connect() {

		String message = "There seems to be a problem connecting to the server."
				+ "\nMake sure you are connected to the network."
				+ "\nIf the problem persists please contact Darren Kibbler";

		try {

			Class.forName("com.mysql.jdbc.Driver");

			con = DriverManager
					.getConnection(
							 //"jdbc:mysql://10.20.1.251:3306/rapdb?zeroDateTimeBehavior=convertToNull","rap",
							 //"");

							"jdbc:mysql://localhost:3306/rapdb?zeroDateTimeBehavior=convertToNull",
							"rap", "");

			st = con.createStatement();

		} catch (Exception e) {

			JOptionPane.showMessageDialog(null, message, "Error",
					JOptionPane.ERROR_MESSAGE);

			System.exit(0);
		}

		return con;
	}

	/**
	 * Closes the database connection
	 */
	public static void closeConnection() {
		try {
			if (rs != null) {
				rs.close();
			}
			if (st != null) {
				st.close();
			}
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	/**
	 * @return Returns a list of product codes
	 */
	public static ArrayList<String> getProductCodes() {
		ArrayList<String> arrayList = new ArrayList<String>();

		connect();

		try {
			String sql = "SELECT ProductCode FROM product_code";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				arrayList.add(rs.getString("ProductCode"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return arrayList;
	}

	/**
	 * @return Returns a list of job references
	 */
	public static ArrayList<String> getJobRefsPC(String pc) {

		ArrayList<String> arrayList = new ArrayList<String>();

		connect();

		try {
			String sql = "SELECT JobRef FROM job_reference WHERE ProductCode=? and is_obsolete=0";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, pc);
			rs = prepStmt.executeQuery();

			while (rs.next()) {
				arrayList.add(rs.getString("JobRef"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return arrayList;
	}

	/**
	 * @return Returns a list of internal die codes
	 */
	public static ArrayList<String> getIntCodes() {

		ArrayList<String> arrayList = new ArrayList<String>();

		arrayList.add("");

		connect();

		try {
			String sql = "SELECT InternalDieCode FROM die_code_int";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				arrayList.add(rs.getString("InternalDieCode"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return arrayList;
	}

	/**
	 * @return Returns a list of internal die codes
	 */
	public static ArrayList<String> getExtCodes() {

		ArrayList<String> arrayList = new ArrayList<String>();

		arrayList.add("");

		connect();

		try {
			String sql = "SELECT ExternalDieCode FROM die_code_ext";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				arrayList.add(rs.getString("ExternalDieCode"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return arrayList;
	}

	/**
	 * @return Returns a list of sizes
	 */
	public static ArrayList<String> getSizes() {

		ArrayList<String> arrayList = new ArrayList<String>();

		arrayList.add("");

		connect();

		try {
			String sql = "SELECT size FROM size_table ORDER BY size asc";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				arrayList.add(rs.getString("size"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return arrayList;
	}

	/**
	 * Returns a product based on product code
	 */
	public static ProductCode getProductDetails(String pc) {

		ProductCode productCode = new ProductCode();

		connect();

		try {
			String sql = "SELECT Size, IntDC, ExtDC FROM product_code WHERE ProductCode=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, pc);
			rs = prepStmt.executeQuery();

			if (rs.next()) {
				productCode = new ProductCode(rs.getString("Size"),
						rs.getString("IntDC"), rs.getString("ExtDC"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return productCode;
	}

	/**
	 * Returns all products
	 */
	public static ArrayList<ProductCode> getProducts() {

		ArrayList<ProductCode> productCodes = new ArrayList<ProductCode>();

		connect();

		try {
			String sql = "SELECT * FROM product_code";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				productCodes.add(new ProductCode(rs.getString("ProductCode"),
						rs.getString("Size"), rs.getString("IntDC"), rs
								.getString("ExtDC"),
						rs.getString("AntimistYR"), rs.getString("AdhesiveYR"),
						rs.getString("DatalaseYR"), rs.getString("OtherYR")));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return productCodes;
	}

	/**
	 * Returns dies based on an internal dc number
	 */
	public static ArrayList<Die> getInternalMOD(String dc) {

		ArrayList<Die> dies = new ArrayList<Die>();

		connect();

		try {
			String sql = "SELECT MOD_Number, Revolutions, DueDate FROM mod_int WHERE DC_Number=? "
					+ "AND Removed=0 AND DieLineChecked=1 AND DamageChecked=1 AND SampleChecked=1";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, dc);
			rs = prepStmt.executeQuery();

			while (rs.next()) {
				dies.add(new Die(rs.getString("MOD_Number"), rs
						.getInt("Revolutions"), rs.getDate("DueDate")));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return dies;
	}

	/**
	 * Returns dies based on an external dc number
	 */
	public static ArrayList<Die> getExternalMOD(String dc) {

		ArrayList<Die> dies = new ArrayList<Die>();

		connect();

		try {
			String sql = "SELECT MOD_Number, Revolutions, DueDate FROM mod_ext WHERE DC_Number=? AND Removed=0 AND DieLineChecked=1 AND DamageChecked=1 AND SampleChecked=1";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, dc);
			rs = prepStmt.executeQuery();

			while (rs.next()) {
				dies.add(new Die(rs.getString("MOD_Number"), rs
						.getInt("Revolutions"), rs.getDate("DueDate")));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return dies;
	}

	/**
	 * Returns removed internal dies
	 */
	public static ArrayList<Die> getRemovedInternalMOD(boolean removed) {

		ArrayList<Die> dies = new ArrayList<Die>();

		connect();

		try {
			String sql = "SELECT MOD_Number, DC_Number, Revolutions, RemovedDate, RemovedReason, Removed FROM mod_int WHERE Removed=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setBoolean(1, removed);
			rs = prepStmt.executeQuery();

			while (rs.next()) {
				dies.add(new Die(rs.getString("DC_Number"), rs
						.getString("MOD_Number"), rs.getInt("Revolutions"), rs
						.getDate("RemovedDate"), rs.getString("RemovedReason")));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return dies;
	}

	/**
	 * Returns removed external dies
	 */
	public static ArrayList<Die> getRemovedExternalMOD(boolean removed) {

		ArrayList<Die> dies = new ArrayList<Die>();

		connect();

		try {
			String sql = "SELECT MOD_Number, DC_Number, Revolutions, RemovedDate, RemovedReason, Removed FROM mod_ext WHERE Removed=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setBoolean(1, removed);
			rs = prepStmt.executeQuery();

			while (rs.next()) {
				dies.add(new Die(rs.getString("DC_Number"), rs
						.getString("MOD_Number"), rs.getInt("Revolutions"), rs
						.getDate("RemovedDate"), rs.getString("RemovedReason")));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return dies;
	}

	/**
	 * Returns removed die numbers
	 */
	public static ArrayList<String> getRemovedModNumbers(String dieType) {

		ArrayList<String> mods = new ArrayList<String>();

		try {

			if (dieType.equals("internal")) {

				connect();

				String sql = "SELECT MOD_Number  FROM mod_int WHERE Removed=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setBoolean(1, true);
				rs = prepStmt.executeQuery();

				while (rs.next()) {

					mods.add(rs.getString("MOD_Number"));
				}

				closeConnection();

			} else {

				connect();

				String sql = "SELECT MOD_Number  FROM mod_ext WHERE Removed=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setBoolean(1, true);
				rs = prepStmt.executeQuery();

				while (rs.next()) {

					mods.add(rs.getString("MOD_Number"));
				}

				closeConnection();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return mods;
	}

	/**
	 * Updates die revolutions
	 * 
	 * @param dieType
	 *            Internal/External
	 * @return
	 */
	public static void unremoveMod(String dieType, String mod) {

		try {

			if (dieType.equals("internal")) {

				connect();

				String sql = "UPDATE mod_int SET Removed=? WHERE MOD_Number=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setBoolean(1, false);
				prepStmt.setString(2, mod);
				prepStmt.executeUpdate();

				closeConnection();

			} else {

				connect();

				String sql = "UPDATE mod_ext SET Removed=? WHERE MOD_Number=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setBoolean(1, false);
				prepStmt.setString(2, mod);
				prepStmt.executeUpdate();

				closeConnection();
			}

		} catch (SQLException e) {
			// do nothing
		}
	}

	/**
	 * Returns int die performance based on mod number
	 */
	public static ArrayList<DiePerformance> getIntDiePerformance(String mod) {

		dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		ArrayList<DiePerformance> dies = new ArrayList<DiePerformance>();

		connect();

		try {
			String sql = "SELECT * FROM performance_int_die WHERE ModNumber=? order by id desc";

			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, mod);
			rs = prepStmt.executeQuery();

			while (rs.next()) {

				String runDate = dateFormat.format(rs.getDate("RunDate"));
				java.util.Date dateRunDate = dateFormat.parse(runDate);
				String formattedRunDate = dateFormat.format(dateRunDate);

				dies.add(new DiePerformance(formattedRunDate, rs
						.getString("ModNumber"), rs.getString("Comments"), rs
						.getInt("JobRevolutions"), rs.getString("UpdateName")));
			}

			closeConnection();

		} catch (SQLException e) {
			// Do nothing
		} catch (NullPointerException n) {
			// Do nothing
		} catch (ParseException e) {
			// Do nothing
		}
		return dies;
	}

	/**
	 * Returns ext die performance based on mod number
	 */
	public static ArrayList<DiePerformance> getExtDiePerformance(String mod) {

		dateFormat = new SimpleDateFormat(" dd-MMM-yyyy ");
		ArrayList<DiePerformance> dies = new ArrayList<DiePerformance>();

		connect();

		try {
			String sql = "SELECT * FROM performance_ext_die WHERE ModNumber=? order by id desc";

			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, mod);
			rs = prepStmt.executeQuery();

			while (rs.next()) {

				String runDate = dateFormat.format(rs.getDate("RunDate"));
				java.util.Date dateRunDate = dateFormat.parse(runDate);
				String formattedRunDate = dateFormat.format(dateRunDate);

				dies.add(new DiePerformance(formattedRunDate, rs
						.getString("ModNumber"), rs.getString("Comments"), rs
						.getInt("JobRevolutions"), rs.getString("UpdateName")));
			}

			closeConnection();

		} catch (SQLException e) {
			// Do nothing
		} catch (NullPointerException n) {
			// Do nothing
		} catch (ParseException e) {
			// Do nothing
		}
		return dies;
	}

	/**
	 * @return Returns a list of product codes
	 */
	public static String getIntRepeat(String dc) {

		String repeat = new String();

		connect();

		try {
			String sql = "SELECT DieRepeat FROM die_code_int WHERE InternalDieCode=?";

			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, dc);
			rs = prepStmt.executeQuery();

			if (rs.next()) {
				repeat = rs.getString("DieRepeat");
			}
			closeConnection();

		} catch (SQLException e) {
			// Do nothing

		} catch (NullPointerException n) {
			// Do nothing
		}
		return repeat;
	}

	/**
	 * @return Returns a list of product codes
	 */
	public static String getExtRepeat(String dc) {

		String repeat = new String();

		connect();

		try {

			String sql = "SELECT DieRepeat FROM die_code_ext WHERE ExternalDieCode=?";

			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, dc);
			rs = prepStmt.executeQuery();

			if (rs.next()) {
				repeat = rs.getString("DieRepeat");
			}
			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "No data");
		} catch (NullPointerException n) {
			// Do nothing
		}
		return repeat;
	}

	/**
	 * @return Returns a list of repeats
	 */
	public static ArrayList<String> getRepeatList() {

		ArrayList<String> arrayList = new ArrayList<String>();

		connect();

		try {
			String sql = "SELECT * FROM repeat_list order by die_repeat";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				arrayList.add(rs.getString("die_repeat"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return arrayList;
	}

	/**
	 * Inserts a new die code internal/external
	 */
	public static void insertDieCode(boolean internal, boolean external,
			String dc, String repeat) {

		try {

			if (internal) {

				connect();

				String sql = "INSERT INTO die_code_int (InternalDieCode, DieRepeat) VALUES(?, ?)";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, dc);
				prepStmt.setString(2, repeat);
				prepStmt.executeUpdate();

				closeConnection();
			}

			if (external) {

				connect();

				String sql = "INSERT INTO die_code_ext (ExternalDieCode, DieRepeat) VALUES(?, ?)";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, dc);
				prepStmt.setString(2, repeat);
				prepStmt.executeUpdate();

				closeConnection();
			}

			JOptionPane.showMessageDialog(null,
					"Die code(s) successfully inserted", "Success",
					JOptionPane.INFORMATION_MESSAGE);

		} catch (SQLException e) {

			if (e.getSQLState().startsWith("23")) {
				JOptionPane.showMessageDialog(null, "DC already exists");
			}
		}
	}

	/**
	 * Inserts a new mod internal/external
	 */
	public static void insertMOD(boolean internal, boolean external, String dc,
			java.util.Date dueDate, double dieCost, String reason, int qty) {

		try {

			java.sql.Date sqlDueDate = new java.sql.Date(dueDate.getTime());

			if (internal) {

				connect();

				for (int i = 0; i < qty; i++) {
					String sql = "INSERT INTO mod_int (DC_Number, DueDate, DieCost, reorder_reason) VALUES(?, ?, ?, ?)";
					PreparedStatement prepStmt = con.prepareStatement(sql);
					prepStmt.setString(1, dc);
					prepStmt.setDate(2, sqlDueDate);
					prepStmt.setDouble(3, dieCost);
					prepStmt.setString(4, reason);
					prepStmt.executeUpdate();
				}

				closeConnection();
			}

			if (external) {

				connect();

				for (int i = 0; i < qty; i++) {
					String sql = "INSERT INTO mod_ext (DC_Number, DueDate, DieCost, reorder_reason) VALUES(?, ?, ?, ?)";
					PreparedStatement prepStmt = con.prepareStatement(sql);
					prepStmt.setString(1, dc);
					prepStmt.setDate(2, sqlDueDate);
					prepStmt.setDouble(3, dieCost);
					prepStmt.setString(4, reason);
					prepStmt.executeUpdate();
				}

				closeConnection();
			}

			JOptionPane.showMessageDialog(null, "Die(s) successfully inserted",
					"Success", JOptionPane.INFORMATION_MESSAGE);

		} catch (SQLException e) {
			if (e.getSQLState().startsWith("23")) {
				JOptionPane.showMessageDialog(null, "MOD already exists");
			} else {
				e.printStackTrace();
			}
		} catch (NullPointerException n) {
			// Do nothing
		}
	}

	public static void insertManualMOD(boolean internal, boolean external,
			String dc, String mod, String name, java.util.Date dueDate) {

		try {

			java.sql.Date sqlDueDate = new java.sql.Date(dueDate.getTime());

			if (internal) {

				connect();

				String sql = "INSERT INTO mod_int (DC_Number, MOD_Number, DieLineChecked, DamageChecked, SampleChecked, "
						+ "CheckInName, CheckInDate, DueDate) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, dc);
				prepStmt.setString(2, mod);
				prepStmt.setBoolean(3, true);
				prepStmt.setBoolean(4, true);
				prepStmt.setBoolean(5, true);
				prepStmt.setString(6, name);
				prepStmt.setDate(7, sqlDueDate);
				prepStmt.setDate(8, sqlDueDate);
				prepStmt.executeUpdate();

				closeConnection();
			}

			if (external) {

				connect();

				String sql = "INSERT INTO mod_ext (DC_Number, MOD_Number, DieLineChecked, DamageChecked, SampleChecked, "
						+ "CheckInName, CheckInDate, DueDate) VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, dc);
				prepStmt.setString(2, mod);
				prepStmt.setBoolean(3, true);
				prepStmt.setBoolean(4, true);
				prepStmt.setBoolean(5, true);
				prepStmt.setString(6, name);
				prepStmt.setDate(7, sqlDueDate);
				prepStmt.setDate(8, sqlDueDate);
				prepStmt.executeUpdate();

				closeConnection();
			}

			JOptionPane.showMessageDialog(null, "Die(s) successfully inserted",
					"Success", JOptionPane.INFORMATION_MESSAGE);

		} catch (SQLException e) {
			if (e.getSQLState().startsWith("23")) {
				JOptionPane.showMessageDialog(null, "MOD already exists");
			} else {
				e.printStackTrace();
			}
		} catch (NullPointerException n) {
			// Do nothing
		}
	}

	/**
	 * Returns internal mod numbers
	 * 
	 * @return Returns internal mod numbers
	 */
	public static ArrayList<String> getIntMODs() {

		ArrayList<String> mods = new ArrayList<String>();

		connect();

		try {
			String sql = "SELECT Mod_Number FROM mod_int WHERE removed=0 AND DieLineChecked=1 AND DamageChecked=1 "
					+ "AND SampleChecked=1 order by Mod_Number";

			rs = st.executeQuery(sql);

			while (rs.next()) {

				mods.add(rs.getString("Mod_Number"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return mods;
	}

	/**
	 * Returns external mod numbers
	 * 
	 * @return Returns external mod numbers
	 */
	public static ArrayList<String> getExtMODs() {

		ArrayList<String> mods = new ArrayList<String>();

		connect();

		try {
			String sql = "SELECT Mod_Number FROM mod_ext WHERE removed=0 AND DieLineChecked=1 AND DamageChecked=1 "
					+ "AND SampleChecked=1 order by Mod_Number";

			rs = st.executeQuery(sql);

			while (rs.next()) {

				mods.add(rs.getString("Mod_Number"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return mods;
	}

	/**
	 * Returns the die code and revolutions associated with the mod
	 * 
	 * @param mod
	 *            The mod to use in query
	 * @return
	 */
	public static Die getIntDcRevs(String mod) {

		Die die = new Die();

		connect();

		try {
			String sql = "SELECT id, DC_Number, Revolutions, 1st_email_sent, 2nd_email_sent FROM mod_int WHERE MOD_Number=? AND Removed=0";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, mod);
			rs = prepStmt.executeQuery();

			if (rs.next()) {

				die = new Die(rs.getInt("id"), rs.getString("DC_Number"),
						rs.getInt("Revolutions"),
						rs.getBoolean("1st_email_sent"),
						rs.getBoolean("2nd_email_sent"));
			}

			closeConnection();

		} catch (SQLException e) {
			// do nothing
			e.printStackTrace();

		} catch (NullPointerException n) {
			// Do nothing
		}

		return die;
	}

	/**
	 * Returns the die code and revolutions associated with the mod
	 * 
	 * @param mod
	 *            The mod to use in query
	 * @return
	 */
	public static Die getExtDcRevs(String mod) {

		Die die = new Die();

		connect();

		try {
			String sql = "SELECT id, DC_Number, Revolutions, 1st_email_sent, 2nd_email_sent FROM mod_ext WHERE MOD_Number=? "
					+ "AND Removed=0";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, mod);
			rs = prepStmt.executeQuery();

			if (rs.next()) {

				die = new Die(rs.getInt("id"), rs.getString("DC_Number"),
						rs.getInt("Revolutions"),
						rs.getBoolean("1st_email_sent"),
						rs.getBoolean("2nd_email_sent"));
			}

			closeConnection();

		} catch (SQLException e) {
			// do nothing
			e.printStackTrace();

		} catch (NullPointerException n) {
			// Do nothing
		}

		return die;
	}

	/**
	 * Inserts a new die code internal/external
	 */
	public static void insertDiePerformance(String dieType,
			java.util.Date runDate, String comments, String performance,
			String mod, int jobRevs, String updateName, String press) {

		try {

			java.sql.Date sqlRunDate = new java.sql.Date(runDate.getTime());

			if (dieType.equals("internal")) {

				connect();

				String sql = "INSERT INTO performance_int_die (RunDate, Comments, Performance, ModNumber, JobRevolutions, UpdateName, Press)"
						+ " VALUES(?, ?, ?, ?, ?, ?, ?)";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setDate(1, sqlRunDate);
				prepStmt.setString(2, comments);
				prepStmt.setString(3, performance);
				prepStmt.setString(4, mod);
				prepStmt.setInt(5, jobRevs);
				prepStmt.setString(6, updateName);
				prepStmt.setString(7, press);
				prepStmt.executeUpdate();

				closeConnection();
			}

			else {

				connect();

				String sql = "INSERT INTO performance_ext_die (RunDate, Comments, Performance, ModNumber, JobRevolutions, UpdateName, Press)"
						+ " VALUES(?, ?, ?, ?, ?, ?, ?)";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setDate(1, sqlRunDate);
				prepStmt.setString(2, comments);
				prepStmt.setString(3, performance);
				prepStmt.setString(4, mod);
				prepStmt.setInt(5, jobRevs);
				prepStmt.setString(6, updateName);
				prepStmt.setString(7, press);
				prepStmt.executeUpdate();

				closeConnection();
			}

			JOptionPane.showMessageDialog(null, "Die successfully updated",
					"Success", JOptionPane.INFORMATION_MESSAGE);

		} catch (Exception e) {
			// Do nothing
		}
	}

	/**
	 * Updates die revolutions
	 * 
	 * @param dieType
	 *            Internal/External
	 * @return
	 */
	public static void updateTotalRevolutions(String dieType, int jobRevs,
			String mod) {

		try {

			if (dieType.equals("internal")) {

				connect();

				String sql = "UPDATE mod_int SET Revolutions=Revolutions+? WHERE MOD_Number=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setInt(1, jobRevs);
				prepStmt.setString(2, mod);
				prepStmt.executeUpdate();

				closeConnection();

			} else {

				connect();

				String sql = "UPDATE mod_ext SET Revolutions=Revolutions+? WHERE MOD_Number=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setInt(1, jobRevs);
				prepStmt.setString(2, mod);
				prepStmt.executeUpdate();

				closeConnection();
			}

		} catch (SQLException e) {
			// do nothing
		}
	}

	public static void updateTotalRevolutionsManually(String dieType,
			int jobRevs, int id) {

		try {

			if (dieType.equals("internal")) {

				connect();

				String sql = "UPDATE mod_int SET Revolutions=? WHERE id=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setInt(1, jobRevs);
				prepStmt.setInt(2, id);
				prepStmt.executeUpdate();

				closeConnection();

			} else {

				connect();

				String sql = "UPDATE mod_ext SET Revolutions=? WHERE id=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setInt(1, jobRevs);
				prepStmt.setInt(2, id);
				prepStmt.executeUpdate();

				closeConnection();
			}

		} catch (SQLException e) {
			// do nothing
		}
	}

	/**
	 * Updates die mod and due date
	 * 
	 * @param dieType
	 *            Internal/External
	 * @return
	 */
	public static void updateModAndDate(int id, String dieType, String mod,
			java.util.Date dueDate, String dieCode) {

		try {

			java.sql.Date sqlDueDate = new java.sql.Date(dueDate.getTime());

			if (dieType.equals("internal")) {

				connect();

				String sql = "UPDATE mod_int SET MOD_Number=?, DueDate=? WHERE id=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, mod);
				prepStmt.setDate(2, sqlDueDate);
				prepStmt.setInt(3, id);
				prepStmt.executeUpdate();

				closeConnection();

			} else {

				connect();

				String sql = "UPDATE mod_ext SET MOD_Number=?, DueDate=? WHERE id=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, mod);
				prepStmt.setDate(2, sqlDueDate);
				prepStmt.setInt(3, id);
				prepStmt.executeUpdate();

				closeConnection();
			}

		} catch (SQLException e) {
			// do nothing

		}
	}

	/**
	 * Marks die as removed
	 * 
	 * @param dieType
	 *            Internal/External
	 * @return
	 */
	public static void removeDie(String dieType, String mod, String reason,
			java.util.Date removedDate) {

		java.sql.Date sqlRemovedDate = new java.sql.Date(removedDate.getTime());

		try {

			if (dieType.equals("internal")) {

				connect();

				String sql = "UPDATE mod_int SET Removed=1, RemovedReason=?, RemovedDate=? WHERE MOD_Number=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, reason);
				prepStmt.setDate(2, sqlRemovedDate);
				prepStmt.setString(3, mod);
				prepStmt.executeUpdate();

				closeConnection();

			} else {

				connect();

				String sql = "UPDATE mod_ext SET Removed=1, RemovedReason=?, RemovedDate=? WHERE MOD_Number=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, reason);
				prepStmt.setDate(2, sqlRemovedDate);
				prepStmt.setString(3, mod);
				prepStmt.executeUpdate();

				closeConnection();
			}

		} catch (SQLException e) {
			// do nothing
		}
	}

	/**
	 * Returns internal/external mod numbers that are unchecked
	 * 
	 * @return Returns internal/external mod numbers
	 */
	public static ArrayList<Die> getUncheckedMODs(String dieType) {

		ArrayList<Die> dieCodes = new ArrayList<Die>();

		try {

			if (dieType.equals("internal")) {

				connect();

				String sql = "SELECT id, DC_Number, DueDate FROM mod_int WHERE removed=? AND DamageChecked=? order by DC_Number";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setBoolean(1, false);
				prepStmt.setBoolean(2, false);
				rs = prepStmt.executeQuery();

				while (rs.next()) {

					dieCodes.add(new Die(rs.getInt("id"), rs
							.getString("DC_Number"), rs.getDate("DueDate")));
				}

				closeConnection();

			} else {

				connect();

				String sql = "SELECT id, DC_Number, DueDate FROM mod_ext WHERE removed=? AND DamageChecked=? order by DC_Number";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setBoolean(1, false);
				prepStmt.setBoolean(2, false);
				rs = prepStmt.executeQuery();

				while (rs.next()) {

					dieCodes.add(new Die(rs.getInt("id"), rs
							.getString("DC_Number"), rs.getDate("DueDate")));
				}

				closeConnection();

			}

		} catch (SQLException e) {

			e.printStackTrace();
		}
		return dieCodes;
	}

	/**
	 * updates die data, mod, checkin date, checkin name
	 * 
	 * @param dieType
	 *            Internal/External
	 * @return
	 */
	public static void updateDieCheckData(int id, String dieType, String mod,
			String checkinName) {

		try {

			if (dieType.equals("internal")) {

				connect();

				String sql = "UPDATE mod_int SET MOD_Number=?, DieLineChecked=1, DamageChecked=1, SampleChecked=1, "
						+ "CheckInDate=curdate(), CheckInName=? WHERE id=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, mod);
				prepStmt.setString(2, checkinName);
				prepStmt.setInt(3, id);
				prepStmt.executeUpdate();

				closeConnection();

			} else {

				connect();

				String sql = "UPDATE mod_ext SET MOD_Number=?, DieLineChecked=1, DamageChecked=1, SampleChecked=1, "
						+ "CheckInDate=curdate(), CheckInName=? WHERE id=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, mod);
				prepStmt.setString(2, checkinName);
				prepStmt.setInt(3, id);
				prepStmt.executeUpdate();

				closeConnection();
			}

			JOptionPane.showMessageDialog(null, "Die successfully updated",
					"Success", JOptionPane.INFORMATION_MESSAGE);

		} catch (SQLException e) {
			// do nothing
			e.printStackTrace();
		}
	}

	/**
	 * Returns dies on order
	 */
	public static ArrayList<Die> getDueDies(String dieType) {

		ArrayList<Die> dies = new ArrayList<Die>();

		try {

			if (dieType.equals("internal")) {

				connect();

				String sql = "SELECT id, MOD_Number, DC_Number, DueDate, Removed FROM mod_int "
						+ "WHERE Removed=? AND DieLineChecked=0 AND DamageChecked=0 AND SampleChecked=0";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setBoolean(1, false);
				rs = prepStmt.executeQuery();

				while (rs.next()) {

					dies.add(new Die(rs.getInt("id"),
							rs.getString("DC_Number"), rs
									.getString("MOD_Number"), rs
									.getDate("DueDate")));
				}

				closeConnection();

			} else {

				connect();

				String sql = "SELECT id, MOD_Number, DC_Number, DueDate, Removed FROM mod_ext "
						+ "WHERE Removed=? AND DieLineChecked=0 AND DamageChecked=0 AND SampleChecked=0";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setBoolean(1, false);
				rs = prepStmt.executeQuery();

				while (rs.next()) {

					dies.add(new Die(rs.getInt("id"),
							rs.getString("DC_Number"), rs
									.getString("MOD_Number"), rs
									.getDate("DueDate")));
				}

				closeConnection();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dies;
	}

	/**
	 * Returns plates with jobRef
	 */
	public static ArrayList<Plate> getPlates(String jobRef) {

		ArrayList<Plate> plates = new ArrayList<Plate>();

		try {

			connect();

			String sql = "SELECT ID, InkRef, YRNumber FROM plate WHERE JobRef=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, jobRef);
			rs = prepStmt.executeQuery();

			while (rs.next()) {

				plates.add(new Plate(rs.getInt("ID"), rs.getString("InkRef"),
						rs.getString("YRNumber")));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return plates;
	}

	/**
	 * Gets current list of reorder reasons
	 * 
	 * @return
	 */
	public static ArrayList<String> getReasons() {

		ArrayList<String> reasons = new ArrayList<String>();

		try {

			connect();

			String sql = "select Reason from reorder_reason";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				reasons.add(rs.getString("Reason"));
			}

			closeConnection();

		} catch (Exception x) {
			System.out.println(x.getMessage());
		}
		return reasons;
	}

	/**
	 * Returns a list of die reorder reasons
	 * 
	 * @return
	 */
	public static ArrayList<String> getReasonsDies() {

		ArrayList<String> reasons = new ArrayList<String>();
		reasons.add("");

		try {

			connect();

			String sql = "SELECT * FROM reorder_reason_die ORDER BY id";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				reasons.add(rs.getString("reason"));
			}

			closeConnection();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return reasons;
	}

	/**
	 * Gets current list of delivery timelines
	 * 
	 * @return
	 */
	public static ArrayList<String> getTimelines() {

		ArrayList<String> timelines = new ArrayList<String>();

		try {

			connect();

			String sql = "select timeline from delivery_timeline";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				timelines.add(rs.getString("timeline"));
			}

			closeConnection();

		} catch (Exception e) {
			// Do nothing
		}
		return timelines;
	}

	/**
	 * Gets current list of Shift Descriptions
	 * 
	 * @return
	 */
	public static ArrayList<String> getShiftDescription() {

		ArrayList<String> shifts = new ArrayList<String>();

		try {

			connect();

			String sql = "select ShiftDescription from shift_code order by ShiftDescription";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				shifts.add(rs.getString("ShiftDescription"));
			}

			closeConnection();

		} catch (Exception e) {
			// Do nothing
		}
		return shifts;
	}

	/**
	 * Adds newly colour ordered plates to the reordered colour plates table
	 * 
	 * @param colourOrder
	 */
	public static void updateReorderedColour(
			ArrayList<UnconfirmedPlates> unconfirmedPlates) {

		try {
			connect();

			for (UnconfirmedPlates plate : unconfirmedPlates) {

				if (plate.isConfirmed()) {

					String sql = "INSERT INTO reordered_plate_colour (JobRef, Colour, ReasonForOrder, YrNumber, "
							+ "OrderName, OrderQty, ShiftCode) VALUES(?,?,?,?,?,?,?)";
					PreparedStatement prepStmt = con.prepareStatement(sql);
					prepStmt.setString(1, plate.getReference());
					prepStmt.setString(2, plate.getColour());
					prepStmt.setString(3, plate.getReason());
					prepStmt.setString(4, plate.getYr());
					prepStmt.setString(5, plate.getName());
					prepStmt.setInt(6, plate.getQty());
					prepStmt.setString(7, plate.getShift());
					prepStmt.executeUpdate();
				}
			}

			closeConnection();

		} catch (SQLException e) {
			// do nothing
			e.printStackTrace();

		} catch (NullPointerException n) {
			// Do nothing
		}
	}

	/**
	 * @return Returns the varnish yr associated with the job ref
	 */
	public static String getVarnishYr(String jobRef) {

		String varnishYr = new String();

		connect();

		try {
			String sql = "SELECT VarnishYR FROM job_reference WHERE VarnishYR IS NOT NULL AND JobRef=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, jobRef);
			rs = prepStmt.executeQuery();

			if (rs.next()) {
				varnishYr = rs.getString("VarnishYR");
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return varnishYr;
	}

	/**
	 * @return Returns the colour yr associated with the job ref
	 */
	public static String getColourYr(String jobRef) {

		String yr = new String();

		connect();

		try {
			String sql = "SELECT YrNumber FROM reordered_plate_colour WHERE JobRef=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, jobRef);
			rs = prepStmt.executeQuery();

			if (rs.next()) {
				yr = rs.getString("YrNumber");
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return yr;
	}

	/**
	 * @return Returns all colour yrs
	 */
	public static ArrayList<String> getAllColourYr() {

		ArrayList<String> yr = new ArrayList<String>();
		yr.add("All");

		connect();

		try {
			String sql = "SELECT DISTINCT YrNumber FROM reordered_plate_colour WHERE YrNumber IS NOT NULL ORDER BY YrNumber";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				yr.add(rs.getString("YrNumber"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return yr;
	}

	/**
	 * @return Returns the colours associated with the job ref
	 */
	public static ArrayList<String> getColours(String jobRef) {

		ArrayList<String> colours = new ArrayList<String>();
		colours.add("All");

		connect();

		try {
			String sql = "SELECT DISTINCT Colour FROM reordered_plate_colour WHERE JobRef=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, jobRef);
			rs = prepStmt.executeQuery();

			while (rs.next()) {
				colours.add(rs.getString("Colour"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return colours;
	}

	/**
	 * @return Returns the colours associated with the job ref
	 */
	public static ArrayList<String> getAllColours() {

		ArrayList<String> colours = new ArrayList<String>();
		colours.add("All");

		connect();

		try {
			String sql = "SELECT DISTINCT Colour FROM reordered_plate_colour ORDER BY Colour";
			rs = st.executeQuery(sql);

			while (rs.next()) {
				colours.add(rs.getString("Colour"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return colours;
	}

	/**
	 * @return Returns the varnish yr associated with the job ref
	 */
	public static ArrayList<String> getVarnishYrFromReorder(String productCode) {

		ArrayList<String> varnishYrs = new ArrayList<String>();

		connect();

		try {
			String sql = "SELECT YrNumber FROM reordered_plate_varnish WHERE YrNumber IS NOT NULL AND ProductCode=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, productCode);
			rs = prepStmt.executeQuery();

			while (rs.next()) {
				varnishYrs.add(rs.getString("YrNumber"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return varnishYrs;
	}

	/**
	 * @return Returns product codes with a datalase yr
	 */
	public static ArrayList<String> getDatalaseProductCodes() {

		ArrayList<String> productCode = new ArrayList<String>();

		connect();

		try {
			String sql = "SELECT ProductCode FROM product_code WHERE DatalaseYR IS NOT NULL";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				productCode.add(rs.getString("ProductCode"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return productCode;
	}

	/**
	 * @return Returns the common yr associated with the product code
	 */
	public static String getCommonYr(String plateType, String productCode) {

		String yr = new String();

		try {

			if (plateType.equals("antimist")) {

				connect();

				String sql = "SELECT AntimistYR FROM product_code WHERE ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				if (rs.next()) {
					yr = rs.getString("AntimistYR");
				}

				closeConnection();
			}

			if (plateType.equals("datalase")) {

				connect();

				String sql = "SELECT DatalaseYR FROM product_code WHERE ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				if (rs.next()) {
					yr = rs.getString("DatalaseYR");
				}

				closeConnection();
			}

			if (plateType.equals("adhesive")) {

				connect();

				String sql = "SELECT AdhesiveYR FROM product_code WHERE ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				if (rs.next()) {
					yr = rs.getString("AdhesiveYR");
				}

				closeConnection();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return yr;
	}

	/**
	 * Adds newly ordered common ordered plates to the reordered common plate
	 * tables
	 * 
	 * @param colourOrder
	 */
	public static void updateReorderedCommon(String plateType,
			UnconfirmedPlates unconfirmedPlate) {

		try {

			if (plateType.equals("varnish")) {
				connect();

				String sql = "INSERT INTO reordered_plate_varnish (ProductCode, ReasonForOrder, YrNumber, "
						+ "OrderName, OrderQty, ShiftCode) VALUES(?,?,?,?,?,?)";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, unconfirmedPlate.getReference());
				prepStmt.setString(2, unconfirmedPlate.getReason());
				prepStmt.setString(3, unconfirmedPlate.getYr());
				prepStmt.setString(4, unconfirmedPlate.getName());
				prepStmt.setInt(5, unconfirmedPlate.getQty());
				prepStmt.setString(6, unconfirmedPlate.getShift());
				prepStmt.executeUpdate();

				closeConnection();
			}

			if (plateType.equals("antimist")) {
				connect();

				String sql = "INSERT INTO reordered_plate_antimist (ProductCode, ReasonForOrder, YrNumber, "
						+ "OrderName, OrderQty, ShiftCode) VALUES(?,?,?,?,?,?)";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, unconfirmedPlate.getReference());
				prepStmt.setString(2, unconfirmedPlate.getReason());
				prepStmt.setString(3, unconfirmedPlate.getYr());
				prepStmt.setString(4, unconfirmedPlate.getName());
				prepStmt.setInt(5, unconfirmedPlate.getQty());
				prepStmt.setString(6, unconfirmedPlate.getShift());
				prepStmt.executeUpdate();

				closeConnection();
			}

			if (plateType.equals("datalase")) {
				connect();

				String sql = "INSERT INTO reordered_plate_datalase (ProductCode, ReasonForOrder, YrNumber, "
						+ "OrderName, OrderQty, ShiftCode) VALUES(?,?,?,?,?,?)";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, unconfirmedPlate.getReference());
				prepStmt.setString(2, unconfirmedPlate.getReason());
				prepStmt.setString(3, unconfirmedPlate.getYr());
				prepStmt.setString(4, unconfirmedPlate.getName());
				prepStmt.setInt(5, unconfirmedPlate.getQty());
				prepStmt.setString(6, unconfirmedPlate.getShift());
				prepStmt.executeUpdate();

				closeConnection();
			}

			if (plateType.equals("adhesive")) {
				connect();

				String sql = "INSERT INTO reordered_plate_adhesive (ProductCode, ReasonForOrder, YrNumber, "
						+ "OrderName, OrderQty, ShiftCode) VALUES(?,?,?,?,?,?)";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, unconfirmedPlate.getReference());
				prepStmt.setString(2, unconfirmedPlate.getReason());
				prepStmt.setString(3, unconfirmedPlate.getYr());
				prepStmt.setString(4, unconfirmedPlate.getName());
				prepStmt.setInt(5, unconfirmedPlate.getQty());
				prepStmt.setString(6, unconfirmedPlate.getShift());
				prepStmt.executeUpdate();

				closeConnection();
			}

		} catch (SQLException e) {
			// do nothing
			e.printStackTrace();

		} catch (NullPointerException n) {
			// Do nothing
		}
	}

	/**
	 * Validates a user login attempt
	 * 
	 * @param username
	 *            The username entered by user
	 * @param password
	 *            The password enetered by user
	 * @return Returns true if user exists
	 */
	public static String validateUser(String username, String password) {

		try {

			connect();

			String sql = "SELECT * FROM user WHERE username=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, username);
			rs = prepStmt.executeQuery();

			if (rs.next()) {

				if (rs.getString("username").equals(username)) {
					if (rs.getString("password").equals(password)) {
						return rs.getString("user_type");
					}
				}
			}

			closeConnection();

		} catch (SQLException e) {

			// Do nothing
		}
		return "Error";
	}

	/**
	 * Add new user (Employee) to the database
	 * 
	 * @param username
	 * @param password
	 * @param type
	 * @throws SQLException
	 */
	public static void addUser(String username, String password, String type) {

		String sql = "INSERT INTO user (username, password, user_type) VALUES (?,?,?)";

		try {
			connect();

			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, username);
			prepStmt.setString(2, password);
			prepStmt.setString(3, type);
			prepStmt.executeUpdate();

			closeConnection();

			JOptionPane.showMessageDialog(null, "User added", "Success",
					JOptionPane.INFORMATION_MESSAGE);

		} catch (SQLException e) {

			JOptionPane.showMessageDialog(null, "User cannot be added",
					"Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Get users from the database
	 * 
	 * @return users
	 */
	public static ArrayList<User> getUsers() {

		ArrayList<User> users = new ArrayList<User>();

		try {
			connect();

			String sql = "Select * from user";
			rs = st.executeQuery(sql);

			while (rs.next()) {

				users.add(new User(rs.getString("username"), rs
						.getString("password"), rs.getString("user_type")));
			}

			closeConnection();

		} catch (SQLException e) {
			// Do nothing
		}
		return users;
	}

	/**
	 * Update selected User
	 * 
	 * @param user
	 *            for updating user
	 */
	public static void updateUser(User user) {
		try {
			connect();

			PreparedStatement statement = (PreparedStatement) con
					.prepareStatement("update user set password = ?, userType = ? where userName = ?");
			statement.setString(1, user.getPassword());
			statement.setString(2, user.getUserType());
			statement.setString(3, user.getUserName());
			statement.executeUpdate();

			closeConnection();

			JOptionPane.showMessageDialog(null,
					"User details updated successfully!", "Success",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (SQLException e) {
			// Do nothing
		}
	}

	/**
	 * Delete selected user (Employee) from the database
	 * 
	 * @param userName
	 */
	public static void deleteUser(String userName) {

		try {

			connect();

			String sql = "Delete from user where username = ?";
			PreparedStatement prepStmt = (PreparedStatement) con
					.prepareStatement(sql);
			prepStmt.setString(1, userName);
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			// Do nothing
		}
	}

	/**
	 * Get userType from the database
	 * 
	 * @return userTypes
	 */
	public static ArrayList<String> getUserTypes() {

		ArrayList<String> userTypes = new ArrayList<String>();

		try {
			connect();

			String sql = "Select * from user_type_table";
			rs = st.executeQuery(sql);

			while (rs.next()) {
				String userType = rs.getString("user_type");

				userTypes.add(userType);
			}

			closeConnection();

		} catch (SQLException e) {
			// Do nothing
		}
		return userTypes;
	}

	/**
	 * Updates product codes
	 * 
	 */
	public static void updateProductCode(ProductCode productCode) {

		try {

			connect();

			String sql = "UPDATE product_code SET ProductCode=?, Size=?, IntDC=?, ExtDC=?, "
					+ "AntimistYR=?, AdhesiveYR=?, DatalaseYR=?, OtherYR=? WHERE ProductCode=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, productCode.getProductCode());
			prepStmt.setString(2, productCode.getSize());
			prepStmt.setString(3, productCode.getIntDC());
			prepStmt.setString(4, productCode.getExtDC());
			prepStmt.setString(5, productCode.getAntimistYr());
			prepStmt.setString(6, productCode.getAdhesiveYr());
			prepStmt.setString(7, productCode.getDatalaseYr());
			prepStmt.setString(8, productCode.getOtherYr());
			prepStmt.setString(9, productCode.getProductCode());
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			e.printStackTrace();

		} catch (NullPointerException n) {
			// Do nothing
		}
	}

	/**
	 * Inserts a new product code
	 */
	public static void insertProductCode(String productCode) {

		try {

			connect();

			String sql = "INSERT INTO product_code (ProductCode) VALUES(?)";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, productCode);
			prepStmt.executeUpdate();

			closeConnection();

			JOptionPane.showMessageDialog(null,
					"Product code successfully inserted", "Success",
					JOptionPane.INFORMATION_MESSAGE);

		} catch (SQLException e) {
			// Do nothing
		}
	}

	/**
	 * @return Returns job references
	 */
	public static ArrayList<JobReference> getJobReferences(String productCode) {

		ArrayList<JobReference> jobReferences = new ArrayList<JobReference>();

		try {

			connect();

			String sql = "SELECT JobRef, Customer, VarnishYR, is_obsolete FROM job_reference WHERE ProductCode=? order by is_obsolete, JobRef";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, productCode);
			rs = prepStmt.executeQuery();

			while (rs.next()) {

				jobReferences.add(new JobReference(rs.getString("JobRef"), rs
						.getString("Customer"), rs.getString("VarnishYR"), rs
						.getBoolean("is_obsolete")));
			}

			closeConnection();

		} catch (SQLException e) {

			// Do nothing
		}
		return jobReferences;
	}

	public static ArrayList<StockCheck> getCurrentJobsAndColours() {

		ArrayList<StockCheck> stock = new ArrayList<StockCheck>();

		try {

			connect();

			String sql = "SELECT job_reference.JobRef, plate.InkRef FROM job_reference, plate WHERE is_obsolete = false and job_reference.JobRef = plate.JobRef order by job_reference.JobRef";
			rs = st.executeQuery(sql);

			while (rs.next()) {

				stock.add(new StockCheck(rs.getString("JobRef"), rs
						.getString("InkRef")));
			}

			closeConnection();

		} catch (SQLException e) {

			// Do nothing
		}
		return stock;
	}

	/**
	 * Updates job_reference
	 * 
	 */
	public static void updateJobReference(JobReference jobReference) {

		try {

			connect();

			String sql = "UPDATE job_reference SET Customer=?, VarnishYR=?, is_obsolete=? WHERE JobRef=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, jobReference.getCustomer());
			prepStmt.setString(2, jobReference.getVarnishYr());
			prepStmt.setBoolean(3, jobReference.isObsolete());
			prepStmt.setString(4, jobReference.getJobReference());
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			// do nothing
		}
	}

	/**
	 * Add job_reference
	 * 
	 */
	public static void addJobReference(JobReference jobReference,
			String productCode) {

		try {

			connect();

			String sql = "INSERT INTO job_reference (JobRef, Customer, ProductCode, VarnishYR) VALUES (?,?,?,?)";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, jobReference.getJobReference());
			prepStmt.setString(2, jobReference.getCustomer());
			prepStmt.setString(3, productCode);
			prepStmt.setString(4, jobReference.getVarnishYr());

			prepStmt.executeUpdate();

			closeConnection();

			JOptionPane.showMessageDialog(null, "Job Reference added",
					"Success", JOptionPane.INFORMATION_MESSAGE);

		} catch (SQLException e) {
			// do nothing
		}
	}

	/**
	 * Get customers from the database
	 * 
	 * @return customers
	 */
	public static ArrayList<String> getCustomers() {

		ArrayList<String> customers = new ArrayList<String>();

		customers.add("");

		try {
			connect();

			String sql = "Select customer_name from customer ORDER BY customer_name asc";
			rs = st.executeQuery(sql);

			while (rs.next()) {

				customers.add(rs.getString("customer_name"));
			}

			closeConnection();

		} catch (SQLException e) {
			// Do nothing
		}
		return customers;
	}

	/**
	 * Inserts a new customer
	 */
	public static void insertCustomer(String customerName) {

		try {

			connect();

			String sql = "INSERT INTO customer (customer_name) VALUES(?)";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, customerName);
			prepStmt.executeUpdate();

			closeConnection();

			JOptionPane.showMessageDialog(null,
					"New customer successfully added", "Success",
					JOptionPane.INFORMATION_MESSAGE);

		} catch (SQLException e) {
			// Do nothing
		}
	}

	/**
	 * Get customers from the database
	 * 
	 * @return plateDetails
	 */
	public static ArrayList<PlateDetail> getPlateDetail(String jobRef) {

		ArrayList<PlateDetail> plateDetails = new ArrayList<PlateDetail>();

		try {
			connect();

			String sql = "Select * from plate WHERE JobRef=? ORDER BY JobRef asc";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, jobRef);
			rs = prepStmt.executeQuery();

			while (rs.next()) {

				plateDetails.add(new PlateDetail(rs.getInt("ID"), rs
						.getString("JobRef"), rs.getString("InkRef"), rs
						.getString("YRNumber"), rs.getInt("stock")));
			}

			closeConnection();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return plateDetails;
	}

	/**
	 * Add plates
	 * 
	 */
	public static int[] addPlate(ArrayList<PlateDetail> plateDetails,
			String jobRef) {

		PreparedStatement prepStmt = null;
		int[] ids = new int[plateDetails.size()];
		int i = 0;

		try {

			connect();

			for (PlateDetail plate : plateDetails) {
				String sql = "INSERT INTO plate (JobRef, InkRef, YRNumber) VALUES (?,?,?)";
				prepStmt = con.prepareStatement(sql,
						Statement.RETURN_GENERATED_KEYS);
				prepStmt.setString(1, jobRef);
				prepStmt.setString(2, plate.getInkRefString());
				prepStmt.setString(3, plate.getYrNumber());
				prepStmt.executeUpdate();

				rs = prepStmt.getGeneratedKeys();
				while (rs.next()) {
					ids[i++] = rs.getInt(1);
				}
			}

			closeConnection();

			JOptionPane.showMessageDialog(null, "Plates added added",
					"Success", JOptionPane.INFORMATION_MESSAGE);

		} catch (SQLException e) {
			// do nothing
		}

		return ids;
	}

	/**
	 * Updates plate yr
	 * 
	 */
	public static void updatePlate(PlateDetail plateDetail) {

		try {

			connect();

			String sql = "UPDATE plate SET InkRef=?, YRNumber=?, stock=? WHERE ID=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, plateDetail.getInkRefString());
			prepStmt.setString(2, plateDetail.getYrNumber());
			prepStmt.setInt(3, plateDetail.getStock());
			prepStmt.setInt(4, plateDetail.getId());
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			// do nothing
		}
	}

	/**
	 * Decrements plate stock
	 * 
	 * @param plate
	 */
	public static void updatePlateStock(UnconfirmedPlates plate) {

		try {

			connect();

			String sql = "UPDATE plate SET stock=stock-? WHERE ID=? AND stock > 0";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setInt(1, plate.getDecrement());
			prepStmt.setInt(2, plate.getId());
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			// do nothing
		}
	}

	/**
	 * Decrements plate stock
	 * 
	 * @param plate
	 */
	public static void updateCommonPlateStock(int decrement, int id) {

		try {

			connect();

			String sql = "UPDATE common_plate SET stock=stock-? WHERE common_plate_id=? AND stock > 0";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setInt(1, decrement);
			prepStmt.setInt(2, id);
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Increments plate stock
	 * 
	 * @param plate
	 */
	public static void incrementColourPlateStock(int id) {

		try {

			connect();

			String sql = "UPDATE plate SET stock=stock+1 WHERE ID=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setInt(1, id);
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			// do nothing
		}
	}

	/**
	 * Increments common plate stock
	 * 
	 * @param id
	 */
	public static void incrementCommonPlateStock(int id) {

		try {

			connect();

			String sql = "UPDATE common_plate SET stock=stock+1 WHERE common_plate_id=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setInt(1, id);
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			// do nothing
		}
	}

	/**
	 * Decrements colour plate stock
	 * 
	 * @param plate
	 */
	public static void decrementColourPlateStock(int id) {

		try {

			connect();

			String sql = "UPDATE plate SET stock=stock-1 WHERE ID=? AND stock > 0";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setInt(1, id);
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			// do nothing
		}
	}

	/**
	 * Decrements common plate stock
	 * 
	 * @param id
	 */
	public static void decrementCommonPlateStock(int id) {

		try {

			connect();

			String sql = "UPDATE common_plate SET stock=stock-1 WHERE common_plate_id=? AND stock > 0";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setInt(1, id);
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			// do nothing
		}
	}

	/**
	 * Updates plate yr
	 * 
	 */
	public static void updatePlateColorAndYr(String colour, String yr, int id) {

		try {

			connect();

			String sql = "UPDATE plate SET InkRef=?, YRNumber=? WHERE ID=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, colour);
			prepStmt.setString(2, yr);
			prepStmt.setInt(3, id);
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get colours from the database
	 * 
	 * @return colours
	 */
	public static ArrayList<String> getColours() {

		ArrayList<String> colours = new ArrayList<String>();

		try {
			connect();

			String sql = "Select PlateColour from colour ORDER BY PlateColour asc";
			rs = st.executeQuery(sql);

			while (rs.next()) {

				colours.add(rs.getString("PlateColour"));
			}

			closeConnection();

		} catch (SQLException e) {
			// Do nothing
		}
		return colours;
	}

	/**
	 * Inserts a new customer
	 */
	public static void insertColour(String colour) {

		try {

			connect();

			String sql = "INSERT INTO colour (PlateColour) VALUES(?)";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, colour);
			prepStmt.executeUpdate();

			closeConnection();

			JOptionPane.showMessageDialog(null,
					"New colour successfully added", "Success",
					JOptionPane.INFORMATION_MESSAGE);

		} catch (SQLException e) {
			// Do nothing
		}
	}

	/**
	 * Inserts a new die reorder reason
	 * 
	 * @param reason
	 */
	public static void insertReasonDies(String reason) {

		try {

			connect();

			String sql = "INSERT INTO reorder_reason_die (reason) VALUES(?)";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, reason);
			prepStmt.executeUpdate();

			closeConnection();

			JOptionPane.showMessageDialog(null,
					"New reason successfully added", "Success",
					JOptionPane.INFORMATION_MESSAGE);

		} catch (SQLException e) {
			// Do nothing
		}
	}

	/**
	 * Inserts a new size
	 */
	public static void insertSize(String size) {

		try {

			connect();

			String sql = "INSERT INTO size_table (size) VALUES(?)";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, size);
			prepStmt.executeUpdate();

			closeConnection();

			JOptionPane.showMessageDialog(null,
					"New size successfully inserted", "Success",
					JOptionPane.INFORMATION_MESSAGE);

		} catch (SQLException e) {
			// Do nothing
		}
	}

	/**
	 * Delete job ref from the database
	 * 
	 * @param userName
	 */
	public static void deleteJobRef(String jobRef) {

		try {

			connect();

			String sql = "Delete from job_reference where JobRef = ?";
			PreparedStatement prepStmt = (PreparedStatement) con
					.prepareStatement(sql);
			prepStmt.setString(1, jobRef);
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			// Do nothing
		}
	}

	/**
	 * Deletes a product code from database
	 * 
	 * @param jobRef
	 */
	public static void deleteProductCode(String productCode) {

		try {

			connect();

			String sql = "Delete from product_code where ProductCode = ?";
			PreparedStatement prepStmt = (PreparedStatement) con
					.prepareStatement(sql);
			prepStmt.setString(1, productCode);
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			// Do nothing
		}
	}

	/**
	 * Delete plate from the database
	 * 
	 * @param userName
	 */
	public static void deletePlate(int id) {

		try {

			connect();

			String sql = "Delete from plate where ID = ?";
			PreparedStatement prepStmt = (PreparedStatement) con
					.prepareStatement(sql);
			prepStmt.setInt(1, id);
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void insertShift(String shift) {

		String sql = "INSERT INTO shift_code (ShiftDescription) VALUES (?)";

		try {
			connect();

			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, shift);
			prepStmt.executeUpdate();

			closeConnection();

			JOptionPane.showMessageDialog(null, "Shift added", "Success",
					JOptionPane.INFORMATION_MESSAGE);

		} catch (SQLException e) {

			// Do nothing
		}
	}

	/**
	 * Updates email sent status
	 * 
	 * @param dieType
	 *            Internal/External
	 * @return
	 */
	public static void updateEmailSent(String dieType, String mod, String email) {

		try {

			if (dieType.equals("internal")) {

				if (email.equals("first")) {
					connect();

					String sql = "UPDATE mod_int SET 1st_email_sent=? WHERE MOD_Number=?";
					PreparedStatement prepStmt = con.prepareStatement(sql);
					prepStmt.setBoolean(1, true);
					prepStmt.setString(2, mod);
					prepStmt.executeUpdate();

					closeConnection();
				} else {
					connect();

					String sql = "UPDATE mod_int SET 2nd_email_sent=? WHERE MOD_Number=?";
					PreparedStatement prepStmt = con.prepareStatement(sql);
					prepStmt.setBoolean(1, true);
					prepStmt.setString(2, mod);
					prepStmt.executeUpdate();

					closeConnection();
				}

			} else {

				if (email.equals("second")) {
					connect();

					String sql = "UPDATE mod_ext SET 1st_email_sent=? WHERE MOD_Number=?";
					PreparedStatement prepStmt = con.prepareStatement(sql);
					prepStmt.setBoolean(1, true);
					prepStmt.setString(2, mod);
					prepStmt.executeUpdate();

					closeConnection();
				} else {
					connect();

					String sql = "UPDATE mod_ext SET 2nd_email_sent=? WHERE MOD_Number=?";
					PreparedStatement prepStmt = con.prepareStatement(sql);
					prepStmt.setBoolean(1, true);
					prepStmt.setString(2, mod);
					prepStmt.executeUpdate();

					closeConnection();
				}
			}

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "error");
		}
	}

	/**
	 * Updates whether a die replacement has been ordered for a given mod number
	 * 
	 * @param dieType
	 * @param mod
	 */
	public static void updateReplacementOrdered(String dieType, String mod) {

		try {

			if (dieType.equals("internal")) {

				connect();

				String sql = "UPDATE mod_int SET replacement_ordered=curdate() WHERE MOD_Number=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, mod);
				prepStmt.executeUpdate();

				closeConnection();
			} else {
				connect();

				String sql = "UPDATE mod_ext SET replacement_ordered=curdate() WHERE MOD_Number=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, mod);
				prepStmt.executeUpdate();

				closeConnection();
			}

		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "error");
		}
	}

	/**
	 * Returns removed dies refined by date
	 * 
	 * @param dieType
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static ArrayList<Die> getRemovedDiesByDate(String dieType,
			java.util.Date startDate, java.util.Date endDate) {
		java.sql.Date sqlStartDate = new java.sql.Date(startDate.getTime());
		java.sql.Date sqlEndDate = new java.sql.Date(endDate.getTime());

		ArrayList<Die> dies = new ArrayList<Die>();
		try {

			if (dieType.equals("internal")) {

				connect();

				String sql = "SELECT MOD_Number, DC_Number, RemovedDate, RemovedReason, DieCost, "
						+ "Removed FROM mod_int WHERE Removed=? AND RemovedDate >= ? "
						+ "AND RemovedDate <= ?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setBoolean(1, true);
				prepStmt.setDate(2, sqlStartDate);
				prepStmt.setDate(3, sqlEndDate);
				rs = prepStmt.executeQuery();

				while (rs.next()) {

					dies.add(new Die(rs.getString("DC_Number"), rs
							.getString("MOD_Number"),
							rs.getDate("RemovedDate"), rs
									.getString("RemovedReason"), rs
									.getDouble("DieCost")));
				}

				closeConnection();

			} else {

				connect();

				String sql = "SELECT MOD_Number, DC_Number, RemovedDate, RemovedReason, DieCost, "
						+ "Removed FROM mod_ext WHERE Removed=? AND RemovedDate >= ? "
						+ "AND RemovedDate <= ?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setBoolean(1, false);
				prepStmt.setDate(2, sqlStartDate);
				prepStmt.setDate(3, sqlEndDate);
				rs = prepStmt.executeQuery();

				while (rs.next()) {

					dies.add(new Die(rs.getString("DC_Number"), rs
							.getString("MOD_Number"),
							rs.getDate("RemovedDate"), rs
									.getString("RemovedReason"), rs
									.getDouble("DieCost")));
				}

				closeConnection();
			}

		} catch (SQLException e) {
			// Do nothing
		}

		return dies;
	}

	/**
	 * Returns a list of manufacturers from the reference table
	 * 
	 * @return
	 */
	public static ArrayList<String> getManufacturer() {

		ArrayList<String> manufacturer = new ArrayList<String>();

		try {
			connect();

			String sql = "SELECT manufacturer FROM reference_table WHERE manufacturer IS NOT NULL";

			rs = st.executeQuery(sql);

			while (rs.next()) {

				manufacturer.add(rs.getString("manufacturer"));
			}

			closeConnection();

		} catch (SQLException e) {
			// Do nothing
		}
		return manufacturer;
	}

	/**
	 * Returns a list of die types from the reference table
	 * 
	 * @return
	 */
	public static ArrayList<String> getDieType() {

		ArrayList<String> dieType = new ArrayList<String>();

		try {
			connect();

			String sql = "SELECT dieType FROM reference_table WHERE dieType IS NOT NULL";

			rs = st.executeQuery(sql);

			while (rs.next()) {

				dieType.add(rs.getString("dieType"));
			}

			closeConnection();

		} catch (SQLException e) {
			// Do nothing
		}
		return dieType;
	}

	/**
	 * Returns a list of discount Types from the reference table
	 * 
	 * @return
	 */
	public static ArrayList<String> getDiscountType() {

		ArrayList<String> discountType = new ArrayList<String>();

		try {
			connect();

			String sql = "SELECT discountType FROM reference_table";

			rs = st.executeQuery(sql);

			while (rs.next()) {

				discountType.add(rs.getString("discountType"));
			}

			closeConnection();

		} catch (SQLException e) {
			// Do nothing
		}
		return discountType;
	}

	/**
	 * Returns a list of discount Types from the reference table
	 * 
	 * @return
	 */
	public static ArrayList<DiePrice> getDieCost() {

		ArrayList<DiePrice> diePrice = new ArrayList<DiePrice>();

		try {
			connect();

			String sql = "SELECT * FROM die_cost order by discountType, manufacturer desc, die_type desc, die_repeat";

			rs = st.executeQuery(sql);

			while (rs.next()) {

				diePrice.add(new DiePrice(rs.getInt("id"), rs
						.getString("die_type"), rs.getString("die_repeat"), rs
						.getString("manufacturer"), rs.getDouble("price"), rs
						.getString("discountType")));
			}

			closeConnection();

		} catch (SQLException e) {
			// Do nothing
		}
		return diePrice;
	}

	/**
	 * Returns a die price based on parameters
	 * 
	 * @return
	 */
	public static double getDiePrice(String dieType, String repeat,
			String discountType, String manufacturer) {

		double price = 0;

		try {
			connect();

			String sql = "SELECT price FROM die_cost WHERE die_type=? AND die_repeat=? AND discountType=? AND manufacturer=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, dieType);
			prepStmt.setString(2, repeat);
			prepStmt.setString(3, discountType);
			prepStmt.setString(4, manufacturer);
			rs = prepStmt.executeQuery();

			if (rs.next()) {

				price = rs.getDouble("price");
			}

			closeConnection();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return price;
	}

	/**
	 * Updates the die_cost table
	 * 
	 * @param diePrice
	 */
	public static void updateDiePrice(DiePrice diePrice) {

		// System.out.println("update"+diePrice.getId());

		try {

			connect();

			String sql = "UPDATE die_cost SET die_type=?, die_repeat=?, price=?, "
					+ "discountType=?, manufacturer=? WHERE id=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, diePrice.getDieType());
			prepStmt.setString(2, diePrice.getRepeat());
			prepStmt.setDouble(3, diePrice.getPrice());
			prepStmt.setString(4, diePrice.getDiscountType());
			prepStmt.setString(5, diePrice.getManufacturer());
			prepStmt.setInt(6, diePrice.getId());
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Updates the die_cost table
	 * 
	 * @param diePrice
	 */
	public static int insertDiePrice(DiePrice diePrice) {

		// System.out.println("insert: " + diePrice.getId());

		int id = 0;

		try {

			connect();

			String sql = "INSERT INTO die_cost (die_type, die_repeat, price, "
					+ "discountType, manufacturer) VALUES (?,?,?,?,?)";
			PreparedStatement prepStmt = con.prepareStatement(sql,
					Statement.RETURN_GENERATED_KEYS);
			prepStmt.setString(1, diePrice.getDieType());
			prepStmt.setString(2, diePrice.getRepeat());
			prepStmt.setDouble(3, diePrice.getPrice());
			prepStmt.setString(4, diePrice.getDiscountType());
			prepStmt.setString(5, diePrice.getManufacturer());
			prepStmt.executeUpdate();

			// Retrieve id generated by mySQL
			rs = prepStmt.getGeneratedKeys();
			if (rs.next()) {
				id = rs.getInt(1);

				closeConnection();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return id;
	}

	/**
	 * @return Returns a list of job references from reordered colour table
	 */
	public static ArrayList<String> getJobRefsReordered() {

		ArrayList<String> arrayList = new ArrayList<String>();
		arrayList.add("All");

		connect();

		try {
			String sql = "SELECT DISTINCT JobRef FROM reordered_plate_colour WHERE JobRef IS NOT NULL ORDER BY JobRef";
			rs = st.executeQuery(sql);

			while (rs.next()) {
				arrayList.add(rs.getString("JobRef"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return arrayList;
	}

	/**
	 * @return Returns a list of product codes from reordered varnish table
	 */
	public static ArrayList<String> getVarnishProductCodesReordered() {

		ArrayList<String> arrayList = new ArrayList<String>();
		arrayList.add("All");

		connect();

		try {
			String sql = "SELECT DISTINCT ProductCode FROM reordered_plate_varnish WHERE ProductCode IS NOT NULL ORDER BY ProductCode";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				arrayList.add(rs.getString("ProductCode"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return arrayList;
	}

	/**
	 * @return Returns a list of product codes from reordered antimist table
	 */
	public static ArrayList<String> getAntimistProductCodesReordered() {

		ArrayList<String> arrayList = new ArrayList<String>();
		arrayList.add("All");

		connect();

		try {
			String sql = "SELECT DISTINCT ProductCode FROM reordered_plate_antimist WHERE ProductCode IS NOT NULL ORDER BY ProductCode";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				arrayList.add(rs.getString("ProductCode"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return arrayList;
	}

	/**
	 * @return Returns a list of product codes from reordered adhesive table
	 */
	public static ArrayList<String> getAdhesiveProductCodesReordered() {

		ArrayList<String> arrayList = new ArrayList<String>();
		arrayList.add("All");

		connect();

		try {
			String sql = "SELECT DISTINCT ProductCode FROM reordered_plate_adhesive WHERE ProductCode IS NOT NULL ORDER BY ProductCode";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				arrayList.add(rs.getString("ProductCode"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return arrayList;
	}

	/**
	 * @return Returns a list of product codes from reordered adhesive table
	 */
	public static ArrayList<String> getDatalaseProductCodesReordered() {

		ArrayList<String> arrayList = new ArrayList<String>();
		arrayList.add("All");

		connect();

		try {
			String sql = "SELECT DISTINCT ProductCode FROM reordered_plate_datalase WHERE ProductCode IS NOT NULL ORDER BY ProductCode";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				arrayList.add(rs.getString("ProductCode"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return arrayList;
	}

	/**
	 * @return Returns the common reorder reasons associated with the product
	 *         code
	 */
	public static ArrayList<String> getCommonReorderReasons(String plateType,
			String productCode) {

		ArrayList<String> reasons = new ArrayList<String>();
		reasons.add("All");

		try {

			if (plateType.equals("varnish")) {

				connect();

				String sql = "SELECT DISTINCT ReasonForOrder FROM reordered_plate_varnish WHERE ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					reasons.add(rs.getString("ReasonForOrder"));
				}

				closeConnection();
			}

			if (plateType.equals("antimist")) {

				connect();

				String sql = "SELECT DISTINCT ReasonForOrder FROM reordered_plate_antimist WHERE ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					reasons.add(rs.getString("ReasonForOrder"));
				}

				closeConnection();
			}

			if (plateType.equals("datalase")) {

				connect();

				String sql = "SELECT DISTINCT ReasonForOrder FROM reordered_plate_datalase WHERE ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					reasons.add(rs.getString("ReasonForOrder"));
				}

				closeConnection();
			}

			if (plateType.equals("adhesive")) {

				connect();

				String sql = "SELECT DISTINCT ReasonForOrder FROM reordered_plate_adhesive WHERE ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					reasons.add(rs.getString("ReasonForOrder"));
				}

				closeConnection();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return reasons;
	}

	/**
	 * @return Returns the colour reorder reasons associated with the job ref
	 */
	public static ArrayList<String> getColourReorderReasons(String jobRef) {

		ArrayList<String> reasons = new ArrayList<String>();
		reasons.add("All");

		try {

			connect();

			String sql = "SELECT DISTINCT ReasonForOrder FROM reordered_plate_colour WHERE JobRef=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, jobRef);
			rs = prepStmt.executeQuery();

			while (rs.next()) {
				reasons.add(rs.getString("ReasonForOrder"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return reasons;
	}

	/**
	 * @return Returns all colour reorder reasons
	 */
	public static ArrayList<String> getAllColourReorderReasons() {

		ArrayList<String> reasons = new ArrayList<String>();
		reasons.add("All");

		try {

			connect();

			String sql = "SELECT DISTINCT ReasonForOrder FROM reordered_plate_colour ORDER BY ReasonForOrder";
			rs = st.executeQuery(sql);

			while (rs.next()) {
				reasons.add(rs.getString("ReasonForOrder"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return reasons;
	}

	/**
	 * @return Returns the colour shifts associated with the job ref
	 */
	public static ArrayList<String> getColourShift(String jobRef) {

		ArrayList<String> shifts = new ArrayList<String>();
		shifts.add("All");

		try {

			connect();

			String sql = "SELECT DISTINCT ShiftCode FROM reordered_plate_colour WHERE JobRef=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, jobRef);
			rs = prepStmt.executeQuery();

			while (rs.next()) {
				shifts.add(rs.getString("ShiftCode"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return shifts;
	}

	/**
	 * @return Returns all colour shifts
	 */
	public static ArrayList<String> getAllColourShift() {

		ArrayList<String> shifts = new ArrayList<String>();
		shifts.add("All");

		try {

			connect();

			String sql = "SELECT DISTINCT ShiftCode FROM reordered_plate_colour ORDER BY ShiftCode";
			rs = st.executeQuery(sql);

			while (rs.next()) {
				shifts.add(rs.getString("ShiftCode"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return shifts;
	}

	/**
	 * @return Returns the colour names associated with the job ref
	 */
	public static ArrayList<String> getColourName(String jobRef) {

		ArrayList<String> names = new ArrayList<String>();
		names.add("All");

		try {

			connect();

			String sql = "SELECT DISTINCT OrderName FROM reordered_plate_colour WHERE JobRef=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, jobRef);
			rs = prepStmt.executeQuery();

			while (rs.next()) {
				names.add(rs.getString("OrderName"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return names;
	}

	/**
	 * @return Returns all colour names
	 */
	public static ArrayList<String> getAllColourName() {

		ArrayList<String> names = new ArrayList<String>();
		names.add("All");

		try {

			connect();

			String sql = "SELECT DISTINCT OrderName FROM reordered_plate_colour ORDER BY OrderName";
			rs = st.executeQuery(sql);

			while (rs.next()) {
				names.add(rs.getString("OrderName"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return names;
	}

	/**
	 * @return Returns all of the common reorder reasons
	 */
	public static ArrayList<String> getAllCommonReorderReasons(String plateType) {

		ArrayList<String> reasons = new ArrayList<String>();
		reasons.add("All");

		try {

			if (plateType.equals("colour")) {

				connect();

				String sql = "SELECT DISTINCT ReasonForOrder FROM reordered_plate_ WHERE ReasonForOrder IS NOT NULL ORDER BY ReasonForOrder";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					reasons.add(rs.getString("ReasonForOrder"));
				}

				closeConnection();
			}

			if (plateType.equals("varnish")) {

				connect();

				String sql = "SELECT DISTINCT ReasonForOrder FROM reordered_plate_varnish WHERE ReasonForOrder IS NOT NULL ORDER BY ReasonForOrder";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					reasons.add(rs.getString("ReasonForOrder"));
				}

				closeConnection();
			}

			if (plateType.equals("antimist")) {

				connect();

				String sql = "SELECT DISTINCT ReasonForOrder FROM reordered_plate_antimist WHERE ReasonForOrder IS NOT NULL ORDER BY ReasonForOrder";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					reasons.add(rs.getString("ReasonForOrder"));
				}

				closeConnection();
			}

			if (plateType.equals("datalase")) {

				connect();

				String sql = "SELECT DISTINCT ReasonForOrder FROM reordered_plate_datalase WHERE ReasonForOrder IS NOT NULL ORDER BY ReasonForOrder";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					reasons.add(rs.getString("ReasonForOrder"));
				}

				closeConnection();
			}

			if (plateType.equals("adhesive")) {

				connect();

				String sql = "SELECT DISTINCT ReasonForOrder FROM reordered_plate_adhesive WHERE ReasonForOrder IS NOT NULL ORDER BY ReasonForOrder";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					reasons.add(rs.getString("ReasonForOrder"));
				}

				closeConnection();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return reasons;
	}

	/**
	 * @return Returns the common reorder reasons associated with the product
	 *         code
	 */
	public static ArrayList<String> getCommonReorderShift(String plateType,
			String productCode) {

		ArrayList<String> shifts = new ArrayList<String>();
		shifts.add("All");

		try {

			if (plateType.equals("varnish")) {

				connect();

				String sql = "SELECT DISTINCT ShiftCode FROM reordered_plate_varnish WHERE ShiftCode IS NOT NULL AND ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					shifts.add(rs.getString("ShiftCode"));
				}

				closeConnection();
			}

			if (plateType.equals("antimist")) {

				connect();

				String sql = "SELECT DISTINCT ShiftCode FROM reordered_plate_antimist WHERE ShiftCode IS NOT NULL AND ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					shifts.add(rs.getString("ShiftCode"));
				}

				closeConnection();
			}

			if (plateType.equals("datalase")) {

				connect();

				String sql = "SELECT DISTINCT ShiftCode FROM reordered_plate_datalase WHERE ShiftCode IS NOT NULL AND ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					shifts.add(rs.getString("ShiftCode"));
				}

				closeConnection();
			}

			if (plateType.equals("adhesive")) {

				connect();

				String sql = "SELECT DISTINCT ShiftCode FROM reordered_plate_adhesive WHERE ShiftCode IS NOT NULL AND ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					shifts.add(rs.getString("ShiftCode"));
				}

				closeConnection();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return shifts;
	}

	/**
	 * @return Returns the common yrs associated with the product code
	 */
	public static ArrayList<String> getCommonReorderYr(String plateType,
			String productCode) {

		ArrayList<String> yrs = new ArrayList<String>();
		yrs.add("All");

		try {

			if (plateType.equals("varnish")) {

				connect();

				String sql = "SELECT DISTINCT YrNumber FROM reordered_plate_varnish WHERE YrNumber IS NOT NULL AND ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					yrs.add(rs.getString("YrNumber"));
				}

				closeConnection();
			}

			if (plateType.equals("antimist")) {

				connect();

				String sql = "SELECT DISTINCT YrNumber FROM reordered_plate_antimist WHERE YrNumber IS NOT NULL AND ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					yrs.add(rs.getString("YrNumber"));
				}

				closeConnection();
			}

			if (plateType.equals("datalase")) {

				connect();

				String sql = "SELECT DISTINCT YrNumber FROM reordered_plate_datalase WHERE YrNumber IS NOT NULL AND ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					yrs.add(rs.getString("YrNumber"));
				}

				closeConnection();
			}

			if (plateType.equals("adhesive")) {

				connect();

				String sql = "SELECT DISTINCT YrNumber FROM reordered_plate_adhesive WHERE YrNumber IS NOT NULL AND ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					yrs.add(rs.getString("YrNumber"));
				}

				closeConnection();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return yrs;
	}

	/**
	 * @return Returns the common yrs associated with the product code
	 */
	public static ArrayList<String> getAllCommonReorderYr(String plateType) {

		ArrayList<String> yrs = new ArrayList<String>();
		yrs.add("All");

		try {

			if (plateType.equals("varnish")) {

				connect();

				String sql = "SELECT DISTINCT YrNumber FROM reordered_plate_varnish WHERE YrNumber IS NOT NULL ORDER BY YrNumber";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					yrs.add(rs.getString("YrNumber"));
				}

				closeConnection();
			}

			if (plateType.equals("antimist")) {

				connect();

				String sql = "SELECT DISTINCT YrNumber FROM reordered_plate_antimist WHERE YrNumber IS NOT NULL ORDER BY YrNumber";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					yrs.add(rs.getString("YrNumber"));
				}

				closeConnection();
			}

			if (plateType.equals("datalase")) {

				connect();

				String sql = "SELECT DISTINCT YrNumber FROM reordered_plate_datalase WHERE YrNumber IS NOT NULL ORDER BY YrNumber";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					yrs.add(rs.getString("YrNumber"));
				}

				closeConnection();
			}

			if (plateType.equals("adhesive")) {

				connect();

				String sql = "SELECT DISTINCT YrNumber FROM reordered_plate_adhesive WHERE YrNumber IS NOT NULL ORDER BY YrNumber";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					yrs.add(rs.getString("YrNumber"));
				}

				closeConnection();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return yrs;
	}

	/**
	 * @return Returns all common reorder reasons
	 */
	public static ArrayList<String> getAllCommonReorderShift(String plateType) {

		ArrayList<String> shifts = new ArrayList<String>();
		shifts.add("All");

		try {

			if (plateType.equals("varnish")) {

				connect();

				String sql = "SELECT DISTINCT ShiftCode FROM reordered_plate_varnish WHERE ShiftCode IS NOT NULL ORDER BY ShiftCode";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					shifts.add(rs.getString("ShiftCode"));
				}

				closeConnection();
			}

			if (plateType.equals("antimist")) {

				connect();

				String sql = "SELECT DISTINCT ShiftCode FROM reordered_plate_antimist WHERE ShiftCode IS NOT NULL ORDER BY ShiftCode";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					shifts.add(rs.getString("ShiftCode"));
				}

				closeConnection();
			}

			if (plateType.equals("datalase")) {

				connect();

				String sql = "SELECT DISTINCT ShiftCode FROM reordered_plate_datalase WHERE ShiftCode IS NOT NULL ORDER BY ShiftCode";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					shifts.add(rs.getString("ShiftCode"));
				}

				closeConnection();
			}

			if (plateType.equals("adhesive")) {

				connect();

				String sql = "SELECT DISTINCT ShiftCode FROM reordered_plate_adhesive WHERE ShiftCode IS NOT NULL ORDER BY ShiftCode";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					shifts.add(rs.getString("ShiftCode"));
				}

				closeConnection();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return shifts;
	}

	/**
	 * @return Returns the common reorder reasons associated with the product
	 *         code
	 */
	public static ArrayList<String> getCommonReorderName(String plateType,
			String productCode) {

		ArrayList<String> names = new ArrayList<String>();
		names.add("All");

		try {

			if (plateType.equals("varnish")) {

				connect();

				String sql = "SELECT DISTINCT OrderName FROM reordered_plate_varnish WHERE OrderName IS NOT NULL AND ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					names.add(rs.getString("OrderName"));
				}

				closeConnection();
			}

			if (plateType.equals("antimist")) {

				connect();

				String sql = "SELECT DISTINCT OrderName FROM reordered_plate_antimist WHERE OrderName IS NOT NULL AND ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					names.add(rs.getString("OrderName"));
				}

				closeConnection();
			}

			if (plateType.equals("datalase")) {

				connect();

				String sql = "SELECT DISTINCT OrderName FROM reordered_plate_datalase WHERE OrderName IS NOT NULL AND ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					names.add(rs.getString("OrderName"));
				}

				closeConnection();
			}

			if (plateType.equals("adhesive")) {

				connect();

				String sql = "SELECT DISTINCT OrderName FROM reordered_plate_adhesive WHERE OrderName IS NOT NULL AND ProductCode=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, productCode);
				rs = prepStmt.executeQuery();

				while (rs.next()) {
					names.add(rs.getString("OrderName"));
				}

				closeConnection();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return names;
	}

	/**
	 * @return Returns all common reorder reasons
	 */
	public static ArrayList<String> getAllCommonReorderName(String plateType) {

		ArrayList<String> names = new ArrayList<String>();
		names.add("All");

		try {

			if (plateType.equals("varnish")) {

				connect();

				String sql = "SELECT DISTINCT OrderName FROM reordered_plate_varnish WHERE OrderName IS NOT NULL ORDER BY OrderName";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					names.add(rs.getString("OrderName"));
				}

				closeConnection();
			}

			if (plateType.equals("antimist")) {

				connect();

				String sql = "SELECT DISTINCT OrderName FROM reordered_plate_antimist WHERE OrderName IS NOT NULL ORDER BY OrderName";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					names.add(rs.getString("OrderName"));
				}

				closeConnection();
			}

			if (plateType.equals("datalase")) {

				connect();

				String sql = "SELECT DISTINCT OrderName FROM reordered_plate_datalase WHERE OrderName IS NOT NULL ORDER BY OrderName";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					names.add(rs.getString("OrderName"));
				}

				closeConnection();
			}

			if (plateType.equals("adhesive")) {

				connect();

				String sql = "SELECT DISTINCT OrderName FROM reordered_plate_adhesive WHERE OrderName IS NOT NULL ORDER BY OrderName";
				rs = st.executeQuery(sql);

				while (rs.next()) {
					names.add(rs.getString("OrderName"));
				}

				closeConnection();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return names;
	}

	public static void insertPlateCheckin(String name, int id, boolean remake) {

		try {

			connect();

			String sql = "INSERT INTO plate_checkin (check_in_name, plate_id, remake) VALUES(?,?,?)";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, name);
			prepStmt.setInt(2, id);
			prepStmt.setBoolean(3, remake);
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void insertCommonPlateCheckin(String name, int id,
			boolean remake) {

		try {

			connect();

			String sql = "INSERT INTO common_plate_checkin (check_in_name, plate_id, remake) VALUES(?,?,?)";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, name);
			prepStmt.setInt(2, id);
			prepStmt.setBoolean(3, remake);
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets common yr numbers from the common table
	 * 
	 * @param plateType
	 * @return
	 */
	public static ArrayList<String> getCommonYrFromCommonTable(String plateType) {

		ArrayList<String> yrs = new ArrayList<String>();
		yrs.add("-ADD NEW-");

		try {

			connect();

			String sql = "SELECT DISTINCT yr_number FROM common_plate WHERE plate_type=? order by yr_number";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, plateType);
			rs = prepStmt.executeQuery();

			while (rs.next()) {
				yrs.add(rs.getString("yr_number"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return yrs;
	}

	/**
	 * Returns a common plate
	 * 
	 * @param plateType
	 * @return
	 */
	public static CommonPlate getCommonPlate(String plateType, String yr) {

		CommonPlate commonPlate = null;

		try {

			connect();

			String sql = "SELECT * FROM common_plate WHERE yr_number=? AND plate_type=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, yr);
			prepStmt.setString(2, plateType);
			rs = prepStmt.executeQuery();

			if (rs.next()) {

				commonPlate = new CommonPlate(rs.getInt("common_plate_id"),
						rs.getString("yr_number"),
						rs.getString("plate_reference"),
						rs.getString("plate_Type"), rs.getInt("stock"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}

		return commonPlate;
	}

	/**
	 * Updates the commom_plate table
	 * 
	 * @param commonPlate
	 */
	public static void updateCommonPlate(CommonPlate commonPlate) {

		try {

			connect();

			String sql = "UPDATE common_plate SET yr_number=?, plate_reference=?, plate_type=?, stock=? WHERE common_plate_id=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, commonPlate.getYr());
			prepStmt.setString(2, commonPlate.getPlateRef());
			prepStmt.setString(3, commonPlate.getPlateType());
			prepStmt.setInt(4, commonPlate.getStock());
			prepStmt.setInt(5, commonPlate.getId());
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
	}

	public static void insertCommonYR(String plateType, String yr) {

		try {

			connect();

			String sql = "INSERT INTO common_plate (yr_number, plate_type) values(?,?)";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, yr);
			prepStmt.setString(2, plateType);
			prepStmt.executeUpdate();

			closeConnection();

			JOptionPane.showMessageDialog(null, "Yr inserted", "Success",
					JOptionPane.INFORMATION_MESSAGE);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
	}

	/**
	 * Returns varnish yr and id
	 * 
	 * @param plateType
	 * @param reference
	 * @return
	 */
	public static CommonPlate getCommonYrAndId(String plateType,
			String reference) {

		CommonPlate commonPlate = null;

		try {

			if (plateType.equals("varnish")) {
				connect();

				String sql = "SELECT VarnishYR, common_plate.common_plate_id "
						+ "FROM job_reference, common_plate "
						+ "WHERE JobRef=? and VarnishYR = common_plate.yr_number and plate_type=?";

				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, reference);
				prepStmt.setString(2, plateType);
				rs = prepStmt.executeQuery();

				if (rs.next()) {
					commonPlate = new CommonPlate(rs.getInt("common_plate_id"),
							rs.getString("VarnishYR"));
				}

				closeConnection();
			} else if (plateType.equals("antimist")) {

				connect();

				String sql = "SELECT AntimistYR, common_plate.common_plate_id FROM product_code, common_plate "
						+ "WHERE ProductCode=? and AntimistYR = common_plate.yr_number and plate_type=?";

				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, reference);
				prepStmt.setString(2, plateType);
				rs = prepStmt.executeQuery();

				if (rs.next()) {
					commonPlate = new CommonPlate(rs.getInt("common_plate_id"),
							rs.getString("AntimistYR"));
				}

				closeConnection();
			} else if (plateType.equals("adhesive")) {

				connect();

				String sql = "SELECT AdhesiveYR, common_plate.common_plate_id FROM product_code, common_plate "
						+ "WHERE ProductCode=? and AdhesiveYR = common_plate.yr_number and plate_type=?";

				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, reference);
				prepStmt.setString(2, plateType);
				rs = prepStmt.executeQuery();

				if (rs.next()) {
					commonPlate = new CommonPlate(rs.getInt("common_plate_id"),
							rs.getString("AdhesiveYR"));
				}

				closeConnection();
			} else if (plateType.equals("datalase")) {

				connect();

				String sql = "SELECT DatalaseYR, common_plate.common_plate_id FROM product_code, common_plate "
						+ "WHERE ProductCode=? and DatalaseYR = common_plate.yr_number and plate_type=?";

				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, reference);
				prepStmt.setString(2, plateType);
				rs = prepStmt.executeQuery();

				if (rs.next()) {
					commonPlate = new CommonPlate(rs.getInt("common_plate_id"),
							rs.getString("DatalaseYR"));
				}

				closeConnection();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return commonPlate;
	}

	/**
	 * Gets checked in plates and creates a csv document
	 * 
	 * @param plateType
	 */
	public static void getReorderedColourPlates(String plateType,
			Date startDate, Date endDate, String jobRef, String yr,
			String reason, String colour, String name) {

		ArrayList<ReorderColourPlate> plates = new ArrayList<ReorderColourPlate>();
		Desktop desktop = null;
		
		if (jobRef.equals("All")) {
			jobRef = "%";
		}
		if (yr.equals("All")) {
			yr = "%";
		}
		if (colour.equals("All")) {
			colour = "%";
		}
		if (reason.equals("All")) {
			reason = "%";
		}
		if (name.equals("All")) {
			name = "%";
		}

		if (startDate == null) {
			Calendar cal = Calendar.getInstance();
			cal.set(2013, 02, 01);
			startDate = cal.getTime();
		}
		if (endDate == null) {
			endDate = new Date();
		}

		try {

			java.sql.Date sqlStartDate = new java.sql.Date(startDate.getTime());
			java.sql.Date sqlEndDate = new java.sql.Date(endDate.getTime() + 24
					* 60 * 60 * 1000);

			if (plateType.equals("colour")) {

				connect();

				String sql = "select * from rapdb.reordered_plate_colour "
						+ "where JobRef like ? "
						+ "AND (YrNumber like ? OR YrNumber IS NULL) "
						+ "AND (ReasonForOrder like ? OR ReasonForOrder IS NULL) "
						+ "AND (Colour like ? OR Colour IS NULL) "
						+ "AND (OrderName like ? OR OrderName IS NULL) "
						+ "AND Date(TimeStamp) BETWEEN ? AND ?"
						+ "order by id desc";

				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setString(1, jobRef);
				prepStmt.setString(2, yr);
				prepStmt.setString(3, reason);
				prepStmt.setString(4, colour);
				prepStmt.setString(5, name);
				prepStmt.setDate(6, sqlStartDate);
				prepStmt.setDate(7, sqlEndDate);
				rs = prepStmt.executeQuery();

				while (rs.next()) {

					plates.add(new ReorderColourPlate(rs.getInt(1), rs
							.getString(2), rs.getString(3), rs.getString(4), rs
							.getString(5), rs.getTimestamp(6), rs.getString(7),
							rs.getInt(8), rs.getString(9)));
				}
				closeConnection();

				if (plates.size() > 0) {
					// Create csv file
					CreateCSV.createReorderedColourCSV("ReorderedColourPlates",
							plates);
					if (Desktop.isDesktopSupported()) {
						desktop = Desktop.getDesktop();
						// // now enable buttons for actions that are supported.
						// enableSupportedActions();
					}
					File file = new File("ReorderedColourPlates.csv");
					desktop.open(file);

				} else {
					JOptionPane.showMessageDialog(null,
							"No data within date range", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		}

		catch (FileNotFoundException f) {

			JOptionPane.showMessageDialog(null, "File already open", "Error",
					JOptionPane.ERROR_MESSAGE);

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void getCheckedInPlates(String plateType, Date startDate,
			Date endDate, boolean remake) {

		ArrayList<PlateCheckin> plateCheckins = new ArrayList<PlateCheckin>();
		Desktop desktop = null;

		if (startDate == null) {
			Calendar cal = Calendar.getInstance();
			cal.set(2013, 02, 01);
			startDate = cal.getTime();
		}
		if (endDate == null) {
			endDate = new Date();
		}

		try {

			java.sql.Date sqlStartDate = new java.sql.Date(startDate.getTime());
			java.sql.Date sqlEndDate = new java.sql.Date(endDate.getTime() + 24
					* 60 * 60 * 1000);

			if (plateType.equals("colour")) {

				if (!remake) {
					connect();
					String sql = "SELECT ID, JobRef, InkRef, YRNumber, stock, check_in_date, check_in_name "
							+ "FROM rapdb.plate, rapdb.plate_checkin "
							+ "WHERE plate.ID = plate_checkin.plate_id "
							+ "AND check_in_date between ? AND ? ORDER BY check_in_date DESC";
					PreparedStatement prepStmt = con.prepareStatement(sql);
					prepStmt.setDate(1, sqlStartDate);
					prepStmt.setDate(2, sqlEndDate);
					rs = prepStmt.executeQuery();
					while (rs.next()) {

						plateCheckins.add(new PlateCheckin(rs
								.getString("JobRef"), rs.getInt("Stock"), rs
								.getString("InkRef"), rs.getString("YRNumber"),
								rs.getDate("check_in_date"), rs
										.getString("check_in_name")));
					}
					closeConnection();
				} else {
					connect();
					String sql = "SELECT ID, JobRef, InkRef, YRNumber, stock, check_in_date, check_in_name "
							+ "FROM rapdb.plate, rapdb.plate_checkin "
							+ "WHERE plate.ID = plate_checkin.plate_id "
							+ "AND check_in_date between ? AND ? "
							+ "AND rapdb.plate_checkin.remake = ? ORDER BY check_in_date DESC";
					PreparedStatement prepStmt = con.prepareStatement(sql);
					prepStmt.setDate(1, sqlStartDate);
					prepStmt.setDate(2, sqlEndDate);
					prepStmt.setBoolean(3, true);
					rs = prepStmt.executeQuery();
					while (rs.next()) {

						plateCheckins.add(new PlateCheckin(rs
								.getString("JobRef"), rs.getInt("Stock"), rs
								.getString("InkRef"), rs.getString("YRNumber"),
								rs.getDate("check_in_date"), rs
										.getString("check_in_name")));
					}
					closeConnection();
				}
				if (plateCheckins.size() > 0) {
					// Create csv file
					CreateCSV.createCSV("Colour", plateCheckins);
					if (Desktop.isDesktopSupported()) {
						desktop = Desktop.getDesktop();
						// // now enable buttons for actions that are supported.
						// enableSupportedActions();
					}
					File file = new File("Colour.csv");
					desktop.open(file);

				} else {
					JOptionPane.showMessageDialog(null,
							"No data within date range", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			} else {

				if (!remake) {
					connect();
					String sql = "SELECT plate_reference, plate_type, yr_number, stock, check_in_date, check_in_name, "
							+ "(select case when plate_type = ? then (select ProductCode from product_code where AntimistYR = yr_number) "
							+ "when plate_type = ? then (select ProductCode from product_code where AdhesiveYR = yr_number) "
							+ "when plate_type = ? then (select ProductCode from product_code where DatalaseYR = yr_number) "
							+ "when plate_type = ? then (select substring(JobRef, 1, 4) from job_reference where VarnishYR = yr_number limit 1)end) as pc "
							+ "from rapdb.common_plate, rapdb.common_plate_checkin "
							+ "WHERE common_plate_id = common_plate_checkin.plate_id "
							+ "AND check_in_date between ? AND ? ORDER BY check_in_date DESC";
					PreparedStatement prepStmt = con.prepareStatement(sql);
					prepStmt.setString(1, "antimist");
					prepStmt.setString(2, "adhesive");
					prepStmt.setString(3, "datalase");
					prepStmt.setString(4, "varnish");
					prepStmt.setDate(5, sqlStartDate);
					prepStmt.setDate(6, sqlEndDate);
					rs = prepStmt.executeQuery();
					while (rs.next()) {

						plateCheckins
								.add(new PlateCheckin(rs
										.getString("plate_reference"), rs
										.getString("plate_type"), rs
										.getString("yr_number"), rs
										.getInt("stock"), rs
										.getDate("check_in_date"), rs
										.getString("check_in_name"), rs
										.getString("pc")));
					}
					closeConnection();
				} else {
					connect();
					String sql = "SELECT plate_reference, plate_type, yr_number, stock, check_in_date, check_in_name, "
							+ "(select case when plate_type = ? then (select ProductCode from product_code where AntimistYR = yr_number) "
							+ "when plate_type = ? then (select ProductCode from product_code where AdhesiveYR = yr_number) "
							+ "when plate_type = ? then (select ProductCode from product_code where DatalaseYR = yr_number) "
							+ "when plate_type = ? then (select substring(JobRef, 1, 4) from job_reference where VarnishYR = yr_number limit 1)end) as pc "
							+ "from rapdb.common_plate, rapdb.common_plate_checkin "
							+ "WHERE common_plate_id = common_plate_checkin.plate_id "
							+ "AND check_in_date between ? AND ? AND common_plate_checkin.remake = ? ORDER BY check_in_date DESC";
					PreparedStatement prepStmt = con.prepareStatement(sql);
					prepStmt.setString(1, "antimist");
					prepStmt.setString(2, "adhesive");
					prepStmt.setString(3, "datalase");
					prepStmt.setString(4, "varnish");
					prepStmt.setDate(5, sqlStartDate);
					prepStmt.setDate(6, sqlEndDate);
					prepStmt.setBoolean(7, true);
					rs = prepStmt.executeQuery();
					while (rs.next()) {

						plateCheckins
								.add(new PlateCheckin(rs
										.getString("plate_reference"), rs
										.getString("plate_type"), rs
										.getString("yr_number"), rs
										.getInt("stock"), rs
										.getDate("check_in_date"), rs
										.getString("check_in_name"), rs
										.getString("pc")));
					}
					closeConnection();
				}
				if (plateCheckins.size() > 0) {
					// Create csv file
					CreateCSV.createCSV("Common", plateCheckins);
					if (Desktop.isDesktopSupported()) {
						desktop = Desktop.getDesktop();
						// // now enable buttons for actions that are supported.
						// enableSupportedActions();
					}
					File file = new File("Common.csv");
					desktop.open(file);

				} else {
					JOptionPane.showMessageDialog(null,
							"No data within date range", "Error",
							JOptionPane.ERROR_MESSAGE);
				}

			}

		} catch (FileNotFoundException f) {

			JOptionPane.showMessageDialog(null, "File already open", "Error",
					JOptionPane.ERROR_MESSAGE);

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public static void getAllJobRefs() {
		try {
			connect();

			int i = 0;
			Desktop desktop = null;
			FileWriter writer = new FileWriter("JobRefs.csv");

			String sql = "SELECT JobRef from job_reference";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				writer.append(rs.getString("JobRef"));
				writer.append(",");
				writer.append(",");
				if (i++ == 5) {
					writer.append("\n");
					i = 0;
				}
			}

			writer.flush();
			writer.close();

			closeConnection();

			if (Desktop.isDesktopSupported()) {
				desktop = Desktop.getDesktop();
				// // now enable buttons for actions that are supported.
				// enableSupportedActions();
			}
			File file = new File("JobRefs.csv");
			desktop.open(file);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Inserts plates into plate_order table
	 * 
	 * @param colourOrder
	 */
	public static void insertColourPlateOrder(ColourOrder colourOrder) {

		try {

			connect();

			for (Plate plate : colourOrder.getPlates()) {

				String sql = "INSERT INTO plate_order (plate_id, plate_type, reference, yr_reference, colour, order_qty, decrement, reorder_reason, timeline, name, shift_code) values(?,?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setInt(1, plate.getId());
				prepStmt.setString(2, colourOrder.getOrderType());
				prepStmt.setString(3, colourOrder.getReference());
				prepStmt.setString(4, plate.getYr_number());
				prepStmt.setString(5, plate.getInkRef());
				prepStmt.setInt(6, plate.getOrderQty());
				prepStmt.setInt(7, plate.getDecrement());
				prepStmt.setString(8, plate.getReorderReason());
				prepStmt.setString(9, plate.getDeliveryTimeline());
				prepStmt.setString(10, colourOrder.getOrderName());
				prepStmt.setString(11, colourOrder.getShiftCode());
				prepStmt.executeUpdate();
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
	}

	public static void insertCommonPlateOrder(CommonOrder commonOrder) {

		try {

			connect();

			String sql = "INSERT INTO plate_order (plate_id, plate_type, reference, yr_reference, colour, order_qty, decrement, reorder_reason, timeline, name, shift_code) values(?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setInt(1, commonOrder.getPlate().getId());
			prepStmt.setString(2, commonOrder.getOrderType());
			prepStmt.setString(3, commonOrder.getReference());
			prepStmt.setString(4, commonOrder.getPlate().getYr_number());
			prepStmt.setString(5, "n/a");
			prepStmt.setInt(6, commonOrder.getPlate().getOrderQty());
			prepStmt.setInt(7, commonOrder.getPlate().getDecrement());
			prepStmt.setString(8, commonOrder.getPlate().getReorderReason());
			prepStmt.setString(9, commonOrder.getPlate().getDeliveryTimeline());
			prepStmt.setString(10, commonOrder.getOrderName());
			prepStmt.setString(11, commonOrder.getShiftCode());
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
	}

	/**
	 * Gets distinct references from plate_order table
	 * 
	 * @return
	 */
	public static ArrayList<String> getReferences() {

		ArrayList<String> arrayList = new ArrayList<String>();

		connect();

		try {
			String sql = "SELECT DISTINCT reference FROM plate_order WHERE confirmed=0";

			rs = st.executeQuery(sql);

			while (rs.next()) {
				arrayList.add(rs.getString("reference"));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return arrayList;
	}

	/**
	 * Gets distinct references from plate_order table
	 * 
	 * @return
	 */
	public static ArrayList<UnconfirmedPlates> getOrderedPlates(String reference) {

		ArrayList<UnconfirmedPlates> arrayList = new ArrayList<UnconfirmedPlates>();

		connect();

		try {
			String sql = "SELECT * FROM plate_order WHERE confirmed=0 and reference=?";

			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setString(1, reference);
			rs = prepStmt.executeQuery();

			while (rs.next()) {
				arrayList.add(new UnconfirmedPlates(rs.getInt(1), rs
						.getString(2), rs.getString(3), rs.getString(4), rs
						.getString(5), rs.getInt(6), rs.getInt(13), rs
						.getString(7), rs.getString(8), rs.getString(9), rs
						.getString(11), rs.getBoolean(10)));
			}

			closeConnection();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException n) {
			// Do nothing
		}
		return arrayList;
	}

	public static void updateUnconfirmedPlates(UnconfirmedPlates u) {

		try {

			connect();

			String sql = "UPDATE plate_order SET order_qty=?, reorder_reason=?, timeline=? WHERE plate_order_id=?";
			PreparedStatement prepStmt = con.prepareStatement(sql);
			prepStmt.setInt(1, u.getQty());
			prepStmt.setString(2, u.getReason());
			prepStmt.setString(3, u.getTimeline());
			prepStmt.setInt(4, u.getId());
			prepStmt.executeUpdate();

			closeConnection();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void updateUnconfirmedPlateStatus(
			ArrayList<UnconfirmedPlates> u) {

		try {

			connect();

			for (UnconfirmedPlates plate : u) {
				String sql = "UPDATE plate_order SET confirmed=? WHERE plate_order_id=?";
				PreparedStatement prepStmt = con.prepareStatement(sql);
				prepStmt.setBoolean(1, true);
				prepStmt.setInt(2, plate.getId());
				prepStmt.executeUpdate();
			}

			closeConnection();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Deletes mod numbers from table
	 * 
	 * @param id
	 */
	public static void deleteMod(String dieType, int id) {

		try {

			if (dieType.equals("internal")) {
				connect();
				String sql = "Delete from mod_int where id = ?";
				PreparedStatement prepStmt = (PreparedStatement) con
						.prepareStatement(sql);
				prepStmt.setInt(1, id);
				prepStmt.executeUpdate();
				closeConnection();
			} else {
				connect();
				String sql = "Delete from mod_ext where id = ?";
				PreparedStatement prepStmt = (PreparedStatement) con
						.prepareStatement(sql);
				prepStmt.setInt(1, id);
				prepStmt.executeUpdate();
				closeConnection();
			}

		} catch (SQLException e) {
			// Do nothing
		}
	}

}// End of class
