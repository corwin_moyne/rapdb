/**
 * 
 */
package ie.rapdb;

import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.table.AbstractTableModel;

/**
 * @author Corwin
 * 
 */
public class TableModel_PlateDetail extends AbstractTableModel {

	private ArrayList<PlateDetail> plateDetails;

	private String[] columnNames = { "Ink Ref", "YR Number", "Stock", "+", "-",
			"" };

	public TableModel_PlateDetail(ArrayList<PlateDetail> plateDetails) {
		this.plateDetails = plateDetails;
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return plateDetails.size();
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public Object getValueAt(int row, int col) {

		PlateDetail p = plateDetails.get(row);

		switch (col) {

		case 0:
			return p.getInkRefString();
		case 1:
			return p.getYrNumber();
		case 2:
			return p.getStock();
		case 3:
			JLabel plusLabel = new JLabel(new ImageIcon(getClass().getResource(
					"/image/plus.png")));
			return plusLabel;
		case 4:
			JLabel minusLabel = new JLabel(new ImageIcon(getClass()
					.getResource("/image/minus.png")));
			return minusLabel;
		case 5:
			JButton button = new JButton("Delete");
			return button;
		}

		return null;
	}

	public void setValueAt(Object value, int row, int col) {

		PlateDetail plateDetail = plateDetails.get(row);

		switch (col) {

		case 0:
			plateDetail.setInkRefString((String) value);
			break;
		case 1:
			plateDetail.setYrNumber((String) value);
			break;
		case 2:
			if ((int) value > 0)
			{
				plateDetail.setStock((int) value);
			}
			else
			{
				plateDetail.setStock(0);
			}
			break;
		}

		DatabaseConnection.updatePlate(plateDetail);
	}

	// getColumnClass
	@Override
	public Class<?> getColumnClass(int col) {

		// return getValueAt(0, col).getClass();

		Class[] columns = new Class[] { JComboBox.class, String.class,
				Integer.class, JButton.class };

		return columns[col];
	}

	@Override
	public boolean isCellEditable(int row, int col) {

		if (col == 3 || col == 4 || col == 5) {
			return false;
		}

		return true;

	}

}
