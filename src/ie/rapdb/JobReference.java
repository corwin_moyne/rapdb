/**
 * 
 */
package ie.rapdb;

/**
 * @author Corwin
 *
 */
public class JobReference {
    
    private String jobReference;
    private String customer;
    private String varnishYr;
    private boolean isObsolete;
    /**
     * 
     */
    public JobReference() {
	
	this.jobReference = new String();
	this.customer = new String();
	this.varnishYr = new String();
	this.isObsolete = false;
    }
    
    /**
     * @param jobReference
     * @param customer
     * @param varnishYr
     */
    public JobReference(String jobReference, String customer, String varnishYr) {
	this.jobReference = jobReference;
	this.customer = customer;
	this.varnishYr = varnishYr;
	this.isObsolete = false;
    }
    /**
     * @param jobReference
     * @param customer
     * @param varnishYr
     */
    public JobReference(String jobReference, String customer, String varnishYr, boolean isObsolete) {
	this.jobReference = jobReference;
	this.customer = customer;
	this.varnishYr = varnishYr;
	this.isObsolete = isObsolete;
    }
    /**
     * @return the jobReference
     */
    public String getJobReference() {
        return jobReference;
    }
    /**
     * @param jobReference the jobReference to set
     */
    public void setJobReference(String jobReference) {
        this.jobReference = jobReference;
    }
    /**
     * @return the customer
     */
    public String getCustomer() {
        return customer;
    }
    /**
     * @param customer the customer to set
     */
    public void setCustomer(String customer) {
        this.customer = customer;
    }
    /**
     * @return the varnishYr
     */
    public String getVarnishYr() {
        return varnishYr;
    }
    /**
     * @param varnishYr the varnishYr to set
     */
    public void setVarnishYr(String varnishYr) {
        this.varnishYr = varnishYr;
    }
    
    
    public boolean isObsolete() {
		return isObsolete;
	}
	public void setObsolete(boolean isObsolete) {
		this.isObsolete = isObsolete;
	}
	
	@Override
	public String toString() {
		return "JobReference [jobReference=" + jobReference + ", customer="
				+ customer + ", varnishYr=" + varnishYr + ", isObsolete="
				+ isObsolete + "]";
	}
}
