/**
 * 
 */
package ie.rapdb;

/**
 * @author Corwin
 *
 */
public class CommonOrder extends Order{
    
    private Plate plate;

    /**
     * @param plate
     */
    public CommonOrder(Plate plate) {
	super();
	this.plate = plate;
    }

    /**
     * @param reference
     * @param orderType
     * @param orderName
     * @param shiftCode
     */
    public CommonOrder(String reference, String orderType, String orderName,
	    String shiftCode, Plate plate) {
	super(reference, orderType, orderName, shiftCode);
	this.plate = plate;
    }

    /**
     * @return the plate
     */
    public Plate getPlate() {
        return plate;
    }

    /**
     * @param plate the plate to set
     */
    public void setPlate(Plate plate) {
        this.plate = plate;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
	return "CommonOrder [plate=" + plate + ", reference=" + reference
		+ ", orderType=" + orderType + ", orderName=" + orderName
		+ ", shiftCode=" + shiftCode + "]";
    }
    
    

}
