/**
 * 
 */
package ie.rapdb;

/**
 * @author Corwin
 *
 */
public class UnconfirmedPlates {
	
	private int id;
	private String plateType;
	private String reference;
	private String yr;
	private String colour;
	private int qty;
	private int decrement;
	private String reason;
	private String timeline;
	private String name;
	private String shift;
	private boolean confirmed;
	
	public UnconfirmedPlates(int id, String plateType, String reference,
			String yr, String colour, int qty, int decrement, String reason,
			String timeline, String name, String shift, boolean confirmed) {
		this.id = id;
		this.plateType = plateType;
		this.reference = reference;
		this.yr = yr;
		this.colour = colour;
		this.qty = qty;
		this.decrement = decrement;
		this.reason = reason;
		this.timeline = timeline;
		this.name = name;
		this.shift = shift;
		this.confirmed = true;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the plateType
	 */
	public String getPlateType() {
		return plateType;
	}

	/**
	 * @param plateType the plateType to set
	 */
	public void setPlateType(String plateType) {
		this.plateType = plateType;
	}

	/**
	 * @return the reference
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * @param reference the reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * @return the yr
	 */
	public String getYr() {
		return yr;
	}

	/**
	 * @param yr the yr to set
	 */
	public void setYr(String yr) {
		this.yr = yr;
	}

	/**
	 * @return the colour
	 */
	public String getColour() {
		return colour;
	}

	/**
	 * @param colour the colour to set
	 */
	public void setColour(String colour) {
		this.colour = colour;
	}

	/**
	 * @return the qty
	 */
	public int getQty() {
		return qty;
	}

	/**
	 * @param qty the qty to set
	 */
	public void setQty(int qty) {
		this.qty = qty;
	}
	
	/**
	 * @return the decrement
	 */
	public int getDecrement() {
		return decrement;
	}

	/**
	 * @param decrement the decrement to set
	 */
	public void setDecrement(int decrement) {
		this.decrement = decrement;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the timeline
	 */
	public String getTimeline() {
		return timeline;
	}

	/**
	 * @param timeline the timeline to set
	 */
	public void setTimeline(String timeline) {
		this.timeline = timeline;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the shift
	 */
	public String getShift() {
		return shift;
	}

	/**
	 * @param shift the shift to set
	 */
	public void setShift(String shift) {
		this.shift = shift;
	}

	/**
	 * @return the confirmed
	 */
	public boolean isConfirmed() {
		return confirmed;
	}

	/**
	 * @param confirmed the confirmed to set
	 */
	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UnconfirmedPlates [id=" + id + ", plateType=" + plateType
				+ ", reference=" + reference + ", yr=" + yr + ", colour="
				+ colour + ", qty=" + qty + ", reason=" + reason
				+ ", timeline=" + timeline + ", name=" + name + ", shift="
				+ shift + ", confirmed=" + confirmed + "]";
	}
	
	

}
