/**
 * 
 */
package ie.rapdb;

/**
 * @author Corwin
 *
 */
public class DiePrice {
	
	private int id;
	private String dieType;
	private String repeat;
	private String manufacturer;
	private double price;
	private String discountType;
	
	
	public DiePrice() {
		
		id = 0;
		dieType = new String();
		repeat = new String();
		manufacturer = new String();
		price = 0;
		discountType = new String();
	}

	public DiePrice(String dieType, String repeat, String manufacturer,
			double price, String discountType) {
		
		this.id = 0;
		this.dieType = dieType;
		this.repeat = repeat;
		this.manufacturer = manufacturer;
		this.price = price;
		this.discountType = discountType;
	}

	public DiePrice(int id, String dieType, String repeat, String manufacturer,
			double price, String discountType) {
		
		this.id = id;
		this.dieType = dieType;
		this.repeat = repeat;
		this.manufacturer = manufacturer;
		this.price = price;
		this.discountType = discountType;
	}
	
	

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getDieType() {
		return dieType;
	}


	public void setDieType(String dieType) {
		this.dieType = dieType;
	}


	public String getRepeat() {
		return repeat;
	}


	public void setRepeat(String repeat) {
		this.repeat = repeat;
	}


	public String getManufacturer() {
		return manufacturer;
	}


	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}


	public String getDiscountType() {
		return discountType;
	}


	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}


	@Override
	public String toString() {
		return "DiePrice [dieType=" + dieType + ", repeat=" + repeat
				+ ", manufacturer=" + manufacturer + ", price=" + price
				+ ", discountType=" + discountType + "]";
	}
	
	

}
