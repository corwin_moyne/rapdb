package ie.rapdb;

public class Plate {

	private int id;
	private String inkRef;
	private String yr_number;
	private int orderQty;
	private int decrement;
	private String reorderReason;
	private String deliveryTimeline;
	private boolean addToOrder;

	/**
     * 
     */
	public Plate() {

		this.id = 0;
		this.inkRef = new String();
		this.yr_number = new String();
		this.orderQty = 1;
		this.decrement = 1;
		this.reorderReason = new String();
		this.deliveryTimeline = new String();
		this.addToOrder = false;
	}

	/**
	 * @param inkRef
	 * @param yr_number
	 */
	public Plate(int id, String inkRef, String yr_number) {
		
		this.id = id;
		this.inkRef = inkRef;
		this.yr_number = yr_number;
		this.orderQty = 1;
		this.decrement = 1;
		this.reorderReason = new String();
		this.deliveryTimeline = new String();
		this.addToOrder = false;
	}
	
	public Plate(int id, String inkRef, String reorderReason, String yr_number, int orderQty) {
		
		this.id = 0;
		this.inkRef = inkRef;
		this.reorderReason = reorderReason;
		this.yr_number = yr_number;
		this.orderQty = orderQty;
	}

	/**
	 * @param yr_number
	 * @param orderQty
	 * @param reorderReason
	 * @param deliveryTimeline
	 */
	public Plate(String yr_number, int orderQty, int decrement, String reorderReason,
			String deliveryTimeline) {
		
		this.id = 0;
		this.yr_number = yr_number;
		this.orderQty = orderQty;
		this.decrement = decrement;
		this.reorderReason = reorderReason;
		this.deliveryTimeline = deliveryTimeline;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the inkRef
	 */
	public String getInkRef() {
		return inkRef;
	}

	/**
	 * @param inkRef
	 *            the inkRef to set
	 */
	public void setInkRef(String inkRef) {
		this.inkRef = inkRef;
	}

	/**
	 * @return the yr_number
	 */
	public String getYr_number() {
		return yr_number;
	}

	/**
	 * @param yr_number
	 *            the yr_number to set
	 */
	public void setYr_number(String yr_number) {
		this.yr_number = yr_number;
	}

	/**
	 * @return the addToOrder
	 */
	public boolean isAddToOrder() {
		return addToOrder;
	}

	/**
	 * @param addToOrder
	 *            the addToOrder to set
	 */
	public void setAddToOrder(boolean addToOrder) {
		this.addToOrder = addToOrder;
	}

	/**
	 * @return the orderQty
	 */
	public int getOrderQty() {
		return orderQty;
	}

	/**
	 * @param orderQty
	 *            the orderQty to set
	 */
	public void setOrderQty(int orderQty) {
		this.orderQty = orderQty;
	}
	
	/**
	 * @return the decrement
	 */
	public int getDecrement() {
		return decrement;
	}

	/**
	 * @param decrement 
	 * 		the decrement to set
	 */
	public void setDecrement(int decrement) {
		this.decrement = decrement;
	}

	/**
	 * @return the reorderReason
	 */
	public String getReorderReason() {
		return reorderReason;
	}

	/**
	 * @param reorderReason
	 *            the reorderReason to set
	 */
	public void setReorderReason(String reorderReason) {
		this.reorderReason = reorderReason;
	}

	/**
	 * @return the deliveryTimeline
	 */
	public String getDeliveryTimeline() {
		return deliveryTimeline;
	}

	/**
	 * @param deliveryTimeline
	 *            the deliveryTimeline to set
	 */
	public void setDeliveryTimeline(String deliveryTimeline) {
		this.deliveryTimeline = deliveryTimeline;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Plate [inkRef=" + inkRef + ", yr_number=" + yr_number
				+ ", orderQty=" + orderQty + ", reorderReason=" + reorderReason
				+ ", deliveryTimeline=" + deliveryTimeline + ", addToOrder="
				+ addToOrder + "]";
	}

}
