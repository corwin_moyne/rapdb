/**
 * 
 */
package ie.rapdb;

/**
 * @author Corwin
 *
 */
public class DiePerformance {
    
    private String runDate;
    private String modNo;
    private String comments;
    private int jobRevs;
    private String updateName;
    
    public DiePerformance()
    {
	runDate = new String();
	modNo = new String();
	comments = new String();
	jobRevs = 0;
	updateName = new String();
    }

    /**
     * @param runDate
     * @param modNo
     * @param comments
     * @param jobRevs
     * @param updateName
     */
    public DiePerformance(String runDate, String modNo, String comments, int jobRevs, String updateName) {
	
	this.runDate = runDate;
	this.modNo = modNo;
	this.comments = comments;
	this.jobRevs = jobRevs;
	this.updateName = updateName;
    }

    /**
     * @return the runDate
     */
    public String getRunDate() {
        return runDate;
    }

    /**
     * @param runDate the runDate to set
     */
    public void setRunDate(String runDate) {
        this.runDate = runDate;
    }

    /**
     * @return the modNo
     */
    public String getModNo() {
        return modNo;
    }

    /**
     * @param modNo the modNo to set
     */
    public void setModNo(String modNo) {
        this.modNo = modNo;
    }

    /**
     * @return the comments
     */
    public String getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * @return the jobRevs
     */
    public int getJobRevs() {
        return jobRevs;
    }

    /**
     * @param jobRevs the jobRevs to set
     */
    public void setJobRevs(int jobRevs) {
        this.jobRevs = jobRevs;
    }

    /**
     * @return the updateName
     */
    public String getUpdateName() {
        return updateName;
    }

    /**
     * @param updateName the updateName to set
     */
    public void setUpdateName(String updateName) {
        this.updateName = updateName;
    }

	@Override
	public String toString() {
		return "DiePerformance [runDate=" + runDate + ", modNo=" + modNo
				+ ", comments=" + comments + ", jobRevs=" + jobRevs
				+ ", updateName=" + updateName + "]";
	}

    
    
    
    
    
}
