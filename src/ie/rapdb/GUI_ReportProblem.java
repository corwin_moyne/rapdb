package ie.rapdb;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import javax.swing.border.LineBorder;

public class GUI_ReportProblem extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txtName;
	private JTextArea txtProblem;
	private GUI_ProgressBar frame;

//	/**
//	 * Launch the application.
//	 */
//	public static void main(String[] args) {
//		try {
//			GUI_ReportProblem dialog = new GUI_ReportProblem();
//			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//			dialog.setVisible(true);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Create the dialog.
	 */
	public GUI_ReportProblem() {
		//setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(new Color(112, 128, 144));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		
		
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JScrollPane scrollPaneTextArea = new JScrollPane();
			scrollPaneTextArea.setViewportBorder(new LineBorder(new Color(162, 182, 182), 2));
			scrollPaneTextArea.setBounds(10, 33, 414, 132);
			contentPanel.add(scrollPaneTextArea);
			
			txtProblem = new JTextArea();
			txtProblem.setWrapStyleWord(true);
			txtProblem.setLineWrap(true);
			scrollPaneTextArea.setViewportView(txtProblem);
		}
		
		JLabel lblProblem = new JLabel("Problem");
		lblProblem.setForeground(Color.WHITE);
		lblProblem.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblProblem.setBounds(10, 11, 92, 14);
		contentPanel.add(lblProblem);
		
		txtName = new JTextField();
		txtName.setBounds(309, 198, 115, 20);
		contentPanel.add(txtName);
		txtName.setColumns(10);
		
		JLabel lblName = new JLabel("Name");
		lblName.setForeground(Color.WHITE);
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblName.setBounds(309, 176, 115, 14);
		contentPanel.add(lblName);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						
						if(isvalidData())
						{
							dispose();
							runProgressBar();
							
							new Thread(new Runnable() {
								
								@Override
								public void run() {
									
									SendMail.reportProblem(txtProblem.getText(), txtName.getText());
									clearFields();
									JOptionPane.showMessageDialog(null, "Problem reported", "Success", JOptionPane.INFORMATION_MESSAGE);
									frame.dispose();
								}
							}).start();
							
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						clearFields();
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	
	/**
	 * Validate user data
	 * @return
	 */
	private boolean isvalidData()
	{
		if(txtProblem.getText().equals(""))
		{
			JOptionPane.showMessageDialog(null, "Problem cannot be blank", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		
		if(txtName.getText().equals(""))
		{
			JOptionPane.showMessageDialog(null, "Name cannot be blank", "Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
	
	/**
	 * Clears txt fields
	 */
	private void clearFields()
	{
		txtName.setText("");
		txtProblem.setText("");
	}
	
	/**
	 * Runs progress bar when ordering plates
	 */
	private void runProgressBar() {
		frame = new GUI_ProgressBar();
		frame.setSize(380, 120);
		frame.setUndecorated(true);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
