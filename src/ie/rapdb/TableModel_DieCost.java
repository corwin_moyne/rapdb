package ie.rapdb;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.AbstractTableModel;

/**
 * @author Corwin
 * 
 */
public class TableModel_DieCost extends AbstractTableModel {

	private ArrayList<DiePrice> diePrices;

	private String[] columnNames = { "Discount Type", "Manufacturer", "Die Type", "Repeat", 
			"Price", "" };

	public TableModel_DieCost(ArrayList<DiePrice> diePrices) {
		this.diePrices = diePrices;
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return diePrices.size();
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public Object getValueAt(int row, int col) {

		DiePrice d = diePrices.get(row);

		switch (col) {

		case 0:
			return d.getDiscountType();
		case 1:
			return d.getManufacturer();
		case 2:
			return d.getDieType();
		case 3:
			return d.getRepeat();
		case 4:
			return d.getPrice();
		case 5:
			JButton jButton = new JButton("Delete");
			return jButton;
		}

		return null;
	}

	@Override
	public void setValueAt(Object value, int row, int col) {
		
		DiePrice d = diePrices.get(row);

		switch (col) {

		case 0:
			d.setDiscountType((String) value);
			break;
		case 1:
			d.setManufacturer((String) value);
			break;
		case 2:
			d.setDieType((String) value);
			break;
		case 3:
			d.setRepeat((String) value);
			break;
		case 4:
			d.setPrice((double) (value));
			break;
		}
		
		DatabaseConnection.updateDiePrice(d);
	}

	// getColumnClass
	@Override
	public Class<?> getColumnClass(int col) {

		Class[] columns = new Class[] { String.class, String.class,
				String.class, String.class, Double.class, JButton.class };

		return columns[col];
	}

	@Override
	public boolean isCellEditable(int row, int col) {
		
		if(col == 5)
		{
			return false;
		}

		return true;

	}
}
