package ie.rapdb;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JList;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.border.LineBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;

public class GUI_ConfirmPlates extends JFrame {

	private JPanel contentPane;
	private JTable tblConfirmPlates;
	private JScrollPane scrollPaneList;
	private JList list;
	private ArrayList<UnconfirmedPlates> unconfirmedPlates;
	private JComboBox cboReason;
	private GUI_ProgressBar frame;
	private JButton btnOrder;
	private JPanel panel;

	/**
	 * Launch the application.
	 */
	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// GUI_ConfirmPlates frame = new GUI_ConfirmPlates();
	// frame.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	/**
	 * Create the frame.
	 */
	public GUI_ConfirmPlates() {
		// setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1280, 730);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panel = new JPanel();
		panel.setBackground(new Color(112, 128, 144));
		panel.setBounds(10, 11, 1244, 670);
		contentPane.add(panel);
		panel.setLayout(null);

		JScrollPane scrollPaneTable = new JScrollPane();
		scrollPaneTable.setBorder(new LineBorder(new Color(162, 182, 182), 3));
		scrollPaneTable.setBounds(10, 253, 1224, 233);
		panel.add(scrollPaneTable);

		tblConfirmPlates = new JTable();
		scrollPaneTable.setViewportView(tblConfirmPlates);

		scrollPaneList = new JScrollPane();
		scrollPaneList.setBorder(new LineBorder(new Color(162, 182, 182), 3));
		scrollPaneList.setBounds(10, 12, 223, 233);
		panel.add(scrollPaneList);
		list = new JList();
		list.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				displayPlates();
			}
		});
		scrollPaneList.setViewportView(list);

		btnOrder = new JButton("Confirm");
		btnOrder.setBounds(1145, 497, 89, 23);
		panel.add(btnOrder);
		
		JLabel lblSelectUnselect = new JLabel("Select / Unselect plates below and click Confirm");
		lblSelectUnselect.setForeground(Color.WHITE);
		lblSelectUnselect.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblSelectUnselect.setHorizontalAlignment(SwingConstants.CENTER);
		lblSelectUnselect.setBounds(243, 81, 991, 31);
		panel.add(lblSelectUnselect);
		btnOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				runProgressBar();

				new Thread(new Runnable() {

					@Override
					public void run() {
						
						int count = 0;
						
						for(UnconfirmedPlates plate : unconfirmedPlates)
						{
							if(plate.isConfirmed())
							{
								count++;
							}
						}

						if (count > 0) {
							
							if (unconfirmedPlates.get(0).getPlateType()
									.equals("Colour plate order")) {
								if (SendMail.sendNewColourEmail(
										unconfirmedPlates, list
												.getSelectedValue().toString())) {
									// update stock
									for (UnconfirmedPlates plate : unconfirmedPlates) {
										if (plate.isConfirmed()) {
											DatabaseConnection
													.updatePlateStock(plate);
										}
									}
								}

								DatabaseConnection
										.updateReorderedColour(unconfirmedPlates);

								DatabaseConnection
										.updateUnconfirmedPlateStatus(unconfirmedPlates);

								frame.dispose();

								JOptionPane.showMessageDialog(null,
										"Plate(s) ordered successfully",
										"Success",
										JOptionPane.INFORMATION_MESSAGE);

								displayReferences();
								displayPlates();
							} else if (unconfirmedPlates.get(0).getPlateType()
									.equals("varnish")
									|| unconfirmedPlates.get(0).getPlateType()
											.equals("antimist")
									|| unconfirmedPlates.get(0).getPlateType()
											.equals("adhesive")
									|| unconfirmedPlates.get(0).getPlateType()
											.equals("datalase")) {
								if (SendMail.sendNewCommonEmail(
										unconfirmedPlates, list
												.getSelectedValue().toString())) {
									// update stock
									for (UnconfirmedPlates plate : unconfirmedPlates) {
										if (plate.isConfirmed()) {
											DatabaseConnection
													.updateCommonPlateStock(
															plate.getDecrement(),
															plate.getId());

											DatabaseConnection
													.updateReorderedCommon(
															plate.getPlateType(),
															plate);
										}
									}
								}

								DatabaseConnection
										.updateUnconfirmedPlateStatus(unconfirmedPlates);

								frame.dispose();

								JOptionPane.showMessageDialog(null,
										"Plate(s) ordered successfully",
										"Success",
										JOptionPane.INFORMATION_MESSAGE);

								displayReferences();
								displayPlates();

							} else {
								frame.dispose();
							}
						}
						else
						{
							DatabaseConnection
							.updateUnconfirmedPlateStatus(unconfirmedPlates);
							
							frame.dispose();
							
							displayReferences();
							displayPlates();
						}
					}
				}).start();
			}
		});

		onFormLoad();

	}

	/**
	 * Fires when form loads
	 */
	private void onFormLoad() {

		displayReferences();
		displayPlates();
	}

	private void displayPlates() {

		try {
			unconfirmedPlates = DatabaseConnection.getOrderedPlates(list
					.getSelectedValue().toString());

			if (unconfirmedPlates.size() != 0) {
				tblConfirmPlates.setModel(new TableModel_ConfirmPlates(
						unconfirmedPlates));

				// Qty combo
				TableColumn qtyColumn = tblConfirmPlates.getColumnModel()
						.getColumn(4);
				String[] quantity = { "1", "2", "3", "4", "5" };
				JComboBox qtyComboBox = new JComboBox(quantity);
				qtyColumn.setCellEditor(new DefaultCellEditor(qtyComboBox));

				// Reason combo
				TableColumn reasonColumn = tblConfirmPlates.getColumnModel()
						.getColumn(5);
				cboReason = new JComboBox(DatabaseConnection.getReasons()
						.toArray());
				cboReason.setMaximumRowCount(DatabaseConnection.getReasons()
						.size());
				reasonColumn.setCellEditor(new DefaultCellEditor(cboReason));
				reasonColumn.setPreferredWidth(170);
				cboReason.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						String reason = cboReason.getSelectedItem().toString();
						int tableRow = tblConfirmPlates.getSelectedRow();

						if (reason.equals("Other")) {
							String otherReason = JOptionPane
									.showInputDialog("Enter reason");
							System.out.println("size: "
									+ unconfirmedPlates.size());
							unconfirmedPlates.get(tableRow).setReason(
									otherReason);
						}
					}
				});

				// Timeline combo
				TableColumn timelineColumn = tblConfirmPlates.getColumnModel()
						.getColumn(6);
				JComboBox timelineComboBox = new JComboBox(DatabaseConnection
						.getTimelines().toArray());
				timelineColumn.setCellEditor(new DefaultCellEditor(
						timelineComboBox));
				timelineColumn.setPreferredWidth(170);
			} else {
				btnOrder.setVisible(false);
			}

		} catch (ArrayIndexOutOfBoundsException a) {
			
		} catch (NullPointerException e) {
			btnOrder.setVisible(false);
			tblConfirmPlates.setModel(new DefaultTableModel());
		}
	}

	/**
	 * Displays references in JList
	 */
	private void displayReferences() {
		

		try {
			DefaultListModel listModel = new DefaultListModel();
			for (String ref : DatabaseConnection.getReferences()) {
				listModel.addElement(ref);
			}

			list.setModel(listModel);
			list.setSelectedIndex(0);
		} catch (Exception e) {
			//Do nothing
		}
	}

	/**
	 * Starts progress bar
	 */
	private void runProgressBar() {
		frame = new GUI_ProgressBar();
		frame.setSize(380, 120);
		frame.setUndecorated(true);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
