package ie.rapdb;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.regex.Pattern;

public class GUI_AddJobReference extends JDialog {

    private final JPanel contentPanel = new JPanel();
    private JTextField txtJobRef;
    private JTextField txtYr;
    private JComboBox cboCustomer;
    private JLabel lblProductCode;
    private static boolean complete = false;

    // /**
    // * Launch the application.
    // */
    // public static void main(String[] args) {
    // try {
    // GUI_AddJobReference dialog = new GUI_AddJobReference(ProductCode
    // productCode);
    // dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    // dialog.setVisible(true);
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    // }

    /**
     * Create the dialog.
     */
    public GUI_AddJobReference(String productCode) {

	setBounds(100, 100, 450, 300);
	getContentPane().setLayout(new BorderLayout());
	contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
	getContentPane().add(contentPanel, BorderLayout.CENTER);
	contentPanel.setLayout(null);

	lblProductCode = new JLabel(productCode);
	lblProductCode.setHorizontalAlignment(SwingConstants.TRAILING);
	lblProductCode.setBounds(19, 104, 46, 14);
	contentPanel.add(lblProductCode);

	txtJobRef = new JTextField();
	txtJobRef.setBounds(68, 101, 53, 20);
	contentPanel.add(txtJobRef);
	txtJobRef.setColumns(10);

	JLabel lblCompleteTheJob = new JLabel("Enter job Reference");
	lblCompleteTheJob.setHorizontalAlignment(SwingConstants.CENTER);
	lblCompleteTheJob.setBounds(10, 79, 147, 14);
	contentPanel.add(lblCompleteTheJob);

	cboCustomer = new JComboBox(DatabaseConnection.getCustomers().toArray());
	cboCustomer.setEditable(true);
	cboCustomer.addActionListener(new ActionListener() {
	    
	    public void actionPerformed(ActionEvent e) {

		String other = cboCustomer.getSelectedItem().toString();

		if (other.equals("-ADD NEW-")) {

		    String newCustomer = JOptionPane
			    .showInputDialog("Enter customer");

		    if (JOptionPane.showConfirmDialog(null,
			    "Would you like to add this customer to the list?",
			    "New customer", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			addNewCustomer(newCustomer);
		    }
		    
		    cboCustomer.setSelectedItem(newCustomer);
		}

	    }
	});
	cboCustomer.setBounds(167, 101, 114, 20);
	contentPanel.add(cboCustomer);

	JLabel lblSelectCustomer = new JLabel("Select Customer");
	lblSelectCustomer.setHorizontalAlignment(SwingConstants.CENTER);
	lblSelectCustomer.setBounds(167, 79, 114, 14);
	contentPanel.add(lblSelectCustomer);

	txtYr = new JTextField();
	txtYr.setBounds(317, 101, 95, 20);
	contentPanel.add(txtYr);
	txtYr.setColumns(10);

	JLabel lblVarnishYr = new JLabel("Varnish YR");
	lblVarnishYr.setHorizontalAlignment(SwingConstants.CENTER);
	lblVarnishYr.setBounds(317, 79, 95, 14);
	contentPanel.add(lblVarnishYr);
	{
	    JPanel buttonPane = new JPanel();
	    buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
	    getContentPane().add(buttonPane, BorderLayout.SOUTH);
	    {
		JButton okButton = new JButton("OK");
		okButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {

			if (isValidData()) {
			    String jobRef = lblProductCode.getText()
				    + txtJobRef.getText();
			    String jobRefToUpper = jobRef.toUpperCase();

			    String message = "You are about to add the following job reference:"
				    + "\nJob Reference: "
				    + jobRefToUpper
				    + "\nCustomer: "
				    + cboCustomer.getSelectedItem().toString()
				    + "\nVarnish YR: "
				    + txtYr.getText()
				    + "\nIs this correct?";

			    if (JOptionPane.showConfirmDialog(null, message,
				    "Add new", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				JobReference jobReference = new JobReference(
					jobRefToUpper, cboCustomer
						.getSelectedItem().toString(),
					txtYr.getText());

				DatabaseConnection.addJobReference(
					jobReference, lblProductCode.getText());

				complete = true;

				clearFields();
				dispose();
			    }
			}
		    }
		});
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);
	    }
	    {
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent arg0) {

			clearFields();
			dispose();
		    }
		});
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
	    }
	}
    }

    /**
     * Validates data entered before submitting
     * 
     * @return
     */
    private boolean isValidData() {
	if (txtJobRef.getText().equals("")) {
	    JOptionPane.showMessageDialog(null,
		    "You must complete the job reference", "Error",
		    JOptionPane.ERROR_MESSAGE);
	    return false;
	}

	if (txtYr.getText().equals("")) {
	    JOptionPane.showMessageDialog(null, "You must enter a varnish yr",
		    "Error", JOptionPane.ERROR_MESSAGE);
	    return false;
	}

	if (!txtYr.getText().matches("\\d+")) {
	    JOptionPane.showMessageDialog(null,
		    "YR number cannot contain letters", "Error",
		    JOptionPane.ERROR_MESSAGE);
	    return false;
	}

	return true;
    }

    /**
     * Clears the fields
     */
    private void clearFields() {
	txtJobRef.setText("");
	cboCustomer.setSelectedIndex(0);
	txtYr.setText("");
    }

    /**
     * Returns true if added successfully
     * 
     * @return
     */
    public static boolean isComplete() {
	return complete;
    }

    /**
     * Adds a new customer to customer table
     * 
     * @param customerName
     */
    private void addNewCustomer(String customerName) {
	DatabaseConnection.insertCustomer(customerName);
	cboCustomer.setModel(new DefaultComboBoxModel(DatabaseConnection
		.getCustomers().toArray()));
    }
}// End Class
