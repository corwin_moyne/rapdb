package ie.rapdb;

import java.util.*;

public class User {

    private String userName;
    private String password;
    private String userType;

    public User() {
	userName = "";
	password = "";
	userType = "";
    }

    public User(String userName, String password, String userType) {
	this.userName = userName;
	this.password = password;
	this.userType = userType;
    }

    public String getUserName() {
	return userName;
    }

    public void setUserName(String userName) {
	this.userName = userName;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public String getUserType() {
	return userType;
    }

    public void setUserType(String userType) {
	this.userType = userType;
    }

    // Read in passenger details
    public void readUserDetails() {
	@SuppressWarnings("resource")
	Scanner kbString = new Scanner(System.in);
	userName = kbString.nextLine();
	password = kbString.nextLine();
	userType = kbString.nextLine();

    }

    @Override
    public String toString() {
	return "User [userName=" + userName + ", password=" + password
		+ ", userType=" + userType + "]";
    }

}
