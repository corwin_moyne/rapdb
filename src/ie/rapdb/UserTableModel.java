package ie.rapdb;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.table.AbstractTableModel;

public class UserTableModel extends AbstractTableModel {

    private ArrayList<User> users;

    private String[] columnNames = { "User Name", "Password", "User Type", "" };

    public UserTableModel(ArrayList<User> users) {
	this.users = users;
    }

    @Override
    public int getColumnCount() {

	return columnNames.length;
    }

    @Override
    public String getColumnName(int col) {
	return columnNames[col];
    }

    @Override
    public int getRowCount() {
	return users.size();
    }

    // Get values for JTable
    @Override
    public Object getValueAt(int row, int col) {

	User u = users.get(row);

	switch (col) {
	case 0:
	    return u.getUserName();
	case 1:
	    return u.getPassword();
	case 2:
	    return u.getUserType();
	case 3:
	    final JButton button = new JButton("Delete");
	    return button;
	}

	return null;
    }

    // Check if cell are editable
    public boolean isCellEditable(int row, int col) {
	if (col == 3 || col == 0) {
	    return false;
	}
	return true;
    }

    // Set editable rows, columns and update database
    public void setValueAt(Object value, int row, int col) {

	// Save the Passenger object selected
	User p = users.get(row);

	// Discover which column needs editing
	switch (col) {
	case 0:
	    // Do nothing as this field cannot be changed
	    break;
	case 1:
	    // Cast value to String
	    String password = String.valueOf(value);
	    // Set name
	    p.setPassword(password);
	    break;
	case 2:
	    // Save date as String
	    String userType = String.valueOf(value);
	    // Set dob
	    p.setUserType(userType);
	    break;

	}
	DatabaseConnection.updateUser(p);

    }

    @Override
    // getColumnClass
    public Class<?> getColumnClass(int col) {
	@SuppressWarnings("rawtypes")
	Class[] columns = new Class[] { String.class, String.class,
		JComboBox.class, JButton.class };

	return columns[col];
    }
}
