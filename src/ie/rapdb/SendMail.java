/**
 * 
 */
package ie.rapdb;

import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

/**
 * @author Corwin
 * 
 */
public class SendMail {

	private static Session session;
	private static String sentFrom = "c_moyne@yahoo.com";
	private static String setReplyTo = "c_moyne@yahoo.com";

	public static void sendColourEmail(ColourOrder colourOrder) {

		prepareEmail();

		try {

			String plates = "";

			for (Plate plate : colourOrder.getPlates()) {
				plates += "\n\nYR REFERENCE: " + plate.getYr_number()
						+ "\nCOLOUR: " + plate.getInkRef() + "\nORDER QTY: "
						+ plate.getOrderQty() + "\nRE-ORDER REASON: "
						+ plate.getReorderReason() + "\nDELIVERY TIMELINE: "
						+ plate.getDeliveryTimeline();
			}

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sentFrom));
			message.setReplyTo(InternetAddress.parse(setReplyTo));

			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse("c_moyne@yahoo.com"));

			// message.setRecipients(Message.RecipientType.TO,
			// InternetAddress.parse("Dermot.McDermott@rapire.com, iain.harris@rapire.com"));

			// message.setRecipients(Message.RecipientType.TO,
			// InternetAddress.parse("Jason.crang@rapuk.com"));
			// message.addRecipients(Message.RecipientType.CC,
			// InternetAddress.parse("c_moyne@yahoo.com, iain.harris@rapire.com, platerequests@yahoo.ie, graphics@rapuk.com, Dermot.McDermott@rapire.com, colm.casserley@rapire.com", Roisin.Robinson@rapire.com, Lisa.Collum@rapire.com));
			
			message.setSubject("Colour plate order");
			message.setText("PLATE TYPE: Colour Plate" + "\nREFERENCE: "
					+ colourOrder.getReference() + plates + "\nORDERED BY: "
					+ colourOrder.getOrderName());

			Transport.send(message);

		} catch (MessagingException e) {
			JOptionPane
					.showMessageDialog(
							null,
							"There is a problem sending the email.\nMake sure you are connected to the internet.",
							"Error", JOptionPane.ERROR_MESSAGE);
		}
	}

	public static boolean sendNewColourEmail(
			ArrayList<UnconfirmedPlates> unconfirmedPlates, String reference) {

		prepareEmail();

		try {

			String plates = "";

			for (UnconfirmedPlates plate : unconfirmedPlates) {
				if(plate.isConfirmed())
				{
				plates += "\n\nYR REFERENCE: " + plate.getYr() + "\nCOLOUR: "
						+ plate.getColour() + "\nORDER QTY: " + plate.getQty()
						+ "\nRE-ORDER REASON: " + plate.getReason()
						+ "\nDELIVERY TIMELINE: " + plate.getTimeline()
						+ "\nORDER NAME: " + plate.getName();
				}
			}

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sentFrom));
			message.setReplyTo(InternetAddress.parse(setReplyTo));

//			message.setRecipients(Message.RecipientType.TO,
//					InternetAddress.parse("c_moyne@yahoo.com"));

			// message.setRecipients(Message.RecipientType.TO,
			// InternetAddress.parse("Dermot.McDermott@rapire.com, iain.harris@rapire.com"));

			 message.setRecipients(Message.RecipientType.TO,
			 InternetAddress.parse("Jason.crang@rapuk.com"));
			 message.addRecipients(Message.RecipientType.CC,
			 InternetAddress.parse("c_moyne@yahoo.com, iain.harris@rapire.com, platerequests@yahoo.ie, graphics@rapuk.com, Dermot.McDermott@rapire.com, colm.casserley@rapire.com, Roisin.Robinson@rapire.com, Lisa.Collum@rapire.com"));
			
			message.setSubject("Colour plate order");
			message.setText("PLATE TYPE: Colour Plate" + "\nREFERENCE: "
					+ reference + plates);

			Transport.send(message);
			
			

		} catch (MessagingException e) {
			JOptionPane
					.showMessageDialog(
							null,
							"There is a problem sending the email.\nMake sure you are connected to the internet.",
							"Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}

	public static void sendCommonEmail(CommonOrder commonOrder) {

		prepareEmail();

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sentFrom));
			message.setReplyTo(InternetAddress.parse(setReplyTo));

			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse("c_moyne@yahoo.com"));

			// message.setRecipients(Message.RecipientType.TO,
			// InternetAddress.parse("Dermot.McDermott@rapire.com, iain.harris@rapire.com"));

			// message.setRecipients(Message.RecipientType.TO,
			// InternetAddress.parse("Jason.crang@rapuk.com"));
			// message.addRecipients(Message.RecipientType.CC,
			// InternetAddress.parse("c_moyne@yahoo.com, iain.harris@rapire.com, platerequests@yahoo.ie, graphics@rapuk.com, Dermot.McDermott@rapire.com, colm.casserley@rapire.com", Roisin.Robinson@rapire.com, Lisa.Collum@rapire.com));
			
			message.setSubject(commonOrder.getOrderType() + " plate order");

			message.setText("PLATE TYPE: " + commonOrder.getOrderType()
					+ "\nREFERENCE: " + commonOrder.getReference()
					+ "\n\nYR REFERENCE: "
					+ commonOrder.getPlate().getYr_number() + "\nORDER QTY: "
					+ commonOrder.getPlate().getOrderQty()
					+ "\nRE-ORDER REASON: "
					+ commonOrder.getPlate().getReorderReason()
					+ "\nDELIVERY TIMELINE: "
					+ commonOrder.getPlate().getDeliveryTimeline()
					+ "\nORDERED BY: " + commonOrder.getOrderName());

			Transport.send(message);

		} catch (MessagingException e) {
			JOptionPane
					.showMessageDialog(
							null,
							"There is a problem sending the email.\nMake sure you are connected to the internet.",
							"Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public static boolean sendNewCommonEmail(
			ArrayList<UnconfirmedPlates> unconfirmedPlates, String reference) {

		prepareEmail();

		try {

			String plates = "";

			for (UnconfirmedPlates plate : unconfirmedPlates) {
				if(plate.isConfirmed())
				{
				plates += "\n\nPLATE TYPE: " + plate.getPlateType() +  
						"\nYR REFERENCE: " + plate.getYr() 
						+ "\nORDER QTY: " + plate.getQty()
						+ "\nRE-ORDER REASON: " + plate.getReason()
						+ "\nDELIVERY TIMELINE: " + plate.getTimeline()
						+ "\nORDER NAME: " + plate.getName();
				}
			}

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sentFrom));
			message.setReplyTo(InternetAddress.parse(setReplyTo));

//			message.setRecipients(Message.RecipientType.TO,
//					InternetAddress.parse("c_moyne@yahoo.com"));

			// message.setRecipients(Message.RecipientType.TO,
			// InternetAddress.parse("Dermot.McDermott@rapire.com, iain.harris@rapire.com"));

			 message.setRecipients(Message.RecipientType.TO,
			 InternetAddress.parse("Jason.crang@rapuk.com"));
			 message.addRecipients(Message.RecipientType.CC,
					 InternetAddress.parse("c_moyne@yahoo.com, iain.harris@rapire.com, platerequests@yahoo.ie, graphics@rapuk.com, Dermot.McDermott@rapire.com, colm.casserley@rapire.com, Roisin.Robinson@rapire.com, Lisa.Collum@rapire.com"));
			
			message.setSubject("Common plate order");
			message.setText("\nREFERENCE: "
					+ reference + plates);

			Transport.send(message);
			
		} catch (MessagingException e) {
			JOptionPane
					.showMessageDialog(
							null,
							"There is a problem sending the email.\nMake sure you are connected to the internet.",
							"Error", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		return true;
	}
	
	public static boolean sendPlateNotificationEmail() {

		prepareEmail();

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sentFrom));
			message.setReplyTo(InternetAddress.parse(setReplyTo));

//			message.setRecipients(Message.RecipientType.TO,
//					InternetAddress.parse("c_moyne@yahoo.com"));

			 message.setRecipients(Message.RecipientType.TO,
			 InternetAddress.parse("Dermot.McDermott@rapire.com, iain.harris@rapire.com"));

			// message.setRecipients(Message.RecipientType.TO,
			// InternetAddress.parse("Jason.crang@rapuk.com"));
			// message.addRecipients(Message.RecipientType.CC,
			// InternetAddress.parse("c_moyne@yahoo.com, iain.harris@rapire.com, platerequests@yahoo.ie, graphics@rapuk.com, Dermot.McDermott@rapire.com, colm.casserley@rapire.com", Roisin.Robinson@rapire.com, Lisa.Collum@rapire.com));
			
			message.setSubject("Plate approval notification");
			message.setText("You have a new plate request that needs approved.");

			Transport.send(message);

		} catch (MessagingException e) {
			JOptionPane
					.showMessageDialog(
							null,
							"There is a problem sending the email.\nMake sure you are connected to the internet.",
							"Error", JOptionPane.ERROR_MESSAGE);
			
			return false;
		}
		return true;
	}

	public static void sendRemovedDieEmail(String dieType, String dieCode,
			String mod, String revs, String comments, String name) {
		prepareEmail();

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sentFrom));
			message.setReplyTo(InternetAddress.parse(setReplyTo));
//			message.setRecipients(Message.RecipientType.TO,
//					InternetAddress.parse("c_moyne@yahoo.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse("darren.kibbler@rapire.com"));
			message.addRecipients(Message.RecipientType.CC,
					InternetAddress.parse("c_moyne@yahoo.com"));
			message.setSubject("Die removed: " + dieType + " DC" + dieCode
					+ " " + mod);

			message.setText("Comments: " + comments + "\nCurrent revs: " + revs
					+ "\nName: " + name);

			Transport.send(message);

		} catch (MessagingException e) {
			// Do nothing
		}
	}

	public static void sendDieUpdateEmail(int milestone, String dieType,
			String dieCode, String mod, String revs, String comments,
			String name) {
		prepareEmail();

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sentFrom));
			message.setReplyTo(InternetAddress.parse("iain.harris@rapire.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse("darren.kibbler@rapire.com"));
			message.addRecipients(Message.RecipientType.CC,
					InternetAddress.parse("c_moyne@yahoo.com"));
			message.setSubject(milestone + " revolutions reached: " + dieType
					+ " DC" + dieCode + " " + mod);

			message.setText("Comments: " + comments + "\nCurrent revs: " + revs
					+ "\nName: " + name);

			Transport.send(message);

		} catch (MessagingException e) {
			// Do nothing
		}
	}

	public static void sendPoorPerformanceEmail(String dieType, String dieCode,
			String mod, String revs, String comments, String name) {

		prepareEmail();

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sentFrom));
			message.setReplyTo(InternetAddress.parse("iain.harris@rapire.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse("darren.kibbler@rapire.com"));
			message.addRecipients(Message.RecipientType.CC,
					InternetAddress.parse("c_moyne@yahoo.com"));
			message.setSubject("Poor die performance alert: " + dieType + " DC"
					+ dieCode + " " + mod);

			message.setText("Comments: " + comments + "\nCurrent revs: " + revs
					+ "\nName: " + name);

			Transport.send(message);

		} catch (MessagingException e) {
			// Do nothing
		}
	}

	public static void reportProblem(String problem, String name) {

		prepareEmail();

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sentFrom));
			message.setReplyTo(InternetAddress.parse(setReplyTo));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse("c_moyne@yahoo.com"));
			message.setSubject("RAP system problem report");

			message.setText("Problem: " + problem + "\nName: " + name);

			Transport.send(message);

		} catch (MessagingException e) {
			// Do nothing
		}
	}

	public static void sendNewDieEmail(String dieType, String dieCode,
			String repeat, String mod, String name) {
		prepareEmail();

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sentFrom));
			message.setReplyTo(InternetAddress.parse(setReplyTo));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse("darren.kibbler@rapire.com"));
//			message.setRecipients(Message.RecipientType.TO,
//					InternetAddress.parse("c_moyne@yahoo.com"));
			message.addRecipients(Message.RecipientType.CC,
					InternetAddress.parse("c_moyne@yahoo.com"));
			message.setSubject("Die not found on system received and checked: "
					+ dieType + " DC" + dieCode + " MOD:" + mod + " Repeat:"
					+ repeat);

			message.setText("Name: " + name);

			Transport.send(message);

			JOptionPane.showMessageDialog(null, "Email sent", "Success",
					JOptionPane.INFORMATION_MESSAGE);

		} catch (MessagingException e) {
			// Do nothing
		}
	}

	private static void prepareEmail() {

		Properties props = new Properties();

		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(
								"rapidactionpackagingireland@gmail.com",
								"Sandwich11");
					}
				});
	}

}
