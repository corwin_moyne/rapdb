package ie.rapdb;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

import java.awt.event.WindowAdapter;
import java.awt.Color;

public class Splash extends JFrame {

	private JPanel contentPane;
	ActionListener openGUI_HomeScreen;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Splash frame = new Splash();
					frame.setLocationRelativeTo(null);
					frame.setUndecorated(true);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	Timer timer;
	/**
	 * Create the frame.
	 */
	public Splash() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent arg0) {
				
				timer.start();
			}
		});

		timer = new Timer(6000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				dispose();
				openGUI_HomeScreen();
			}
		});

//		Timer timer = new Timer(5000, openGUI_HomeScreen);
//		timer.start();

		// addWindowListener(new WindowAdapter() {
		// @Override
		// public void windowActivated(WindowEvent arg0) {
		//
		// Timer timer = new Timer(5000, closeWindowActionListener);
		// timer.start();
		// }
		// @Override
		// public void windowDeactivated(WindowEvent e) {
		//
		// openGUI_HomeScreen();
		// }
		// });

//		openGUI_HomeScreen = new ActionListener() {
//			@Override
//			public void actionPerformed(ActionEvent arg0) {
//				openGUI_HomeScreen();
//			}
//		};

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Splash.class
				.getResource("/image/splash.gif")));
		lblNewLabel.setBounds(8, 0, 684, 289);
		contentPane.add(lblNewLabel);
	}

	private void openGUI_HomeScreen() {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new DatabaseConnection();
					UIManager
							.setLookAndFeel("com.jtattoo.plaf.mint.MintLookAndFeel");

					GUI_HomeScreen frame = new GUI_HomeScreen();
					frame.setTitle("Home");
					frame.setSize(1280, 730);
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame.setLocationRelativeTo(null);
					frame.setVisible(true);
					
					timer.stop();
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
}
