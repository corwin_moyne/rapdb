package ie.rapdb;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;

@SuppressWarnings("serial")
public class UserButtonRenderer extends JButton implements TableCellRenderer {
	
	//public class userButtonRenderer() 
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
	    boolean isSelected, boolean hasFocus, int row, int col) {
	
	JButton button = (JButton) (value);
	
	//button.setBackground(isSelected?table.getSelectionBackground():table.getBackground());
	
	return button;
    }
	

}
