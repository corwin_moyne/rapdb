/**
 * 
 */
package ie.rapdb;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.table.AbstractTableModel;

/**
 * @author Corwin
 *
 */
public class TableModel_ProductCodes extends AbstractTableModel{
    
    private ArrayList<ProductCode> productCodes;

    private String[] columnNames = { "Product Code", "Size", "Internal DC",
	    "External DC", "Antimist YR", "Adhesive YR", "Datalase YR", "" };

    public TableModel_ProductCodes(ArrayList<ProductCode> productCodes) {
	this.productCodes = productCodes;
    }

    @Override
    public int getColumnCount() {
	return columnNames.length;
    }

    @Override
    public int getRowCount() {
	return productCodes.size();
    }

    @Override
    public String getColumnName(int col) {
	return columnNames[col];
    }

    @Override
    public Object getValueAt(int row, int col) {

	ProductCode p = productCodes.get(row);

	switch (col) {

	case 0:
	    return p.getProductCode();
	case 1:
	    return p.getSize();
	case 2:
	    return p.getIntDC();
	case 3:
	    return p.getExtDC();
	case 4:
	    return p.getAntimistYr();
	case 5:
	    return p.getAdhesiveYr();
	case 6:
	    return p.getDatalaseYr();
	case 7:
		JButton jbutton = new JButton("Delete");
		return jbutton;
	}

	return null;
    }

    public void setValueAt(Object value, int row, int col) {
	
	ProductCode productCode = productCodes.get(row);
	
	switch (col) {
	case 0:
		productCode.setProductCode((String) value);
		break;
	case 1:
	    productCode.setSize((String) value);
	    break;
	case 2:
	    productCode.setIntDC((String) value);
	    break;
	case 3:
	    productCode.setExtDC((String) value);
	    break;
	case 4:
	    productCode.setAntimistYr((String) value);
	    break;
	case 5:
	    productCode.setAdhesiveYr((String) value);
	    break;
	case 6:
	    productCode.setDatalaseYr((String) value);
	    break;
	}
	
	DatabaseConnection.updateProductCode(productCode);
    }

    // getColumnClass
    @Override
    public Class<?> getColumnClass(int col) {

	Class[] columns = new Class[] { String.class, String.class,
		JComboBox.class, JComboBox.class, String.class,
		String.class, String.class, String.class };

	return columns[col];
    }

    @Override
    public boolean isCellEditable(int row, int col) {

	if (col == 0) {
	    return false;
	}
	return true;

    }

}
