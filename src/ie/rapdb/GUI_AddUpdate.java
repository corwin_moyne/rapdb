package ie.rapdb;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;
import java.awt.Color;

import javax.swing.border.LineBorder;

import java.awt.GridLayout;

import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.ImageIcon;

import org.omg.CORBA.DATA_CONVERSION;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.event.CaretListener;
import javax.swing.event.CaretEvent;

public class GUI_AddUpdate extends JFrame {

	private JPanel contentPane;
	private JTable tableProductCodes;
	private ArrayList<ProductCode> productCodes;
	private ArrayList<JobReference> jobReferences;
	private ArrayList<PlateDetail> plateDetails;
	private JButton btnAddProductCode;
	private JButton btnAddDcNumbers;
	private JPanel panelJobRef;
	private JScrollPane scrollPaneJobRef;
	private JTable tableJobRef;
	private JComboBox cboCustomerComboBox, cboColourComboBox, cboSizeComboBox,
			cboStock, cboYrNumber, cboAntimistYr, cboAdhesiveYr, cboDatalaseYr,
			cboVarnishYr;
	private JPanel panelColours;
	private JScrollPane scrollPaneColours;
	private JTable tableColours;
	private String productCode;
	private String originalCustomer;
	private String originalColour;
	private String originalSize;
	private JButton btnAddInkRef;
	private String jobRef;
	private JButton btnAdmin;
	private JLabel lblProductCode, lblJobReference;
	private String userType;
	private JPanel panelBackground;
	private int oldStock = 0, newStock = 0;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private CommonPlate commonPlate = null;
	private JTextField txtYr;
	private JTextField txtPlateRef;
	private JLabel lblStock;
	private JTextField txtStock;
	private String commonPlateString;
	private JLabel lblPlusCommon;
	private JLabel lblMinusCommon;
	private JButton btnPlates;

	// /**
	// * Launch the application.
	// */
	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// GUI_AddUpdate frame = new GUI_AddUpdate();
	// frame.setVisible(true);
	// frame.setSize(1024, 730);
	// frame.setLocationRelativeTo(null);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	/**
	 * Create the frame.
	 */
	public GUI_AddUpdate(String userType) {
		setResizable(false);
		this.userType = userType;
		setSize(new Dimension(1280, 730));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panelBackground = new JPanel();
		panelBackground.setBackground(new Color(112, 128, 144));
		panelBackground.setBounds(10, 11, 1254, 681);
		contentPane.add(panelBackground);
		panelBackground.setLayout(null);

		lblJobReference = new JLabel("Job Reference");
		lblJobReference.setBounds(649, 374, 567, 23);
		panelBackground.add(lblJobReference);
		lblJobReference.setForeground(Color.WHITE);
		lblJobReference.setVerticalAlignment(SwingConstants.BOTTOM);
		lblJobReference.setHorizontalAlignment(SwingConstants.LEFT);
		lblJobReference.setFont(new Font("Tahoma", Font.BOLD, 16));

		lblProductCode = new JLabel("Product Code");
		lblProductCode.setBounds(20, 26, 122, 31);
		panelBackground.add(lblProductCode);
		lblProductCode.setForeground(Color.WHITE);
		lblProductCode.setVerticalAlignment(SwingConstants.BOTTOM);
		lblProductCode.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblProductCode.setHorizontalAlignment(SwingConstants.LEFT);

		JPanel panelProductCodes = new JPanel();
		panelProductCodes
				.setBorder(new LineBorder(new Color(162, 182, 182), 3));
		panelProductCodes.setBounds(10, 58, 996, 305);
		panelBackground.add(panelProductCodes);
		panelProductCodes.setLayout(null);

		JScrollPane scrollPaneProductCode = new JScrollPane();
		scrollPaneProductCode.setBounds(10, 11, 976, 249);
		panelProductCodes.add(scrollPaneProductCode);

		tableProductCodes = new JTable();
		tableProductCodes.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {

				fireProductCodeChange();
			}
		});
		tableProductCodes.setFillsViewportHeight(true);
		tableProductCodes.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {

				try {
					originalSize = productCodes.get(
							tableProductCodes.getSelectedRow()).getSize();

				} catch (ArrayIndexOutOfBoundsException a) {
					// Do nothing
				}
			}
		});
		tableProductCodes.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				fireProductCodeChange();
			}
		});

		scrollPaneProductCode.setViewportView(tableProductCodes);

		btnAddProductCode = new JButton("Add Product Code");
		btnAddProductCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String productCode = JOptionPane
						.showInputDialog("Enter new product code");

				if (productCode != null) {

					DatabaseConnection.insertProductCode(productCode);

					setupProductCodeTable();
				}
			}
		});
		btnAddProductCode.setBounds(850, 271, 119, 23);
		panelProductCodes.add(btnAddProductCode);

		btnAddDcNumbers = new JButton("Add DC Numbers");
		btnAddDcNumbers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				openGUI_InsertNewDC();
			}
		});
		btnAddDcNumbers.setBounds(719, 271, 119, 23);
		panelProductCodes.add(btnAddDcNumbers);

		panelJobRef = new JPanel();
		panelJobRef.setBorder(new LineBorder(new Color(162, 182, 182), 3));
		panelJobRef.setBounds(10, 400, 606, 259);
		panelBackground.add(panelJobRef);
		panelJobRef.setLayout(null);

		scrollPaneJobRef = new JScrollPane();
		scrollPaneJobRef.setBounds(10, 11, 586, 202);
		panelJobRef.add(scrollPaneJobRef);

		tableJobRef = new JTable();
		tableJobRef.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				fireJobRefChange();
			}
		});
		tableJobRef.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {

				try {
					originalCustomer = jobReferences.get(
							tableJobRef.getSelectedRow()).getCustomer();
				} catch (ArrayIndexOutOfBoundsException a) {
					// Do nothing
				}
			}
		});
		tableJobRef.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				fireJobRefChange();
			}
		});
		scrollPaneJobRef.setViewportView(tableJobRef);

		JButton btnAddNewJob = new JButton("Add New Job");
		btnAddNewJob.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (productCode != null) {
					openGUI_AddJobReference(productCode);
				} else {
					String pc = (String) tableProductCodes.getValueAt(0, 0);
					openGUI_AddJobReference(pc);
				}
			}
		});
		btnAddNewJob.setBounds(456, 224, 140, 23);
		panelJobRef.add(btnAddNewJob);

		panelColours = new JPanel();
		panelColours.setBorder(new LineBorder(new Color(162, 182, 182), 3));
		panelColours.setBounds(638, 400, 606, 259);
		panelBackground.add(panelColours);
		panelColours.setLayout(null);

		scrollPaneColours = new JScrollPane();
		scrollPaneColours.setBounds(10, 11, 586, 202);
		panelColours.add(scrollPaneColours);

		tableColours = new JTable();
		tableColours.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				if (tableColours.getSelectedColumn() == 3) {
					// Add new plate_checkin
					if (JOptionPane.showConfirmDialog(null,
							"Are you adding a new plate?", "New?",
							JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
						try {
							String name = JOptionPane.showInputDialog(
									"Enter checkin name").toUpperCase();

							DatabaseConnection.insertPlateCheckin(
									name,
									plateDetails.get(
											tableColours.getSelectedRow())
											.getId(), true);
						} catch (NullPointerException n) {
							// Do nothing
						}

					}

					// Increment stock
					DatabaseConnection.incrementColourPlateStock(plateDetails
							.get(tableColours.getSelectedRow()).getId());

					if (tableJobRef.getRowCount() > 0) {
						displayPlateDetail(lblJobReference.getText());
					} else {
						tableColours.setModel(new DefaultTableModel());
					}
				} else if (tableColours.getSelectedColumn() == 4) {

					int value = (int) tableColours.getValueAt(
							tableColours.getSelectedRow(), 2);

					if (value > 0) {
						// Decrement stock
						DatabaseConnection
								.decrementColourPlateStock(plateDetails.get(
										tableColours.getSelectedRow()).getId());

						if (tableJobRef.getRowCount() > 0) {
							displayPlateDetail(lblJobReference.getText());
						} else {

							tableColours.setModel(new DefaultTableModel());
						}
					}
				} else if (tableColours.getSelectedColumn() == 5) {
					// Delete row
					if (JOptionPane
							.showConfirmDialog(
									null,
									"Are you sure you want to delete this ink reference?",
									"?", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
						DatabaseConnection.deletePlate(plateDetails.get(
								tableColours.getSelectedRow()).getId());
						displayPlateDetail(jobRef);
					}

				}
			}
		});
		tableColours.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {

				try {
					originalColour = plateDetails.get(
							tableColours.getSelectedRow()).getInkRefString();
				} catch (ArrayIndexOutOfBoundsException a) {
					// Do nothing
				}
			}
		});
		scrollPaneColours.setViewportView(tableColours);

		btnAddInkRef = new JButton("Add Ink Ref");
		btnAddInkRef.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				openGUI_AddNewColour(jobRef);
			}
		});
		btnAddInkRef.setBounds(456, 225, 140, 23);
		panelColours.add(btnAddInkRef);

		btnAdmin = new JButton("Admin");
		btnAdmin.setBounds(1155, 8, 89, 23);
		panelBackground.add(btnAdmin);

		JPanel panelCommonPlates = new JPanel();
		panelCommonPlates
				.setBorder(new LineBorder(new Color(162, 182, 182), 3));
		panelCommonPlates.setBounds(1016, 58, 228, 305);
		panelBackground.add(panelCommonPlates);
		panelCommonPlates.setLayout(null);

		JPanel panelRadios = new JPanel();
		panelRadios.setBorder(new LineBorder(new Color(0, 0, 0)));
		panelRadios.setBounds(10, 11, 208, 40);
		panelCommonPlates.add(panelRadios);
		panelRadios.setLayout(new GridLayout(2, 2, 0, 0));

		JRadioButton rdbtnVarnish = new JRadioButton("Varnish");
		rdbtnVarnish.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				getCommonYr("varnish");
				commonPlateString = "varnish";
				getCommonPlate(commonPlateString, cboYrNumber.getItemAt(1)
						.toString());
			}
		});
		buttonGroup.add(rdbtnVarnish);
		rdbtnVarnish.setSelected(true);
		rdbtnVarnish.setHorizontalAlignment(SwingConstants.LEFT);
		panelRadios.add(rdbtnVarnish);

		JRadioButton rdbtnAntimist = new JRadioButton("Antimist");
		rdbtnAntimist.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				getCommonYr("antimist");
				commonPlateString = "antimist";
				getCommonPlate(commonPlateString, cboYrNumber.getItemAt(1)
						.toString());
			}
		});
		buttonGroup.add(rdbtnAntimist);
		rdbtnAntimist.setHorizontalAlignment(SwingConstants.LEFT);
		panelRadios.add(rdbtnAntimist);

		JRadioButton rdbtnAdhesive = new JRadioButton("Adhesive");
		rdbtnAdhesive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getCommonYr("adhesive");
				commonPlateString = "adhesive";
				getCommonPlate(commonPlateString, cboYrNumber.getItemAt(1)
						.toString());
			}
		});
		buttonGroup.add(rdbtnAdhesive);
		rdbtnAdhesive.setHorizontalAlignment(SwingConstants.LEFT);
		panelRadios.add(rdbtnAdhesive);

		JRadioButton rdbtnDatalase = new JRadioButton("Datalase");
		rdbtnDatalase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				getCommonYr("datalase");
				commonPlateString = "datalase";
				getCommonPlate(commonPlateString, cboYrNumber.getItemAt(1)
						.toString());
			}
		});
		buttonGroup.add(rdbtnDatalase);
		rdbtnDatalase.setHorizontalAlignment(SwingConstants.LEFT);
		panelRadios.add(rdbtnDatalase);

		txtYr = new JTextField();
		txtYr.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				fireCommonUpdate();

				cboYrNumber.setModel(new DefaultComboBoxModel(
						DatabaseConnection.getCommonYrFromCommonTable(
								commonPlateString).toArray()));

				cboYrNumber.setEditable(true);

				cboYrNumber.setSelectedItem(txtYr.getText());

				cboYrNumber.setEditable(false);

				getCommonYr(commonPlateString);

				getCommonPlate(commonPlateString, txtYr.getText());
			}
		});
		txtYr.setBounds(132, 101, 86, 20);
		panelCommonPlates.add(txtYr);
		txtYr.setColumns(10);

		JLabel lblNewLabel = new JLabel("Yr Number");
		lblNewLabel.setForeground(new Color(112, 128, 144));
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel.setBounds(132, 84, 86, 14);
		panelCommonPlates.add(lblNewLabel);

		JLabel lblPlateReference = new JLabel("Reference");
		lblPlateReference.setForeground(new Color(112, 128, 144));
		lblPlateReference.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPlateReference.setBounds(10, 132, 100, 14);
		panelCommonPlates.add(lblPlateReference);

		txtPlateRef = new JTextField();
		txtPlateRef.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {

				fireCommonUpdate();
			}
		});
		txtPlateRef.setColumns(10);
		txtPlateRef.setBounds(10, 149, 100, 20);
		panelCommonPlates.add(txtPlateRef);

		lblStock = new JLabel("Stock\r\n");
		lblStock.setForeground(new Color(112, 128, 144));
		lblStock.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblStock.setBounds(132, 132, 86, 14);
		panelCommonPlates.add(lblStock);

		txtStock = new JTextField();
		txtStock.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				fireCommonUpdate();
			}
		});
		txtStock.setColumns(10);
		txtStock.setBounds(132, 149, 86, 20);
		panelCommonPlates.add(txtStock);

		lblPlusCommon = new JLabel("");
		lblPlusCommon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				// Add new plate_checkin
				if (JOptionPane.showConfirmDialog(null,
						"Are you adding a new plate?", "New?",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					try {
						String name = JOptionPane.showInputDialog(
								"Enter checkin name").toUpperCase();

						DatabaseConnection.insertCommonPlateCheckin(name,
								commonPlate.getId(), true);
					} catch (NullPointerException n) {
						// Do nothing
					}

				}

				// Increment stock
				DatabaseConnection.incrementCommonPlateStock(commonPlate
						.getId());
				getCommonPlate(commonPlateString, cboYrNumber.getSelectedItem()
						.toString());
			}
		});
		lblPlusCommon.setBackground(Color.WHITE);
		lblPlusCommon.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlusCommon.setIcon(new ImageIcon(GUI_AddUpdate.class
				.getResource("/image/plusWhite.png")));
		lblPlusCommon.setBounds(132, 180, 40, 20);
		panelCommonPlates.add(lblPlusCommon);

		lblMinusCommon = new JLabel("");
		lblMinusCommon.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {

				if (Integer.valueOf(txtStock.getText()) > 0) {
					
					// Decrement stock
					DatabaseConnection.decrementCommonPlateStock(commonPlate
							.getId());
					getCommonPlate(commonPlateString, cboYrNumber.getSelectedItem()
							.toString());
				}		
			}
		});
		lblMinusCommon.setIcon(new ImageIcon(GUI_AddUpdate.class
				.getResource("/image/minusWhite.png")));
		lblMinusCommon.setHorizontalAlignment(SwingConstants.CENTER);
		lblMinusCommon.setForeground(Color.WHITE);
		lblMinusCommon.setBackground(Color.WHITE);
		lblMinusCommon.setBounds(178, 180, 40, 20);
		panelCommonPlates.add(lblMinusCommon);

		cboYrNumber = new JComboBox();
		cboYrNumber.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					if (cboYrNumber.getSelectedItem().equals("-ADD NEW-")) {

						String yr = JOptionPane
								.showInputDialog("Enter new yr number");

						addNewYr(commonPlateString, yr);

						cboYrNumber.setModel(new DefaultComboBoxModel(
								DatabaseConnection.getCommonYrFromCommonTable(
										commonPlateString).toArray()));

						cboYrNumber.setEditable(true);

						cboYrNumber.setSelectedItem(yr);

						cboYrNumber.setEditable(false);

						getCommonYr(commonPlateString);

						getCommonPlate(commonPlateString, yr);

					} else {

						getCommonPlate(commonPlateString, cboYrNumber
								.getSelectedItem().toString());

					}
				} catch (NullPointerException n) {
					// TODO Auto-generated catch block
					n.printStackTrace();
				}
			}
		});
		cboYrNumber.setBounds(10, 101, 100, 20);
		panelCommonPlates.add(cboYrNumber);
		
		btnPlates = new JButton("Plates");
		btnPlates.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				openGUI_ConfirmPlates();
			}
		});
		btnPlates.setBounds(1155, 8, 89, 23);
		btnPlates.setVisible(false);
		panelBackground.add(btnPlates);
		btnAdmin.setVisible(false);
		btnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				openGUI_Administrator();
			}
		});

		onFormLoad();
	}

	/**
	 * Runs on form load
	 */
	private void onFormLoad() {
		setupProductCodeTable();
		productCode = (String) tableProductCodes.getValueAt(0, 0);
		displayJobReferences(productCode);
		if (tableJobRef.getRowCount() > 0) {

			jobRef = (String) tableJobRef.getValueAt(0, 0);

			displayPlateDetail(jobRef);

			lblProductCode.setText((String) tableProductCodes.getValueAt(0, 0));
			lblJobReference.setText(jobRef);

			if (userType.equals("admin")) {
				btnAdmin.setVisible(true);
			}
			else if(userType.equals("manager"))
			{
				btnPlates.setVisible(true);
			}
		}
		commonPlateString = "varnish";
		getCommonYr("varnish");

		try {
			getCommonPlate(commonPlateString, cboYrNumber.getItemAt(1).toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}

	/**
	 * Sets up the product code table
	 */
	private void setupProductCodeTable() {

		productCodes = DatabaseConnection.getProducts();
		tableProductCodes.setModel(new TableModel_ProductCodes(productCodes));
		tableProductCodes.getColumn("").setCellRenderer(
				new UserButtonRenderer());

		cboSizeComboBox = new JComboBox(DatabaseConnection.getSizes().toArray());
		cboSizeComboBox.setEditable(true);
		TableColumn sizeColumn = tableProductCodes.getColumnModel()
				.getColumn(1);
		sizeColumn.setCellEditor(new DefaultCellEditor(cboSizeComboBox));

		cboSizeComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {

					String other = cboSizeComboBox.getSelectedItem().toString();

					if (other.equals("-ADD NEW-")) {

						String newSize = JOptionPane
								.showInputDialog("Enter size");

						if (newSize != null) {

							if (JOptionPane
									.showConfirmDialog(
											null,
											"Would you like to add this size to the list?",
											"New size",
											JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

								addNewSize(newSize);

							}
							cboSizeComboBox.setSelectedItem(newSize);
						} else {
							cboSizeComboBox.setSelectedItem(originalSize);
						}

					}
				} catch (NullPointerException n) {

				}

			}
		});

		// Internal DC
		JComboBox cboIntDComboBox = new JComboBox(DatabaseConnection
				.getIntCodes().toArray());
		TableColumn intDcColumn = tableProductCodes.getColumnModel().getColumn(
				2);
		intDcColumn.setCellEditor(new DefaultCellEditor(cboIntDComboBox));

		// External DC
		JComboBox cboExtDComboBox = new JComboBox(DatabaseConnection
				.getExtCodes().toArray());
		TableColumn extDcColumn = tableProductCodes.getColumnModel().getColumn(
				3);
		extDcColumn.setCellEditor(new DefaultCellEditor(cboExtDComboBox));

		// Antimist
		cboAntimistYr = new JComboBox(DatabaseConnection
				.getCommonYrFromCommonTable("antimist").toArray());
		TableColumn antimistYrColumn = tableProductCodes.getColumnModel()
				.getColumn(4);
		antimistYrColumn.setCellEditor(new DefaultCellEditor(cboAntimistYr));
		cboAntimistYr.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				try {

					if (cboAntimistYr.getSelectedItem().equals("-ADD NEW-")) {
						String yr = JOptionPane
								.showInputDialog("Enter new yr number");

						if (yr != null) {
							addNewYr("antimist", yr);

							cboAntimistYr.setModel(new DefaultComboBoxModel(
									DatabaseConnection
											.getCommonYrFromCommonTable(
													"antimist").toArray()));

							cboAntimistYr.setEditable(true);

							cboAntimistYr.setSelectedItem(yr);

							cboAntimistYr.setEditable(false);

							getCommonYr("antimist");
						}
					}

				} catch (NullPointerException n) {

				}

			}
		});

		// Adhesive
		cboAdhesiveYr = new JComboBox(DatabaseConnection
				.getCommonYrFromCommonTable("adhesive").toArray());
		TableColumn adhesiveYrColumn = tableProductCodes.getColumnModel()
				.getColumn(5);
		adhesiveYrColumn.setCellEditor(new DefaultCellEditor(cboAdhesiveYr));
		cboAdhesiveYr.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				try {

					if (cboAdhesiveYr.getSelectedItem().equals("-ADD NEW-")) {

						String yr = JOptionPane
								.showInputDialog("Enter new yr number");

						if (yr != null) {
							addNewYr("adhesive", yr);

							cboAdhesiveYr.setModel(new DefaultComboBoxModel(
									DatabaseConnection
											.getCommonYrFromCommonTable(
													"adhesive").toArray()));

							cboAdhesiveYr.setEditable(true);

							cboAdhesiveYr.setSelectedItem(yr);

							cboAdhesiveYr.setEditable(false);
						}
					}

					getCommonYr("adhesive");
				} catch (NullPointerException n) {

				}
			}
		});

		// Datalase
		cboDatalaseYr = new JComboBox(DatabaseConnection
				.getCommonYrFromCommonTable("datalase").toArray());
		TableColumn datalaseYrColumn = tableProductCodes.getColumnModel()
				.getColumn(6);
		datalaseYrColumn.setCellEditor(new DefaultCellEditor(cboDatalaseYr));
		cboDatalaseYr.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (cboDatalaseYr.getSelectedItem().equals("-ADD NEW-")) {

						String yr = JOptionPane
								.showInputDialog("Enter new yr number");

						if (yr != null) {
							addNewYr("datalase", yr);

							cboDatalaseYr.setModel(new DefaultComboBoxModel(
									DatabaseConnection
											.getCommonYrFromCommonTable(
													"datalase").toArray()));

							cboDatalaseYr.setEditable(true);

							cboDatalaseYr.setSelectedItem(yr);

							cboDatalaseYr.setEditable(false);

							getCommonYr("datalase");
						}
					}

				} catch (NullPointerException n) {

				}
			}
		});
	}

	/**
	 * Opens GUI_InsertNewDC so user can add new dc numbers
	 */
	private void openGUI_InsertNewDC() {

		GUI_InsertNewDC dialog = new GUI_InsertNewDC();
		dialog.setTitle("Enter die information");
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setLocationRelativeTo(null);
		dialog.setModal(true);
		dialog.setVisible(true);

		setupProductCodeTable();
	}

	/**
	 * Displays job references based on the product code
	 * 
	 * @param productCode
	 */
	private void displayJobReferences(String productCode) {
		jobReferences = DatabaseConnection.getJobReferences(productCode);
		tableJobRef.setModel(new TableModel_JobReference(jobReferences));

		cboCustomerComboBox = new JComboBox(DatabaseConnection.getCustomers()
				.toArray());
		TableColumn customerColumn = tableJobRef.getColumnModel().getColumn(1);
		customerColumn
				.setCellEditor(new DefaultCellEditor(cboCustomerComboBox));
		cboCustomerComboBox.setEditable(true);
		cboCustomerComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String other = cboCustomerComboBox.getSelectedItem().toString();

				if (other.equals("-ADD NEW-")) {

					String newCustomer = JOptionPane
							.showInputDialog("Enter customer");

					if (newCustomer != null) {
						if (JOptionPane
								.showConfirmDialog(
										null,
										"Would you like to add this customer to the list?",
										"New customer",
										JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
							addNewCustomer(newCustomer);
						}

						cboCustomerComboBox.setSelectedItem(newCustomer);
					} else {
						cboCustomerComboBox.setSelectedItem(originalCustomer);
					}

				}
			}
		});

		cboVarnishYr = new JComboBox(DatabaseConnection
				.getCommonYrFromCommonTable("varnish").toArray());

		TableColumn varnishYrColumn = tableJobRef.getColumnModel().getColumn(2);
		varnishYrColumn.setCellEditor(new DefaultCellEditor(cboVarnishYr));
		cboVarnishYr.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					if (cboVarnishYr.getSelectedItem().equals("-ADD NEW-")) {

						String yr = JOptionPane
								.showInputDialog("Enter new yr number");

						if (yr != null) {
							addNewYr("varnish", yr);

							cboVarnishYr.setModel(new DefaultComboBoxModel(
									DatabaseConnection
											.getCommonYrFromCommonTable(
													"varnish").toArray()));

							cboVarnishYr.setEditable(true);

							cboVarnishYr.setSelectedItem(yr);

							cboVarnishYr.setEditable(false);

							getCommonYr("varnish");
						}
					}

				} catch (NullPointerException n) {

				}

			}
		});

		try {
			lblJobReference.setText((String) tableJobRef.getValueAt(0, 0));
		} catch (IndexOutOfBoundsException i) {
			lblJobReference.setText("");
		}

	}

	/**
	 * Adds a new customer to customer table
	 * 
	 * @param customerName
	 */
	private void addNewCustomer(String customerName) {
		DatabaseConnection.insertCustomer(customerName);
		cboCustomerComboBox.setModel(new DefaultComboBoxModel(
				DatabaseConnection.getCustomers().toArray()));
	}

	/**
	 * Adds a new colour to colour table
	 * 
	 * @param
	 */
	private void addNewColour(String colour) {
		DatabaseConnection.insertColour(colour);
		cboColourComboBox.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getColours().toArray()));
	}

	/**
	 * Adds a new size to size combo
	 * 
	 * @param
	 */
	private void addNewSize(String size) {

		DatabaseConnection.insertSize(size);
		cboSizeComboBox.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getSizes().toArray()));
	}

	/**
	 * Displays plate details based on a job reference
	 * 
	 * @param jobRef
	 */
	private void displayPlateDetail(String jobRef) {

		plateDetails = DatabaseConnection.getPlateDetail(jobRef);

		tableColours.setModel(new TableModel_PlateDetail(plateDetails));

		cboColourComboBox = new JComboBox(DatabaseConnection.getColours()
				.toArray());

		TableColumn colourColumn = tableColours.getColumnModel().getColumn(0);
		colourColumn.setCellEditor(new DefaultCellEditor(cboColourComboBox));
		cboColourComboBox.setEditable(true);
		cboColourComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String other = cboColourComboBox.getSelectedItem().toString();
				// int tableRow = tableColours.getSelectedRow();

				if (other.equals("-ADD NEW-")) {

					String newColour = JOptionPane
							.showInputDialog("Enter colour");

					if (newColour != null) {
						if (JOptionPane
								.showConfirmDialog(
										null,
										"Would you like to add this colour to the list?",
										"New colour", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
							addNewColour(newColour);
						}

						cboColourComboBox.setSelectedItem(newColour);
					} else {
						cboColourComboBox.setSelectedItem(originalColour);
					}
				}
			}
		});

		tableColours.getColumn("Stock").setPreferredWidth(30);
		tableColours.getColumn("+").setCellRenderer(new LabelRenderer());
		tableColours.getColumn("+").setPreferredWidth(10);
		tableColours.getColumn("-").setCellRenderer(new LabelRenderer());
		tableColours.getColumn("-").setPreferredWidth(10);
		tableColours.getColumn("").setCellRenderer(new UserButtonRenderer());
	}

	/**
	 * Opens GUI_InsertNewDC so user can add new dc numbers
	 */
	private void openGUI_AddJobReference(String productCode) {

		GUI_AddJobReference dialog = new GUI_AddJobReference(productCode);
		dialog.setTitle("Enter details");
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setLocationRelativeTo(null);
		dialog.setModal(true);
		dialog.setVisible(true);

		if (GUI_AddJobReference.isComplete()) {
			displayJobReferences(productCode);
		}
	}

	/**
	 * Opens GUI_AddNewColour so user can add colours
	 */
	private void openGUI_AddNewColour(String jobRef) {

		GUI_AddNewColour dialog = new GUI_AddNewColour(jobRef);
		dialog.setTitle("Enter details");
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setLocationRelativeTo(null);
		dialog.setModal(true);
		dialog.setVisible(true);

		if (GUI_AddNewColour.isComplete()) {
			displayPlateDetail(jobRef);
		}
	}

	/**
	 * Opens GUI_Administrator
	 */
	private void openGUI_Administrator() {

		GUI_Administrator adminFrame = new GUI_Administrator();
		adminFrame.setTitle("Administrator");
		adminFrame.setSize(1280, 730);
		adminFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		adminFrame.setLocationRelativeTo(null);
		adminFrame.setVisible(true);
	}
	
	/**
	 * Opens GUI_ConfirmPlates
	 */
	private void openGUI_ConfirmPlates() {

		GUI_ConfirmPlates frame = new GUI_ConfirmPlates();
		frame.setTitle("Plates");
		frame.setSize(1280, 730);
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	/**
	 * Gets common yr numbers from the common plate table
	 * 
	 * @param plateType
	 */
	private void getCommonYr(String plateType) {

		cboYrNumber.setModel(new DefaultComboBoxModel(DatabaseConnection
				.getCommonYrFromCommonTable(plateType).toArray()));
		cboYrNumber.setSelectedIndex(1);
	}

	/**
	 * Gets the common plate type, depending on parameters
	 * 
	 * @param plateType
	 * @param yr
	 */
	private void getCommonPlate(String plateType, String yr) {

		try {
			commonPlate = DatabaseConnection.getCommonPlate(commonPlateString,
					yr);

			txtYr.setText(commonPlate.getYr());
			txtPlateRef.setText(commonPlate.getPlateRef());
			txtStock.setText(String.valueOf(commonPlate.getStock()));
		} catch (NullPointerException n) {
			// n.printStackTrace();
		}
	}

	/**
	 * Adds a new common yr
	 * 
	 * @param plateType
	 * @param yr
	 */
	private void addNewYr(String plateType, String yr) {
		DatabaseConnection.insertCommonYR(plateType, yr);
	}

	/**
	 * Fires when a new product code is selected
	 */
	private void fireProductCodeChange() {
		productCode = productCodes.get(tableProductCodes.getSelectedRow())
				.getProductCode();

		lblProductCode.setText(productCode);

		displayJobReferences(productCode);

		if (tableJobRef.getRowCount() > 0) {
			displayPlateDetail((String) tableJobRef.getValueAt(0, 0));
		} else {
			tableColours.setModel(new DefaultTableModel());
		}

		if (tableProductCodes.getSelectedColumn() == 7) {
			if (JOptionPane.showConfirmDialog(null,
					"Are you sure you want to delete " + productCode + "?",
					"DELETE ?", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				DatabaseConnection.deleteProductCode(productCode);
				setupProductCodeTable();
			}
		}
	}

	/**
	 * Fires when a new job ref is selected
	 */
	private void fireJobRefChange() {
		try {
			jobRef = jobReferences.get(tableJobRef.getSelectedRow())
					.getJobReference();
			lblJobReference.setText(jobRef);

			if (tableJobRef.getSelectedColumn() == 3) {

				displayJobReferences(productCode);

				displayPlateDetail(jobReferences.get(0).getJobReference());
			} else {
				displayPlateDetail(jobReferences.get(
						tableJobRef.getSelectedRow()).getJobReference());
			}
		} catch (Exception e) {
			// Do nothing
		}
	}

	/**
	 * Updates common plate info
	 */
	private void fireCommonUpdate() {
		CommonPlate tempCommonPlate = new CommonPlate(commonPlate.getId(),
				txtYr.getText(), txtPlateRef.getText(), commonPlateString,
				Integer.valueOf(txtStock.getText()));

		DatabaseConnection.updateCommonPlate(tempCommonPlate);

		getCommonPlate(commonPlateString, txtYr.getText());
	}
}// End class
