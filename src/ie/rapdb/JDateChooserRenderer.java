/**
 * 
 */
package ie.rapdb;

import java.awt.Component;
import java.util.Date;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import com.toedter.calendar.JDateChooser;

/**
 * @author Corwin
 *
 */
public class JDateChooserRenderer extends JDateChooser implements TableCellRenderer
{
	private static final long serialVersionUID = 1L;
	private JDateChooser dateChooser = new JDateChooser();

	@Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {

	    java.sql.Date date = null;
	    if (value instanceof Date) {
	        date = (java.sql.Date) value;
	    }
	    dateChooser.setDateFormatString("dd-MMM-yyyy");
	    dateChooser.setDate(date);

	    return dateChooser;
	}
}
