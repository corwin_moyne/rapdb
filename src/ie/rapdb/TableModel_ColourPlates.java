package ie.rapdb;

import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.table.AbstractTableModel;

public class TableModel_ColourPlates extends AbstractTableModel {

	private ArrayList<Plate> plates;

	private String[] columnNames = { "Ink Ref", "YR Number", "Order Qty",
			"Decrement", "Reason", "Delivery Timeline", "Add To Order" };

	public TableModel_ColourPlates(ArrayList<Plate> plates) {
		this.plates = plates;
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return plates.size();
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public Object getValueAt(int row, int col) {

		Plate p = plates.get(row);

		switch (col) {

		case 0:
			return p.getInkRef();
		case 1:
			return p.getYr_number();
		case 2:
			return p.getOrderQty();
		case 3:
			return p.getDecrement();
		case 4:
			return p.getReorderReason();
		case 5:
			return p.getDeliveryTimeline();
		case 6:
			return p.isAddToOrder();
		}

		return null;
	}

	public void setValueAt(Object value, int row, int col) {

		Plate p = plates.get(row);

		switch (col) {

		case 0:
			p.setInkRef((String) value);
			break;
		case 1:
			p.setYr_number((String) value);
			break;
		case 2:
			plates.get(row).setOrderQty(Integer.valueOf((String) value));
			break;
		case 3:
			plates.get(row).setDecrement(Integer.valueOf((String) value));
			break;
		case 4:
			if (value.equals("Other")) {
				plates.get(row).getReorderReason();
				break;
			} else {
				plates.get(row).setReorderReason((String) value);
				break;
			}
		case 5:
			plates.get(row).setDeliveryTimeline((String) value);
			break;
		case 6:
			plates.get(row).setAddToOrder((boolean) value);
			break;
		}

		DatabaseConnection.updatePlateColorAndYr(p.getInkRef(),
				p.getYr_number(), p.getId());
	}

	// getColumnClass
	@Override
	public Class<?> getColumnClass(int col) {

		Class[] columns = new Class[] { String.class, String.class,
				JComboBox.class, JComboBox.class, JComboBox.class,
				JComboBox.class, Boolean.class };

		return columns[col];
	}

	@Override
	public boolean isCellEditable(int row, int col) {

		return true;

	}

}
