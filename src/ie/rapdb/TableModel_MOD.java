package ie.rapdb;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.table.AbstractTableModel;

import com.toedter.calendar.JDateChooser;

public class TableModel_MOD extends AbstractTableModel {

	private ArrayList<Die> dies;
	private String[] columnNames;
	private String displayType;
	private String dieType;

	public TableModel_MOD(ArrayList<Die> dies, String[] columnNames,
			String displayType) {
		this.dies = dies;
		this.columnNames = columnNames;
		this.displayType = displayType;
		this.dieType = new String();
	}
	
	public TableModel_MOD(ArrayList<Die> dies, String[] columnNames,
			String displayType, String dieType) {
		this.dies = dies;
		this.columnNames = columnNames;
		this.displayType = displayType;
		this.dieType = dieType;
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public int getRowCount() {
		return dies.size();
	}

	// Get values for JTable
	@Override
	public Object getValueAt(int row, int col) {

		Die d = dies.get(row);

		if (displayType.equals("removed")) {
			switch (col) {

			case 0:
				return d.getDC_Number();
			case 1:
				return d.getModNo();
			case 2:
				return d.getTotalRevs();
			case 3:
				return d.getRemovedDate();
			case 4:
				return d.getRemovedReason();
			}
		} else if (displayType.equals("due")) {
			switch (col) {

			case 0:
				return d.getDC_Number();
			case 1:
				return d.getModNo();
			case 2:
				return d.getDueDate();
			case 3:
				JButton button = new JButton("Delete");
				return button;
			}
		} else {
			switch (col) {

			case 0:
				return d.getModNo();
			case 1:
				return d.getTotalRevs();
			case 2:
				JButton jbutton = new JButton("Re-Order");
				return jbutton;
			}
		}
		return null;
	}

	@Override
	public void setValueAt(Object value, int row, int col) {

		Die d = dies.get(row);

		if (displayType.equals("due")) {

			switch (col) {

			case 1:
				d.setModNo((String) value);
				break;
			case 2:
				java.sql.Date sqlDueDate = new java.sql.Date(((java.util.Date) value).getTime());
				d.setDate(sqlDueDate);
				break;
			}
			
			DatabaseConnection.updateModAndDate(d.getId(), dieType, d.getModNo(), d.getDueDate(), d.getDC_Number());
		}
	}

	@Override
	public Class<?> getColumnClass(int col) {

		if (displayType.equals("due")) {
			Class[] columns = new Class[] { String.class, String.class,
					Date.class, JButton.class };

			return columns[col];
		}
		else if (displayType.equals("removed")) {
			
			Class[] columns = new Class[] { String.class, String.class,
					Integer.class, Date.class, String.class };

			return columns[col];

		}
		Class[] columns = new Class[] { String.class, Integer.class,
				JButton.class };

		return columns[col];
	}

	@Override
	public boolean isCellEditable(int row, int col) {

		if (displayType.equals("due")) {
			if (col == 1 || col == 2) {
				return true;
			}
		}

		return false;
	}
}
